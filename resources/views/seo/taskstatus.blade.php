 <div class="paginator">  
	                @if($SeoMilestoneTasks->count() > 0)
	                	<span> Viewing <strong>{{$SeoMilestoneTasks->firstItem()}}</strong> - <strong>{{$SeoMilestoneTasks->lastItem()}}</strong> of <strong>{{$SeoMilestoneTasks->count()}}</strong> </span>  
	                @endif	
	                </div>                 
							   
							 		<div class="tasks_wrapper">
							 			@foreach($SeoMilestoneTasks as $SeoMilestoneTask)
								      <div class="task clearfix state_open tasks_loop_con" id="task_{{$SeoMilestoneTask->id}}">
								      	<input type="hidden" id="task_ID" value="{{$SeoMilestoneTask->id}}">
								         <div class="taskwrap">							         
								            <a href="javascript:void(0);" title="Select Task Status" data-icon="o" class="taskcheckbox"> <span>Select Task Status</span> </a> 
									          <div class="taskpopover taskstatus"  style="display:none;"> 
									            <a data-state="1" href="javascript:void(0);"  class="iconcomplete">Completed</a> 
									            <a data-state="2" href="javascript:void(0);"  class="iconverified">Verified</a> 
									            <a data-state="3" href="javascript:void(0);" class="iconreview">In Review</a> 
									            <a data-state="4" href="javascript:void(0);"  class="iconhold">On Hold</a> 
									            <a data-state="5" href="javascript:void(0);"  class="iconreject">Not Applicable</a> 
									            <a data-state="0" href="javascript:void(0);"  class="iconopen">Open</a> 
								            </div>
								            <div class="pointsandstatus">
								               <ul class="taskpoints">
								                  <li class="hint"><b>{{$SeoMilestoneTask->estimated_time}}</b><small> Min</small>Effort</li>
								                  <li class="impactpoints hint"><b>{{$SeoMilestoneTask->points}}</b><small> Pts</small>Impact</li>
								               </ul>
								            </div>
								            <p>{{$SeoMilestoneTask->title}}</p>
								            <div class="taskStatuscheck" data-icon="U"> We've verified that this task is done. Go ahead and mark it "Completed." </div>
								            <ul class="taskfunctions">
								              <li class="bulk_container"><input type="checkbox"></li>
								              <li>
								               	<a data-function="about" href="javascript:void(0)" data-icon="r">About</a>
								               </li>
								              <li>
								              	<a data-function="how_to_complete" href="javascript:void(0)" data-icon="s">How To</a>
								              </li>
								              <li>
								              	<a data-function="notes" href="javascript:void(0)" data-icon="t">Notes</a>
								              </li>
								              <!-- <li>
								              	<a class="attachments" data-function="attachments" href="javascript:void(0)">Attachments</a>
								              </li> -->
								              <li>
								              	<a data-function="assign" href="javascript:void(0)" data-icon="Q">Assign<small>&nbsp;&nbsp;</small></a>
								              </li>
								            </ul>
								         
								            <div class="taskfunctions_container">
								               <div class="taskpopover taskfunction taskfunction_about" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>About Task</h4>
								                  <div class="about_task">
								                  	{{$SeoMilestoneTask->about}}
								                  </div>
								               </div>
								               <div class="taskpopover taskfunction taskfunction_how_to_complete" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>How To Complete</h4>
								                 <div class="about_task">
								                  	{{$SeoMilestoneTask->how_to}}
								                  </div>
								               </div>
								               <div class="taskpopover taskfunction taskfunction_notes" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>Notes</h4>
								                  <form action="{{Route('taskNotesUpdate')}}" id="taskNotesUpdate">
									                  <p>
									                  	<textarea>@if($SeoMilestoneTask->userTaskDetail){{ $SeoMilestoneTask->userTaskDetail->notes	}}@endif</textarea>
									                  </p>
									                  <button>Save</button> 
								                  </form>
								               </div>
								               <div class="taskpopover taskfunction taskfunction_assign" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>Assign Task</h4>
								                  <div class="assign_container">
								                     <select>
								                        <option value="">Unassigned</option>
								                     </select>
								                     Due: 
								                     <div> <input type="text" value="2/4/2017" class="due_at"> <a href="javascript:void(0);" class="close" >X</a> </div>
								                     <br> 
								                  </div>
								                  <button>Save</button> 
								               </div>
								               <div class="taskpopover taskfunction taskfunction_attachments" style="display:none;">
								                  <a href="#" title="Close"  class="closepopover"></a>   
								                  <h4>Add Attachment</h4>
								                  <div class="attachment_container">
								                     <form class="add_attachment" enctype="multipart/form-data" action="/opus/attachments" accept-charset="UTF-8" method="post">
								                        <input name="utf8" type="hidden" value="✓"><input type="hidden" name="authenticity_token" value="+TZWMyKAJ39gejTVxH5Qvc8kmXT17uUktbkmgHZ7LcLgKmHe0X27xaMyquTy4vNIE0WNo9fFn9xTy+7AJuBZpg=="> <input name="attachment[task_id]" value="6943319" type="hidden"> File: 
								                        <div> <input type="file" name="attachment[stored_file]" size="30" class="attachment_stored_file"> </div>
								                        or URL: 
								                        <div> <input type="text" name="attachment[url]" size="30" class="attachment_url"> </div>
								                        <input name="commit" type="submit" value="Upload" class="btncolored"> 
								                     </form>
								                     <br> 
								                  </div>
								               </div>
								            </div>
								         </div>
								      </div>

							      @endforeach
							   </div> 
						   </div>