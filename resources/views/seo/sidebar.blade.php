<div class="seo-sidebar">
            <div class="cl-navblock">
                <div class="menu-space-area">
                        <div class="side-seo" >
                          <a href="{{ url('/home') }}"> 
                            <img src="{{asset('/public/images/logo.png')}}">
                          </a>
                        </div>
                        <div class="flipportal">
                            <select class="flip-it">
                                <option value="{{Route('home')}}">SMO</option>
                                <option selected="selected" value="{{Route('seotasks')}}">SEO</option>
                            </select>
                        </div>
                        <ul id="w0" class="cl-vnavigation nav">
                            <li class="{{ Request::is('home') ? 'active' : '' }}">
                                <a href="/seo/category/1"><i class="fa fa-dashboard fa-fw"></i><span>Tasks</span></a>
                            </li>
                           
                        </ul>                       
        
                </div>
            </div>
        </div>
    <div class="powered-by-mobile" style="display:none">Powered by, <img src="{!! asset('images/logo.jpeg') !!}"></div>