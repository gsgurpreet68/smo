	<input type="hidden" value="{{Request::segment(3)}}" id="category_segment"> 
	<input type="hidden" value="{{Request::segment(5)}}" id="milestone_segment"> 
	<input type="hidden" value="{{Route('getSubCategoryInfo')}}" id="getSubCatInfo">
	<aside class="functioncol view_tag" style="min-width: 320px; display: table-cell;">
		@if($masterCat->count() > 0)
	  	@foreach($masterCat as  $mastercat) 
				<div class="view_tag parent_con" id="first_level_{{$mastercat->id}}">  
	    		<h3 data-icon="g">
	    			<a class="top-nav" href="javascript:void(0);">{{$mastercat->title}}</a>
	      	</h3> 
	        <div class="pointbox">95</div> 
        	<div class="progressbar">
        		<div class="bar" style="width: 2%;"></div>
          </div> 
          @if($mastercat->subcats->count() > 0)
	          <ul class="task_section_children functionlist" >
	          	@foreach($mastercat->subcats as $subcat)
	          	<li class="view_tag second_level_{{$subcat->id}}" id="task_section_{{$subcat->id}}" > 
	          		<a href="javascript:void(0);" data-id="{{$subcat->id}}" class="second_level_nav" id="milestone_category_info">{{$subcat->title}}</a>  
	      				<ul class="task_section_children">
	      				@foreach($subcat->milestonecats as $milestonecat)
	      					<li class="view_tag">	
	      						<a href="/seo/category/{{$subcat->id}}/tasks/{{$milestonecat->id}}" class="third_level_nav third_level_{{$milestonecat->id}}">{{$milestonecat->title}}</a>  
	      					</li>
	      				@endforeach	
								</ul>  
	      			</li>
	      			@endforeach
	      		</ul> 
	      	@endif	
				</div>
			@endforeach
		@endif		
	</aside>