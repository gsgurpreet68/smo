<!DOCTYPE html>
<!-- saved from url=(0041)https://dashboard.backatyou.com/scheduler -->
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link rel="icon" type="image/png" href="{!! asset('/public/images/index.png') !!}" />
  <title>Admin Panel </title>
  <link href="{!! asset('/public/css/linkPreview.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/jquery.timepicker.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/bootstrap.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/bootstrap.editable.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/font-awesome.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/style.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/admin/style.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/style-overrides.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/jquery-ui.css') !!}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="{!! asset('/public/js/jquery.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/bootstrap.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/jquery.timepicker.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/jquery-ui.js') !!}"></script>
</head>
<body>
<div class="container">
      @if(Session::has('error'))
        <div class="alert alert-info error">
          <h3 style="color:red">{{ Session::get('error') }}</h3>
        </div>
        @endif
      <form class="form-signin" action="{{Route('adminAuthorize')}}" method="POST">
       {{ csrf_field() }}
        <h2 class="form-signin-heading">Please sign in</h2>
        <div class="form-group">
          <label for="inputEmail" class="sr-only">Email address</label>
          <input id="inputEmail" name="email" class="form-control" placeholder="Email address" required="" autofocus="" type="email">
        </div>
        <div class="form-group">
          <label for="inputPassword" class="sr-only">Password</label>
          <input id="inputPassword" name="password" class="form-control" placeholder="Password" required="" type="password">
        </div>
        <div class="checkbox">
          <label>
            <input value="rememberme" name="rememberme" type="checkbox"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div>
</body>
</html>