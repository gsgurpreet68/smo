<div class="pull-right">
  <div class="btn-group" role="group" aria-label="...">
    <div class="btn-group profile-dropdown" role="group">
      <button type="button" class="btn btn-default dropdown-toggle no-margin-right" data-toggle="dropdown">
        <img class="img-rounded" src="@if(Auth::user()->avatar !='' ){{ Auth::user()->avatar}} @else {!! asset('/public//images/user-profile.jpg') !!} @endif " style="width:36px;height:36px;" >{{ Auth::user()->name}} <span class="caret"></span>
      </button>
      <ul class="dropdown-menu dropdown-menu-right" role="menu">
        <li><a href="{{ url('/admin/logout') }}">Log Out</a></li>
      </ul>
    </div>
  </div>
</div>