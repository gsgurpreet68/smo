<div class="cl-sidebar">
            <div class="cl-navblock">
                <div class="menu-space">
                    <div class="content">
                        <div class="side-user" style="text-align: center;border-bottom: 1px solid #2487e6;">
                           <span class="bay-logo" ><a href="{{ url('/home') }}"> Marketing Engines </a></span>
                        </div>
                        <ul id="w0" class="cl-vnavigation nav">
                            <li class="{{ Request::is('admin/welcome') ? 'active' : '' }}">
                                <a href="{{ url('admin/welcome')}}"><i class="fa fa-dashboard fa-fw"></i><span>Home</span></a>
                            </li>
                            <li class="{{ Request::is('admin/schedule') ? 'active' : '' }}">
                                <a href="{{ url('admin/schedule')}}"><i class="fa fa-calendar fa-fw"></i><span>Scheduler</span></a>
                            </li>
                            
                        </ul>                       
                      
                    </div>
                </div>
            </div>
        </div>
    <div class="powered-by-mobile" style="display:none">Powered by, <img src="{!! asset('images/logo.jpeg') !!}"></div>