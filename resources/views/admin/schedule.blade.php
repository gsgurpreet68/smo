@extends('admin.layout')

@section('pagetitle')
  <h3 class="social-media-center">Scheduler</h3>
  <p>Share announcements and personalize your automated content stream.</p>
@endsection

@section('content')
  @if (Session::has('message'))
    <div class="alert alert-info">
      <h3 style="color:green">{{ Session::get('message') }}</h3>
    </div>
  @endif
  @if (Session::has('error'))
    <div class="error">
      <h3 style="color:red">{{ Session::get('error') }}</h3>
    </div>
  @endif
  <input type="hidden" id="previewAPI" value="{{ url('/previewlink') }}">
  <form id="SubmitNewPost" role="form" enctype="multipart/form-data" action="{{ Route('adminScheduleSave') }}" data-async="" data-async-multi="" data-reload="true" method="POST" novalidate="">
    <input type="hidden"  value = "" name="postScheduleTime" class="postScheduleTime">   
    <input type="hidden"  value = "{{ csrf_token() }}" name="_token" id="tokenForAjax">
    <div id="ShareFields" class="row no-margin-top">
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="post-block-header">
          <span class="badge badge-primary">1</span> CREATE YOUR POST
        </div>
        <div id="content-box" class="post-block">
          <div id="selectPostType">
            <label>Choose a Post Type</label>
            <div class="btn-group btn-group-justified post-type-buttons" role="group" aria-label="...">
              <div class="btn-group" role="group">
                <button type="button" style="margin-bottom: -1px !important;" class="btn listing active btn-primary">
                  <i class="fa fa-link"></i> 
                  <span class="post-type-label">Link</span>
                </button>
              </div>
              <div class="btn-group" role="group">
                <button type="button" style="margin-bottom: -1px !important;" class="btn btn-default photo">
                  <i class="fa fa-picture-o"></i> 
                  <span class="post-type-label">Photos</span>
                </button>
              </div>
              <div class="btn-group" role="group">
                <button type="button" style="margin-bottom: -1px !important;" class="btn btn-default message">
                  <i class="fa fa-pencil-square-o"></i> 
                  <span class="post-type-label">Message</span>
                </button>
              </div>
            </div>
          </div>
          
          <!-- LINK PREVIEW -->
          <div id="FieldsLink">
            <div class="row">
              <div class="col-lg-12">
                <label>Provide Your Link</label>
                <div id="contentLinkContainer">
                  <div class="input-group">
                    <input type="url" id="text_contentLinkContainer" name="first_link" style="text-align: left" placeholder="Paste your link and click preview" class="form-control first_link" value="{{ old('first_link') }}">
                    <span class="input-group-btn">
                      <button id="getPreviewBtn_contentLinkContainer" class="btn btn-primary content-link-preview first_link_preview" type="button">
                        <i class="fa fa-link"></i> Preview
                      </button>
                    </span>
                  </div>
                  <span class="error previewurlerror"></span>
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                      <div class="preview_areas">
                        <div class="col-lg-12  top">
                          <img class="previewImg" src="">
                          <input type="hidden" name="previewImage" id="previewImage" value="" />                        
                        </div>
                        <div class="col-lg-12  bottom"> 
                          <span class="closePreview">X</span>
                          <h2 class="previewTitle" ></h2>
                          <div class="previewDescription" ></div>
                          <div class="previewSiteLink"></div>
                        </div>
                      </div>
                    </div>
                  </div>    
                </div>
                <p id="noLinkPreviewError" class="text-danger pull-left" style="display:none;font-weight: bold;">You must preview the link before submitting.</p>
              </div>
            </div>
          </div>
          <!-- LISTING FIELDS -->
          <div id="FieldsListing" style="margin-bottom: 10px;">
            <div id="listing-inputs">
              <div class="row" style="margin-top: 5px;">
                <div class="col-sm-3 col-xs-6">
                  <label>Banner</label>
                  <select id="listingType" class="form-control listing-field input-lg" value="{{ old('banner') }}" name="banner" data-parsley-required="true" style="font-size: 14px;">
                    <option value="New Listing">New Listing</option>
                    <option value="Open House">Open House</option>
                    <option value="For Sale">For Sale</option>
                    <option value="For Rent">For Rent</option>
                    <option value="Pending">Pending</option>
                    <option value="Reduced Price">Reduced Price</option>
                    <option value="Coming Soon">Coming Soon</option>
                    <option value="Sold">Just Sold</option>
                    <option value="No Banner">No Banner</option>
                  </select>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <label>Bedrooms</label>
                  <input id="listingBedrooms" class="form-control listing-field input-lg" type="number" value="{{ old('bedrooms') }}" min="0" max="100" step="1" name="bedrooms" >
                </div>
                <div class="col-sm-3 col-xs-6">
                  <label>Bathrooms</label>
                  <input id="listingBaths" value="{{ old('bathrooms') }}" class="form-control listing-field input-lg" type="number" min="0" max="100" step="0.25" name="bathrooms" >
                </div>
                <div class="col-sm-3 col-xs-6">
                  <label>Square Feet</label>
                  <input id="listingArea" class="form-control listing-field input-lg" type="number" min="0" max="100000" step="1" name="squarefeet" value="{{ old('squarefeet') }}">
                </div>
              </div>
            </div>
          </div>

          <!-- IMAGE UPLOADER -->
          <div id="FieldsPhoto" style="display:none;">
            <div class="large-images-alert alert alert-info alert-white alert-red rounded fade in" style="display:none">
              <div class="icon"><i class="fa fa-info-circle"></i></div>
              <div>Looks like you uploaded high-quality images. Thanks for your patience, it may take a few minutes to upload.</div>
            </div>
            <div id="photoUploaderDDArea">          
              <div id="photoUploader" class="ui-sortable photos-con">                
                <div class="multiboxContainer" style="float:left;">
                  <div class="multibox uploadedImg uploadButton">                 
                    <i style="color: #0095d7;margin: 5px;" class="fa fa-plus-circle fa-3x"></i><br>Add Photos
                  </div>                  
                  <input type="file" name="photoUpload[]" class="photoUploaderInput orakuploaderFileInput" >              
                </div>          
              </div>          
              <div class="clear"> </div>      
            </div>
            <p id="photoWarningTwitter" class="text-danger pull-left" style="display: none;margin: 0;">Twitter only supports 4 images per post. The first 4 images will be used.</p>
            <p id="photoWarningLinkedin" class="text-danger pull-left" style="display:none;margin: 0;">LinkedIn only supports 1 image per post. The first image will be used.</p>
            <p id="photoWarningNone" class="text-danger pull-left" style="display:none;margin: 0;">You must select at least 1 image.</p>
          </div>

          <!-- MESSAGE BOX -->
          <div id="FieldsMessage">
            <label>Write a Caption</label>
            <textarea id="newContentMessage" name="caption" class="form-control countbox" style="height: 56px; overflow: hidden; word-wrap: break-word; resize: horizontal;" placeholder="Type a caption and add a call to action here." data-parsley-required="true" >{{ old('caption') }}</textarea>
            <div class="character-counter pull-right text-muted"></div>
            <span class="error"></span>
          </div>
          <div id="FieldsBrandedBar" style="margin-top:10px;display: none!important;">
            <input type="checkbox" name="brandbar" value="{{ old('brandbar') }}" data-parsley-multiple="branded_bar_enabled"> <span class="brand-it"> Enable Branded Bar</span>
          </div>
        </div>

        <!-- SCHEDULE -->
        <div class="row">
          <div class="col-xs-12">
            <div class="post-block-header"><span class="badge badge-primary">2</span> SCHEDULE AND CONFIRM</div>
            <div class="post-block" id="scheduleArea">
              <div id="scheduleOptions">
                <p id="NewPostDateError" class="text-danger pull-left" style="display:none;">You must select a date and time in the future.</p>
              </div>
              <div class="row no-margin-top">
                <div class="col-md-6">
                  <label>Date</label>
                  <div class="input-group date-time-inputs input-group-lg" style="width: 100%;">
                    <span class="input-group-addon"><i class="fa fa-calendar-check-o"></i></span>
                    <input type="text" placeholder="Select a date" value="{{ old('schedule_date') }}" id="NewPostDate" class="form-control" name="schedule_date" >
                  </div>
                </div>
                <div class="col-md-6">
                  <label>Time</label>
                  <div class="input-group input-group-lg" style="width: 100%;">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input type="text" id="NewPostTime" name="schedule_time" class="form-control time ui-timepicker-input" placeholder="Select a time" data-parsley-required="true" value="{{ old('schedule_time') }}" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-12">
                  <span class="error"> </span>
                </div>
              </div>
              <!-- NETWORKS -->
              <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                  <span style="font-size: 16px;line-height: 21px;font-weight: 600;">
                    Networks
                    <span class="btn btn-default btn-sm" style="margin-left: 7px;display: none;" id="toggleNetworksOn"><i class="fa fa-caret-down"></i></span>
                    <span class="btn btn-default btn-sm" id="toggleNetworksOff" style="margin-left: 7px;"><i class="fa fa-caret-up"></i></span>
                  </span>               
                  <div id="networksPanel" >
                    <p id="networkCountError" class="text-danger" style="display:none;">You must select at least 1 social network.</p>                  
                    <div class="checkbox" style="margin-bottom: 2px;">
                      <label>
                        <input id="newPostFacebook" name="post_facebook" class="social-network" type="checkbox" value="1" checked="checked" >
                        <i class="fa fa-facebook-square fa-fw" style="color: #3B5998;"></i> Facebook
                      </label>
                    </div>
                    <div class="checkbox" style="margin-bottom: 2px;">
                       <label>
                         <input id="newPostTwitter" name="post_twitter" class="social-network" type="checkbox" value="1" checked="checked" >
                         <i class="fa fa-twitter-square fa-fw" style="color:#0077b5 ;"></i>Twitter
                       </label>
                    </div>
                    <div class="checkbox" style="margin-bottom: 2px;">
                      <label>
                        <input id="newPostLinkedin" name="post_linkedin" class="social-network" type="checkbox" value="1" checked="checked" >
                        <i class="fa fa-linkedin-square fa-fw" style="color: #0077b5;"></i>   LinkedIn
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center submit-new">
          <button id="btnSubmitNewPost" type="submit" class="btn btn-success btn-lg btn-submit-post" name="schedule_later">
            Schedule
          </button>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row">
        <div class="users-table">
            @if($allUsers != "")
              <h2>Users List</h2>
              <h5>Select user to assign post.</h5>
              <table>
                <thead>
                  <tr>
                    <th><input type="checkbox" name=""  id="select_all" ></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($allUsers as $user)
                  <tr>
                    <td>  
                      <input type="checkbox" class="checkbox" name="checkedusers[]" id="{{ $user->id }}" value="{{ $user->id }}">
                    </td>
                    <td>
                      <label for="{{ $user->id }}">{{$user->name}}</label> <br>
                    </td>
                    <td>
                      @if($user->email){{$user->email}} @else {{'zzzz@zzz.com'}} @endif <br>
                    </td> 
                    <td>
                      @if($user->usermeta){{$user->usermeta->phone}} @else {{ 'xxx-xxx-xxxx' }} @endif <br>
                    </td>  
                  </tr>
                @endforeach
                </tbody>
              </table>
            @endif 
          </div>       
        </div>
      </div>
    </div>
   
    <div class="row post-ui" style="margin-top: 10px;"></div>
  </form>
    
<!-- Failed posts -->

@if(!empty($failedposts) &&  $failedposts->count() > 0)
  <hr>
  <div class="row">
    <div class="col-xs-12">
      <div style="font-size: 20px;margin-bottom: 15px;">Failed Posts</div>
        <ul class="timeline" style="max-width: 594px;">
          @foreach($failedposts as $post)
            <li id="content_{{$post->id}}">
              <?php 
              $dateTime = strtotime($post->schedule_time); 
              $scheduleTime =  date('g:ia',$dateTime);  
              ?>
              <input type="hidden" id="scheduleTime-{{$post->id}}" value="<?php echo $scheduleTime; ?>">
              <?php $scheduleDate =  date('m/d/Y',$dateTime);  ?>
              <input type="hidden" id="scheduleDate-{{$post->id}}" value="<?php echo $scheduleDate; ?>">
              <i class="fa fa-image"></i>
              <span class="date">
                <?php $dateTime = strtotime($post->schedule_time); 
                echo $scheduleTime = date('F j, g:i a',$dateTime);  ?> 
              </span>
              <div class="boost-bar text-right">
                <!--   <button style="margin: 0 !important;" class="btn btn-primary btn-sm open-BoostContent boost-billing-check" data-ccid="1009315" data-cqid="1053788" data-lid="" data-boost="" data-toggle="modal" data-target="#BoostPost"><i class="fa fa-rocket"></i> Boost Post</button> -->
              </div>
              <div class="content">
                <div id="contentPreviewData" style="max-width: 472px;">
                <!-- Text -->                           
                  <div class="media-body">
                    <p style="margin-bottom: 10px;" class="caption-{{$post->id}}">{{$post->caption}}</p>
                  </div>                  
                   <!-- Photo -->
                  <div id="posts-media-{{$post->id}}">
                    @foreach($post->media as $photo)
                      <div class="media">
                        <div id="imageID">
                          <input type="hidden" value="{{$photo->id}}">
                        </div>
                        <img class="post-image main" src="/public/uploads/{{Auth::user()->id}}/{{$photo->photo}}">
                        <a href="javascript:void(0)" class="deletePhoto"></a>
                      </div>
                    @endforeach
                  </div>
                </div>                          <!-- Clear -->
                <div class="clear"></div>
                <hr style="margin: 15px 0 10px 0;">
                  <!-- Toolbar -->
                <div class="pull-left">
                @if($post->poststatus)
                Post failed on: 
                @if($post->poststatus->fb_status == 2)
                  <i class="fa fa-facebook-square" style="color: #3B5998;"></i>
                @endif    
                @if($post->poststatus->tw_status == 2)
                  <i class="fa fa-twitter-square" style="color: #55acee;"></i>
                @endif  
                @if($post->poststatus->in_status == 2)
                  <i class="fa fa-linkedin-square" style="color: #0077b5;"></i>
                @endif
                @endif    
                </div>
                <div class="pull-right">
                  <a href="{{url('/post/reschedule',$post->id)}}" class=""  data-toggle="modal" >Post Again</a><span style="color:#ccc">|</span>
                  <a href="javascript:void(0);" targetlink="{{ url('/post/delete',$post->id) }}" class="open-DeleteContent" data-toggle="modal" data-target="#DeleteContent">Delete Post</a>
                </div>
                <div class="clear"></div>
              </div>
            </li>
          @endforeach
        </ul>
      </div>
    </div>    
  @endif

<!-- Future posts -->     
  <hr>
  <div class="row">
    <div class="col-xs-12">
      <div style="font-size: 20px;margin-bottom: 15px;">        Scheduled Posts </div>
      @if(!empty($allposts))
        <ul class="timeline" style="max-width: 594px;">
         @foreach($allposts as $post)          
            <li id="content_{{$post->id}}">
              <input type="hidden" id="banner-{{$post->id}}" value="{{$post->banner}}">
              <input type="hidden" id="bathrooms-{{$post->id}}" value="{{$post->bathrooms}}">
              <input type="hidden" id="bedrooms-{{$post->id}}" value="{{$post->bedrooms}}">
              <input type="hidden" id="squarefeet-{{$post->id}}" value="{{$post->squarefeet}}">
              <input type="hidden" id="first_link-{{$post->id}}" value="{{$post->first_link}}">
              <input type="hidden" id="second_link-{{$post->id}}" value="{{$post->second_link}}">
              <input type="hidden" id="caption-{{$post->id}}" value="{{$post->caption}}">
              <?php   $dateTime = strtotime($post->schedule_time); 
                $scheduleTime =  date('g:ia',$dateTime);  ?>
              <input type="hidden" id="scheduleTime-{{$post->id}}" value="<?php echo $scheduleTime; ?>">
              <?php $scheduleDate =  date('m/d/Y',$dateTime);  ?>
              <input type="hidden" id="scheduleDate-{{$post->id}}" value="<?php echo $scheduleDate; ?>">
              <i class="fa fa-image"></i>
                <span class="date">
                  <?php $dateTime = strtotime($post->schedule_time); 
                  echo $scheduleTime = date('F j, g:i a',$dateTime);  ?> 
                </span>
                <div class="boost-bar text-right">
                    <!--   <button style="margin: 0 !important;" class="btn btn-primary btn-sm open-BoostContent boost-billing-check" data-ccid="1009315" data-cqid="1053788" data-lid="" data-boost="" data-toggle="modal" data-target="#BoostPost"><i class="fa fa-rocket"></i> Boost Post</button> -->
                </div>
                <div class="content">
                  <div id="contentPreviewData" style="max-width: 472px;">
                    <!-- Text -->                           
                    <div class="media-body">
                      <p style="margin-bottom: 10px;" class="caption-{{$post->id}}">{{$post->caption}}</p>
                    </div>
                     <!-- Photo -->
                    <div id="posts-media-{{$post->id}}">
                      @foreach($post->media as $photo)
                        <div class="media">
                          <div id="imageID">
                            <input type="hidden" value="{{$photo->id}}">
                          </div>
                          <img class="post-image main" src="/public/uploads/{{Auth::user()->id}}/{{$photo->photo}}">
                          <a href="javascript:void(0)" class="deletePhoto"></a>
                        </div>
                      @endforeach
                    </div>
                  </div>                          <!-- Clear -->
                  <div class="clear"></div>
                  <hr style="margin: 15px 0 10px 0;">
                    <!-- Toolbar -->
                  <div class="pull-left">
                  @if($post->network)
                    @if($post->network->fb_enable)
                      <i class="fa fa-facebook-square" style="color: #3B5998;"></i>
                    @endif    
                    @if($post->network->tw_enable)
                      <i class="fa fa-twitter-square" style="color: #55acee;"></i>
                    @endif
                    @if($post->network->in_enable)    
                      <i class="fa fa-linkedin-square" style="color: #0077b5;"></i>
                    @endif
                  @endif    
                  </div>
                  <div class="pull-right">
                    <!-- <a href="{{url('/post/update',$post->id)}}" class="open-EditContent" editpostid="{{$post->id}}" data-toggle="modal" data-target="#EditContent">Edit  Post</a><span style="color:#ccc">|</span>
                     -->
                     <a href="javascript:void(0);" targetlink="{{ url('admin/post/delete',$post->id) }}" class="open-DeleteContent" data-toggle="modal" data-target="#DeleteContent">Delete Post</a>
                  </div>
                  <div class="clear"></div>
                </div>
            </li>
           @endforeach                  
          @endif
        </ul>            
      </div>
    </div>     
  {!! $allposts->render() !!}    

<!-- DELETE CONTENT MODAL -->
  <div class="modal fade" id="DeleteContent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" action="" data-async="" method="POST" novalidate="">
          <input type="hidden"  name="_token" value="{{csrf_token()}}">
          <div class="modal-header bg-danger">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Confirm Content Removal</h4>
          </div>
          <div class="modal-body">
            This action will permanently remove this content.Are you sure you want to delete?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" name="action" value="Delete" class="btn btn-danger">Delete</button>
          </div>
        </form>
      </div>
    </div>
  </div><!-- /#DeleteContent -->
 
<!-- EDIT CONTENT MODAL -->
  <div class="modal fade" id="EditContent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" enctype="multipart/form-data" action="" data-async="" data-async-multi="" method="POST" novalidate="">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" id="editPostId" name="content_queue_id" value="">
          <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              ×
            </button>
            <h4 class="modal-title">Edit Post</h4>
          </div>
          <div class="modal-body">
            <div class="row no-margin-top">
              <div class="col-md-12 no-margin-top">
                <textarea id="contentMessage" rows="2" placeholder="Share a local story, current listing or open house announcement." name="caption" class="form-control countbox" ></textarea>
                <div id="contentEditListing" style="margin: 20px 0;">
                  <div style="border: 1px solid #CCC;padding: 10px;">
                    <div class="row no-margin-top">
                      <div class="col-xs-3">
                        <label>Type</label>
                        <select id="banner" class="form-control listing-field" name="banner" data-parsley-required="false">
                          <option value="New Listing">New Listing</option>
                          <option value="Open House">Open House</option>
                          <option value="For Sale">For Sale</option>
                          <option value="For Rent">For Rent</option>
                          <option value="Pending">Pending</option>
                          <option value="Reduced Price">Reduced Price</option>
                          <option value="Coming Soon">Coming Soon</option>
                          <option value="Sold">Just Sold</option>
                          <option value="No Banner">No Banner</option>
                        </select>
                      </div>
                      <div class="col-xs-3">
                        <label>Bedrooms</label>
                        <input id="bedrooms" class="form-control listing-field" type="number" min="0" max="100" step="1" name="bedrooms" data-parsley-required="false">
                      </div>
                      <div class="col-xs-3">
                        <label>Bathrooms</label>
                        <input id="bathrooms" class="form-control listing-field" type="number" min="0" max="100" step="0.25" name="bathrooms" data-parsley-required="false">
                      </div>
                      <div class="col-xs-3">
                        <label>Square Feet</label>
                        <input id="square_feet" class="form-control listing-field" type="number" min="0" max="100000" step="1" name="squarefeet" data-parsley-required="false">
                      </div>
                    </div>
                  </div>
                </div>
                <div id="contentEditUrl">
                  <div id="contentEditLinkContainer"></div>
                  <p id="noLinkPreviewEditError" class="text-danger pull-left" style="display:none;font-weight: bold;">You must preview the link before submitting.</p>
                  <input type="hidden" name="content_image">
                </div>
                <div id="contentEditPhoto">
                  <div id="editPhotoUploaderDDArea">
                    <div class="delete-media-con">                 
                    </div>          
                    <div id="editPhotoUploader" class="ui-sortable photos-con">               
                      <div class="multiboxContainer" style="float:left;"> 
                        <div class="multibox uploadedImg uploadButton"> 
                          <i style="color: #0095d7;margin: 5px;" class="fa fa-plus-circle fa-3x"></i><br>Add Photos
                        </div>                  
                        <input type="file" name="photoUpload[]" class="photoUploaderInput orakuploaderFileInput" >
                      </div>          
                    </div>          
                    <div class="clear"> </div>  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <p id="EditPostDateError" class="text-danger pull-left" style="display:none;">You must select a date and time in the future.</p>
            <div class="clear"></div>
            <span class="date-and-time pull-left" style="margin-top:3px">
              <input type="text" id="SPDate" class="form-control " name="schedule_date" data-parsely-required="true" style="margin-right:5px;display:inline-block;width: 125px">
              <input id="SPTime" name="schedule_time" value="" type="text" class="form-control time ui-timepicker-input" placeholder="Select a time" style="display:inline-block;width: 125px" data-parsley-required="true" autocomplete="off">
            </span>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button id="btnSubmitEditPost" type="submit" class="btn btn-success btn-submit-post">Save</button>
           </div>
        </form>
      </div>
    </div>
  </div><!-- /#EditContent -->   
@endsection