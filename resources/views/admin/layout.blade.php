<!DOCTYPE html>

<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link rel="icon" type="image/png" href="{!! asset('/public/images/index.png') !!}" />
  <title>Scheduler </title>
  <link href="{!! asset('/public/css/linkPreview.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/jquery.timepicker.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/bootstrap.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/bootstrap.editable.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/font-awesome.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/style.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/admin/style.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/style-overrides.css') !!}" rel="stylesheet">
  <link href="{!! asset('/public/css/jquery-ui.css') !!}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="{!! asset('/public/js/jquery.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/bootstrap.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/jquery.timepicker.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/jquery-ui.js') !!}"></script>
  <script type="text/javascript">
    if (window.location.hash == "#_=_"){
      window.location.hash = " ";
    }
  </script>
</head>
<body cz-shortcut-listen="true">
  <div id="cl-wrapper" class="fixed-menu">
     
    @include('admin.sidebar')
     
    <div class="container-fluid" id="pcont">
      <div class="cl-mcont">
        
        <div class="row no-margin-top">
          <div class="col-xs-12 top-main-header">
                     	         
            <!-- For Real Living -->
            <div class="row real-living">
              <div class="arrow_box"></div>
            </div>
            
  				  @include('admin.header-right')
  				  @yield('pagetitle')

  			  </div>
        </div>

        <hr class="title-rule">
  	    @yield('content')
        
      </div>
    </div>
  </div> 
  <script type="text/javascript" src="{!! asset('/public/js/script.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('/public/js/admin/script.js') !!}"></script>
</body>
</html>