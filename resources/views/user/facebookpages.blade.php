@extends('user.layout')

@section('pagetitle')
  <h3 class="social-media-center">Facebook Pages List</h3>
  <p>Connect your specific page here.</p>
@endsection

@section('content')
 
  @if (Session::has('message'))
    <div class="alert alert-info error">
      <h3 style="color:red">{{ Session::get('message') }}</h3>
    </div>
  @endif

  @if($pages)
    <div class="row">
      <div class="col-md-12">
        <form action="{{url('/facebook/savepage')}}" class="form-inline" method="post">
          <h2>Pages List </h2>
          <?php $i = 1; ?>
            @foreach($pages as $page)
              <input type="hidden" name="{{$page['id']}}" value="{{$page['name']}}">
              <div class="form-group">
                <label for="{{$page['id']}}">{{$page['name']}}</label>
                <input type="radio" class="form-control" name="pagename" value="{{$page['id']}}" id="{{$page['id']}}" @if($i==1) required @endif>
              </div>
              <br>
              <?php $i++; ?>
            @endforeach      
            <div class="form-group">
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
            <input type="hidden"  value = "{{ csrf_token() }}" name="_token">
        </form>    
      </div>
    </div>
  @endif 

@endsection