<div class="pull-right">
  <div class="btn-group" role="group" aria-label="...">
    <div class="btn-group" role="group">
      <button type="button" id="global-notifications" role="button" class="btn btn-default dropdown-toggle @if(Session::get('total_notifications') > 0) red-alert @endif " style="padding: 11px 13px 12px; position: relative;" >
        <i class="fa fa-bell" ></i>
        <span class="noty-manager-bubble">{{Session::get('total_notifications')}}</span>
      </button>
      <div id="notification-container" class="dropdown-menu dropdown-menu-right" role="menu">
        <input type="hidden" id="notification_route" value="{{Route('getnotifications')}}">
        <section class="panel">
          <header class="panel-heading"><strong>Notifications</strong></header>
          <div id="notification-list" class="list-group list-group-alt">
            <div class="no-notification notymanager-empty-html">You have no new notifications</div>
          </div>
          <footer class="panel-footer">
            <a href="{{ url('notifications')}}">See all notifications</a>
          </footer>
        </section>
      </div>
    </div>
    <div class="btn-group hidden-xs">
      <a href="{{ $userprofilelink['fb'] }}" class="btn btn-default social-btn" target="_blank" href="">
        <i class="fa fa-facebook"></i>
      </a>
      <a href="{{ $userprofilelink['tw'] }}"  class="btn btn-default social-btn" target="_blank" href="">
        <i class="fa fa-twitter"></i>
      </a>
      <a href="{{ $userprofilelink['in'] }}" class="btn btn-default social-btn" target="_blank" href="">
        <i class="fa fa-linkedin"></i>
      </a>
      <a href="{{ url('settings')}}" class="btn btn-default setting-btn hidden-xs setting-link">Settings</a>
    </div>
    <div class="btn-group profile-dropdown" role="group">
      <button type="button" class="btn btn-default dropdown-toggle no-margin-right" data-toggle="dropdown">
        <img class="img-rounded" src="@if(Auth::user()->avatar !='' && Auth::user()->avatar !='dummy.png'){{ Auth::user()->avatar}} @else {{url().'/public/images/user-profile.jpg'}}  @endif " style="width:36px;height:36px;" >{{ Auth::user()->name}} <span class="caret"></span>
      </button>
      <ul class="dropdown-menu dropdown-menu-right" role="menu">
        <li><a href="{{ url('/logout') }}">Log Out</a></li>
      </ul>
    </div>
  </div>
</div>