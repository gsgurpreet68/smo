@extends('user.layout')

@section('pagetitle')
  <h3 class="social-media-center">Notifications</h3>
@endsection


@section('content')

  @foreach($notifications as $notification)
    <ul>
      <li>
        <span>Title: {{$notification->title}} </span></li><br>
        <span>Description: {{$notification->description}} </span>
      </li>
    </ul>

  @endforeach

  {!! $notifications->render() !!}   

@endsection
