@extends('user.layout')

@section('pagetitle')
  <h3 class="social-media-center">Landing Pages</h3>
  <p>Personalized landing pages to capture leads and drive traffic to your listings and website.</p>
@endsection


@section('content')

  <div class="row">
  <!-- HOME VALUE WIDGET -->
    <div class="col-lg-6">
      <div class="block block-tool">
        <div class="header">
          <h4>Home Value Landing Page</h4>
        </div>
        <div class="content">
          <div class="row" style="margin-top:0">
            <div class="col-xs-5" style="padding-right:0px">
              <img class="img-rounded" src="{!! asset('/public/images/tool_hv.jpg') !!}" style="border: 1px solid #e9e9e9; width:100%">
            </div>
            <div class="col-xs-1" style="padding:0;">
                <i class="fa fa-facebook-square fa-fw fa-2x tool-popover" style="position:relative;left:0px"></i><br>
                <i class="fa fa-globe fa-2x fa-fw tool-popover" style="position:relative;left:0px"></i><br>
                <i class="fa fa-mobile fa-2x fa-fw tool-popover" style="position:relative;left:0px"></i>
            </div>
            <div class="col-xs-6">
                Maximize lead generation by placing this landing page on your website, Facebook page or newsletter.
                <br><br>
                <div class="center-content">
                    <a href="/landing-pages/home-value" class="btn btn-primary btn-lg"><i class="fa fa-gear"></i> Customize</a>
                </div>
            </div>
          </div>
      	</div>
  		</div>
		</div>
		<!-- CONTACT WIDGET -->
		<div class="col-lg-6">
	    <div class="block block-tool">
        <div class="header" style="padding: 10px 5px 5px">
            <h4>Contact Form Landing Page</h4>
        </div>
        <div class="content">
          <div class="row" style="margin-top:0">
            <div class="col-xs-5" style="padding-right:0px">
              <div style="display: inline">
                  <img class="img-rounded" src="{!! asset('/public/images/tool_contact.jpg') !!}" style="border: 1px solid #e9e9e9; width:100%">
              </div>
            </div>
            <div class="col-xs-1" style="padding:0;">
                <i class="fa fa-facebook-square fa-fw fa-2x tool-popover" style="position:relative;left:0px"></i><br>
                <i class="fa fa-globe fa-2x fa-fw tool-popover" style="position:relative;left:0px"></i><br>
                <i class="fa fa-mobile fa-2x fa-fw tool-popover" style="position:relative;left:0px"></i>
            </div>
            <div class="col-xs-6">
              Every website and Facebook page should have a contact form. You can also embed this form in your newsletter or email signature.
              <br><br>
              <div class="center-content">
                    <a href="/landing-pages/contact-form" class="btn btn-primary btn-lg"><i class="fa fa-gear"></i> Customize</a>
              </div>
            </div>
       		</div>
		    </div>
    	</div>
    </div>
	</div>

@endsection