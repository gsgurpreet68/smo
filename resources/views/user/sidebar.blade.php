<div class="cl-sidebar">
            <div class="cl-navblock">
                <div class="menu-space">
                    <div class="content">
                        <div class="side-user" style="text-align: center;border-bottom: 1px solid #2487e6;">
                           <a href="{{ url('/home') }}"> 
                            <img style="max-width: 100px;margin: 0 auto;" src="{{asset('/public/images/logo.png')}}">
                          </a>
                        </div>
                         <div class="flipportal">
                            <select class="flip-it">
                                <option selected="selected" value="{{Route('home')}}" >SMO</option>
                                <option value="{{Route('seotasks')}}">SEO</option>
                            </select>
                        </div>
                        <ul id="w0" class="cl-vnavigation nav">
                            <li class="{{ Request::is('home') ? 'active' : '' }}">
                                <a href="{{ url('home')}}"><i class="fa fa-dashboard fa-fw"></i><span>Home</span></a>
                            </li>
                            <li class="{{ Request::is('schedule') ? 'active' : '' }}">
                                <a href="{{ url('schedule')}}"><i class="fa fa-calendar fa-fw"></i><span>Scheduler</span></a>
                            </li>
                            <li class="{{ Request::is('analytics/posts') ? 'active' : '' }}">
                                <a href="{{ url('analytics/posts')}}"><i class="fa fa-calendar fa-fw"></i><span>Analytics</span></a>
                            </li>
                           <!--  <li class="{{ Request::is('landing-pages') ? 'active' : '' }}">
                                <a href="{{ url('landing-pages')}}"><i class="fa fa-calendar fa-fw"></i><span>Landing Pages</span></a>
                            </li> -->
                             
                            
                        </ul>                       
                      
                    </div>
                </div>
            </div>
        </div>
    <div class="powered-by-mobile" style="display:none">Powered by, <img src="{!! asset('images/logo.jpeg') !!}"></div>