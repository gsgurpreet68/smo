@extends('user.layout')

@section('pagetitle')
  <h3 class="social-media-center">Landing Pages</h3>
  <p>The home valuation tool lets your audience find the value of their home and captures the lead.</p>
@endsection


@section('content')

	<div class="col-lg-3">
    <form action="https://api.backatyou.com/v2/accounts/update-apps" method="POST" enctype="multipart/form-data" data-async="" data-async-multi="" novalidate="">
     	<input name="account_id" value="7446" type="hidden">
			<input name="bay_access_token" value="6E91231E-84C0-41F9-9F0E-DDFE68B02AE6" type="hidden">
			<input name="client_id" value="3896" type="hidden">
			<input name="update_app" value="home_value" type="hidden">
			<h3 style="display:inline">Settings</h3>
			<hr style="margin-top:15px;">
			<!-- LEADS EMAIL TO -->
			<label>Send Leads To <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Email where you want leads sent." data-original-title="" title=""></i></label>
			<input style="max-width:300px;" name="email" class="form-control" value="mdarwich12@gmail.com" type="email"><br>

			<!-- CC EMAIL TO -->
			<label>CC Leads To <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="In addition to the email address above, if you would like us to send leads to multiple email addresses, enter them here, separated by commas." data-original-title="" title=""></i></label>
	    <input name="cc_emails" class="form-control" value="" style="max-width: 300px;" type="text"><br>

			<!-- NAME AND AVATAR -->
			<label>Courtesy Of <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Choose how to show your profile photo and name." data-original-title="" title=""></i></label>
			<select style="max-width:300px;" id="hv_display_options" name="hv_display_options" class="form-control">
				<option value="3" selected="selected">Show display name and photo</option>
				<option value="2">Show display name only</option>
				<option value="1">Show photo only</option>
				<option value="0">Do not show</option>
			</select>
			<input id="display_name" name="display_name" value="1" type="hidden">
			<input id="display_photo" name="display_photo" value="1" type="hidden">
			<br>
			<!-- advanced settings -->
			<div class="advanced-options">

        <!-- PARAGRAPH BOLD -->
        <label>Title Text</label>
        <input name="title_text" class="form-control" value="What is Your Home Worth?" type="text"><br>

        <!-- PARAGRAPH BODY -->
        <label>Description</label>
        <input name="paragraph_text" class="form-control" value="Get Your Free Report Now!" type="text"><br>

				<!-- INSTANT VALUATION -->
				<label>Instant Valuation <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="When enabled, display the low, average, and high home value estimates for the specified address." data-original-title="" title=""></i></label><br>
                    <input id="instant_quote_off" name="instant_quote" value="0" data-parsley-multiple="instant_quote" type="radio">
				<label for="instant_quote_off" onclick="" style="margin-right:10px;">Off</label>

                    <input id="instant_quote_on" name="instant_quote" value="1" checked="checked" data-parsley-multiple="instant_quote" type="radio">
                    <label for="instant_quote_on" onclick="">On</label>
				<br><br>

				<!-- BACKGROUND IMAGE UPLOAD -->
				<label>Background Image <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Upload a custom background image." data-original-title="" title=""></i></label>
				<input id="background_url" name="background_url" value="https://s3.amazonaws.com/bay_apps/background-home-value.jpg" type="hidden">
				<input id="background_file" style="max-width:300px;display:none;" name="background" class="form-control" type="file"><br>
				<div id="background_links" style="">
					<a class="bg-link bg-change" href="#">Change</a> | <a class="bg-link bg-remove" href="#">Remove</a>
				</div><br>
				<!-- OVERLAY COLOR PICKER -->
				<label style="padding-right: 10px">Overlay Color <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Choose an overlay color." data-original-title="" title=""></i></label>
				<input id="hvColorPicker" style="display: none;" type="text"><div class="sp-replacer sp-light"><div class="sp-preview"><div class="sp-preview-inner" style="background-color: rgba(0, 0, 0, 0.5);"></div></div><div class="sp-dd">▼</div></div>
				<input name="background_rgba" id="hv_background_rgba" value="rgba(0, 0, 0, 0.5)" type="hidden">
				<br><br>

        <div class="text-center">
            <button id="ResetAdvancedSettings" type="button" class="btn btn-sm btn-default">Reset to Default Settings</button>
        </div>
        <hr>
			</div>
      <div class="text-center" style="margin-bottom: 10px;">
        <button type="submit" class="btn btn-success text-center">Save and Preview</button>
      </div>
		</form>

		<!-- CUSTOM URL -->
		<label style="margin-top:10px">Personalized URL <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="This is your personalized URL to capture leads." data-original-title="" title=""></i></label>
		<div class="input-group" style="max-width: 300px;">
      <input id="home-value-url" readonly="" class="form-control" value="http://bit.ly/1lkvXlF" type="text">
        <span class="input-group-btn">
            <button class="btn btn-default" id="copy-button" data-clipboard-target="home-value-url">Copy <i class="fa fa-copy"></i></button>
        </span>
		</div>

		<!-- FACEBOOK INSTALL BUTTON -->
		<label style="margin-top:10px">Facebook Application</label><br>
		<div id="hv_installed">
			<p>This app is installed on your Facebook business page.</p>
			<div class="text-center">
				<button disabled="" class="btn" style="margin-left:0;width:150px;background: #3B5998; color:#FFF"><i class="fa fa-facebook-square"></i> Installed</button><br><br>
			</div>
		</div>
		<div id="hv_not_installed" style="display:none;">
			<p>You can install the home value tool on your Facebook business page.</p>
			<form action="https://api.backatyou.com/v2/facebook/install-app" method="POST" data-async="" novalidate="">
       	<input name="account_id" value="7446" type="hidden">
				<input name="bay_access_token" value="6E91231E-84C0-41F9-9F0E-DDFE68B02AE6" type="hidden">
				<input name="app" value="home_value" type="hidden">
        <div class="text-center">
            <button type="submit" class="btn " style="margin-left:0;width:150px;background: #3B5998; color:#FFF"><i class="fa fa-facebook-square"></i> Install Now</button><br><br>
        </div>
      </form>
		</div>
	</div>
@endsection