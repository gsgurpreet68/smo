@extends('user.layout')

@section('pagetitle')
  <h3 class="social-media-center">Landing Pages</h3>
  <p>The contact form tool lets prospective buyers and sellers get in touch quickly.</p>
@endsection


@section('content')
	<div class="row no-margin-top">
  	<div class="col-md-3">
      <form action="https://api.backatyou.com/v2/accounts/update-apps" method="POST" enctype="multipart/form-data" data-async="" data-async-multi="" novalidate="">
    	<input name="account_id" value="7446" type="hidden">
      <input name="bay_access_token" value="6E91231E-84C0-41F9-9F0E-DDFE68B02AE6" type="hidden">
      <input name="client_id" value="3896" type="hidden">
			<input name="update_app" value="contact_form" type="hidden">
      <h3 style="display:inline">Settings</h3>
      <hr style="margin-top:15px;">

      <!-- LEADS EMAIL TO -->
      <label>Send Leads To <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Specify the email you would like us to send your leads." data-original-title="" title=""></i></label>
      <input name="email" class="form-control" value="mdarwich12@gmail.com" style="max-width: 300px;" type="email"><br>

			<!-- CC EMAIL TO -->
			<label>CC Leads To <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="In addition to the email address above, if you would like us to send leads to multiple email addresses, enter them here, separated by commas." data-original-title="" title=""></i></label>
      <input name="cc_emails" class="form-control" value="" style="max-width: 300px;" type="text"><br>

      <!-- NAME AND AVATAR -->
      <label>Courtesy Of <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Choose how to show your profile photo and name." data-original-title="" title=""></i></label>
      <select style="max-width:300px;" id="contact_display_options" name="contact_display_options" class="form-control">
				<option value="3" selected="selected">Show display name and photo</option>
				<option value="2">Show display name only</option>
				<option value="1">Show photo only</option>
				<option value="0">Do not show</option>
      </select>
			<input id="display_name" name="display_name" value="1" type="hidden">
			<input id="display_photo" name="display_photo" value="1" type="hidden">
      <br>
        <!-- advanced settings -->
			<div class="advanced-options">

	      <!-- PARAGRAPH BOLD -->
	      <label>Title Text</label>
	      <input name="title_text" class="form-control" value="Looking to buy or sell a home?" type="text"><br>

	      <!-- PARAGRAPH BODY -->
	      <label>Description</label>
	      <input name="paragraph_text" class="form-control" value="Let our experience and knowledge of Real Estate work for you. No one knows your neighborhood better than us. Contact us below and see how we can work for you!" type="text"><br>

        <!-- BACKGROUND IMAGE UPLOAD -->
				<label>Background Image <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Upload a custom background image." data-original-title="" title=""></i></label>
				<input id="background_url" name="background_url" value="https://s3.amazonaws.com/bay_apps/background-contact-form.jpg" type="hidden">
				<input id="background_file" style="max-width:300px;display:none;" name="background" class="form-control" type="file"><br>
				<div id="background_links" style="">
					<a class="bg-link bg-change" href="#">Change</a> | <a class="bg-link bg-remove" href="#">Remove</a>
				</div><br>

				<!-- OVERLAY COLOR PICKER -->
        <label style="padding-right: 10px">Overlay Color <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Choose an overlay color." data-original-title="" title=""></i></label>
        <input id="contactColorPicker" style="display: none;" type="text"><div class="sp-replacer sp-light"><div class="sp-preview"><div class="sp-preview-inner" style="background-color: rgba(0, 0, 0, 0.5);"></div></div><div class="sp-dd">▼</div></div>
        <input name="background_rgba" id="contact_background_rgba" value="rgba(0, 0, 0, 0.5)" type="hidden">
        <br><br>

        <div class="text-center">
            <button id="ResetAdvancedSettings" type="button" class="btn btn-sm btn-default">Reset to Default Settings</button>
        </div>
        <hr>
   		</div>
      <div class="text-center" style="margin-bottom: 10px;">
          <button type="submit" class="btn btn-success text-center">Save and Preview</button>
      </div>
    </form>

    <!-- CUSTOM URL -->
    <label style="margin-top:10px">Personalized URL <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="This is your personalized URL to capture leads." data-original-title="" title=""></i></label>
    <div class="input-group" style="max-width: 300px;">
      <input readonly="" id="contact-form-url" class="form-control" value="http://bit.ly/1lkvXlH" type="text">
		<span class="input-group-btn">
		  <button id="copy-button" data-clipboard-target="contact-form-url" class="btn btn-default ">Copy <i class="fa fa-copy"></i></button>
		</span>
  </div>
	<!-- FACEBOOK INSTALL BUTTON -->
	  <label style="margin-top:10px">Facebook Application</label><br>
	  <div id="leads_installed">
	      <p>This app is installed on your Facebook business page.</p>
	      <div class="text-center">
	          <button disabled="" class="btn " style="margin-left:0;width:150px;background: #3B5998; color:#FFF"><i class="fa fa-facebook-square"></i> Installed</button><br><br>
	      </div>
	  </div>
    <div id="leads_not_installed" style="display:none;">
      <p>You can install the contact form tool on your Facebook business page.</p>
      <form action="https://api.backatyou.com/v2/facebook/install-app" method="POST" data-async="" novalidate="">
	      <input name="account_id" value="7446" type="hidden">
				<input name="bay_access_token" value="6E91231E-84C0-41F9-9F0E-DDFE68B02AE6" type="hidden">
				<input name="app" value="contact_form" type="hidden">
        <div class="text-center">
            <button type="submit" class="btn " style="margin-left:0;width:150px;background: #3B5998; color:#FFF"><i class="fa fa-facebook-square"></i> Install Now</button><br><br>
        </div>
      </form>
    </div>
  </div>
  <div class="col-md-9">
	  <iframe id="contactIframe" src="https://www.backatyou.com/t/contact-form/6E91231E-84C0-41F9-9F0E-DDFE68B02AE6" seamless="seamless" width="100%" height="700"></iframe>
	</div>
</div>
@endsection