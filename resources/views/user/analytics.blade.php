@extends('user.layout')
	@section('pagetitle')
		<h3 class="social-media-center">Facebook Analytics</h3>
	  <p>Analytics and demographics from your Facebook business page.</p>
	@endsection
@section('content')
<!-- Get scheduled post -->
@if(!empty($allposts)) 
	<!-- get parameter values from URL -->
	{{--*/ $sort = app('request')->input('sort') /*--}}			    	
	{{--*/ $perpage = app('request')->input('per-page') /*--}}	
	{{--*/ $search = app('request')->input('search') /*--}}
	{{--*/ $completeUrl = ""; /*--}}

	<!-- If Per page parameter exist in URL -->
	@if($perpage &&  $perpage !="")
		<?php $completeUrl = $completeUrl."&per-page=".$perpage; ?>
	@endif
	<!-- If Search parameter exist in URL -->
	@if($search &&  $search !="")
		<?php $completeUrl = $completeUrl."&search=".$search; ?>
	@endif
	<!-- If Order By Parameter from Controller -->
	@if($orderBy)
		@if($orderBy == "DESC")
			{{--*/ $orderkey = ""; /*--}}
		@else	
			{{--*/ $orderkey = "-"; /*--}}
		@endif
	@endif	
	

	<!-- End of getting parameter values from URL -->
	<div role="tabpanel">
  	<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="/analytics/posts">Posts</a></li>
			<!-- <li class=""><a href="/analytics/page">Page</a></li> -->
		</ul>
		<div class="tab-content">
		@if($totalRecords != "" && $totalRecords > 0 )
		<!-- POSTS TAB -->
			<div role="tabpanel" class="tab-pane active" id="posts">
				<div class="search-form">
			    <form id="w0" action="/analytics/posts" method="GET" role="form" novalidate="">
			    	
			    	@if($sort && $sort != "")
			    		<input type="hidden" name="sort" value="{{app('request')->input('sort')}}">
			    	@endif
			      <div class="per-page-select">Show
			        <select name="per-page" class="input-sm">
								<option value="10" @if($perpage == 10)  selected="selected" @endif>10</option>
								<option value="25" @if($perpage == 25)  selected="selected" @endif>25</option>
								<option value="50" @if($perpage == 50)  selected="selected" @endif>50</option>
							</select>
							&nbsp;entries
						</div>
						<div class="search-page">
							<div class="search-label">Search:</div>
							<div class="search-input-area">
								<div class="input-group">
									<div class="form-group field-analyticspostssearch-search">
										<input id="analyticspostssearch-search" class="form-control" name="search" type="text" value="{{ app('request')->input('search') }}">

										<p class="help-block help-block-error"></p>
									</div>					
									<span class="input-group-btn">
										<button type="submit" class="btn btn-success">Find</button>
									</span>
								</div>
							</div>
						</div>
			    </form>	
		    </div>
		    <!-- Get URL parameter  -->

				<div class="table table-responsive analytics-table">
					<div id="no-more-tables" class="grid-view hide-resize">
						<div id="no-more-tables-container" class="table-responsive kv-grid-container">
							<table id="postAnalytics" class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<thead>
									<tr class="table-header">
										<th>
											<a class="@if($sort == 'published') {{'desc'}} @elseif($sort == '-published') {{'asc'}} @endif" href="/analytics/posts?sort={{$orderkey}}published{{$completeUrl}}">Published</a>
										</th>
										<th>
											Post
										</th>
										<th>
											<a class="@if($sort == 'impressions') {{'desc'}} @elseif($sort == '-impressions') {{'asc'}} @endif" href="/analytics/posts?sort={{$orderkey}}impressions{{$completeUrl}}"{{$completeUrl}}>Impressions 
											<i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="The number of times your post is displayed. People may see multiple impressions of the post." data-original-title="" title=""></i>
											</a>
										</th>
										<th>
											<a class="@if($sort == 'reach') {{'desc'}} @elseif($sort == '-reach') {{'asc'}} @endif" href="/analytics/posts?sort={{$orderkey}}reach{{$completeUrl}}">
												Reach <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="The number of unique people that see your post. Reach may be less than impressions since a person is only counted once, even if they see your post multiple times." data-original-title="" title=""></i>
											</a>
										</th>
										<th>
											<a class="@if($sort == 'engagement') {{'desc'}} @elseif($sort == '-engagement') {{'asc'}} @endif" href="/analytics/posts?sort={{$orderkey}}engagement{{$completeUrl}}">Engagement <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="This is the total number of likes, comments, clicks and shares combined." data-original-title="" title=""></i></a>
										</th>
									</tr>
								</thead>
								<tbody>
						 		  @foreach($allposts as $post)
	                 	<?php $dateTime = strtotime($post->schedule_time); 
	                  $scheduleDate = date('m/d/Y',$dateTime); 
	                 	$scheduleTime = date('g:i a',$dateTime); ?>
		                <tr>
											<td data-title="Published">
												<span class="post-date">{{$scheduleDate}}</span>
												<div class="post-time">{{$scheduleTime}}</div>
											</td>
											<td data-title="Post">
												<div class="clear">
													<div class="post-container row">
														@foreach($post->media as $photo)
														<div class="post-thumbnail">
															<img src="/public/uploads/{{Auth::user()->id}}/{{$photo->photo}}"/>	
														</div>	
															<?php break; ?>		
														@endforeach
														<div class="post-link">
															<a target="_blank">{{ str_limit( $post->caption, 100)  }}</a>
														</div>
													</div>
												</div>
											</td>
											<td data-title="Impressions">
												<div class="post-impressions-count"> @if($post->analytic != "") {{$post->analytic->impressions}} @else {{'0'}} @endif</div>
											</td>
											<td data-title="Reach">
												<div class="post-reach-count" style="float:left"> @if($post->analytic != "") {{$post->analytic->reach}} @else {{'0'}} @endif</div>
											</td>
											<td data-title="Engagement">
												<div class="post-engagement-count"> @if($post->analytic != ""  ) {{$post->analytic->clicks + $post->analytic->likes +$post->analytic->comments + $post->analytic->share}} @else {{'0'}} @endif</div>
											</td>
										</tr>
			        		@endforeach
								</tbody>
							</table>
						</div>
						  {!! $allposts->render() !!} 
						  Showing {!! $allposts->firstItem() !!}-{!! $allposts->lastItem() !!} of {!! $allposts->total() !!}  items.   
					</div>	
				</div>
			</div>
			@else
				<div class="msg" style="text-align: center;" >No post has been scheduled.</div>
			@endif
		</div>
	</div>             
@endif       
@endsection