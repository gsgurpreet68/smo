<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoMilestoneTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_milestone_tasks', function(Blueprint $table){
            $table->increments('id');
            $table->integer('milestone_id')->unsigned();
            $table->foreign('milestone_id')->references('id')->on('seo_milestone_cat');
            $table->longText('title');
            $table->longText('about');
            $table->longText('how_to');
            $table->string('estimated_time');
            $table->string('points');
            $table->string('priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo_milestone_tasks');
    }
}
