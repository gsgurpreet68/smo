<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('banner');
			$table->string('bedrooms');
			$table->string('bathrooms');
			$table->string('squarefeet');
			$table->string('first_link');
			$table->string('second_link');
			$table->string('caption');
			$table->integer('brandbar');
			$table->integer('schedule_status');
			$table->string('fb_id');
			$table->datetime('schedule_time');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
