<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
           $table->increments('id');
           $table->string('guid')->nullable();
           $table->string('name')->nullable();
           $table->string('username')->nullable();
           $table->string('email')->nullable();
           $table->string('password', 255)->nullable();
           $table->string('avatar')->nullable();
           $table->string('provider')->nullable();
           $table->string('provider_id')->nullable();
           $table->integer('role')->default(2);
           $table->string('active')->default(2);
           $table->string('confirmed')->default(0);
           $table->string('last_login_ip')->nullable();
           $table->dateTime('last_login_date')->nullable();
           $table->rememberToken();
           $table->timestamps();
           $table->timestamp('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
