<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoSubCatgoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_sub_catgory', function(Blueprint $table){
            $table->increments('id');
            $table->integer('master_id')->unsigned();
            $table->foreign('master_id')->references('id')->on('seo_master_catgory');
            $table->longText('title');
            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo_sub_catgory');
    }
}
