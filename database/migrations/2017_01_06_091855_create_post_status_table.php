<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostStatusTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('post_status', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('post_id')->unsigned();
      $table->foreign('post_id')->references('id')->on('posts');
      $table->integer('fb_status');
      $table->integer('in_status');
      $table->integer('tw_status');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
     Schema::drop('post_status');
  }
}
