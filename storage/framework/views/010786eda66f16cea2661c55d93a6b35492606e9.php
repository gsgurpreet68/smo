<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
	<title>Marketing Engines Authorization</title>
	<link rel="icon" type="image/png" href="<?php echo asset('/public/images/index.png'); ?>" />
	<link href="<?php echo asset('/public/css/linkPreview.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/jquery.timepicker.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/font-awesome.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/style.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/style-overrides.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/jquery-ui.css'); ?>" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo asset('/public/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo asset('/public/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo asset('/public/js/jquery.timepicker.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo asset('/public/js/jquery-ui.js'); ?>"></script>
   <!-- Styles -->
	   
		<script>
if (window.location.hash == '#_=_'){

	// Check if the browser supports history.replaceState.
	if (history.replaceState) {

		// Keep the exact URL up to the hash.
		var cleanHref = window.location.href.split('#')[0];

		// Replace the URL in the address bar without messing with the back button.
		history.replaceState(null, null, cleanHref);

	} else {

		// Well, you're on an old browser, we can get rid of the _=_ but not the #.
		window.location.hash = '';

	}

}
		</script>
		<style type="text/css">
			.js-section-loading > h1::before {
			  background-color: #7ab55c;
			  bottom: -20px;
			  content: "";
			  display: block;
			  height: 3px;
			  left: 50%;
			  margin-left: -15px;
			  position: absolute;
			  top: auto;
			  width: 30px;
			}
			.js-section-loading > h1 {
			  position: relative;
			  color: #30373b;
			  display: block;
			  font-kerning: normal;
			  font-size: 1.875em;
			  margin-bottom: 30px;
			  font-weight: 500;
			  line-height: 1.33;
			}
			.js-section-loading > p.enter {
			  opacity: 1;
			  transform: translateY(0px);
			  transition-delay: 0.2s;
			}
			.js-section-loading > p {
			  color: #000;
			  font-size: 18px;
			  margin: 0;
			  opacity: 0;
			  position: absolute;
			  transform: translateY(-20px);
			  transition: all 0.7s ease-out 0s;
			  width: 100%;
			}
			.js-section-loading > p.exit {
			  opacity: 0;
			  transform: translateY(20px);
			  transition-delay: 0s;
			}
		</style>
	</head>
	<body>
		<div class="flex-center position-ref full-height">
			<div class="content">
				<div class="row text-center" >
				<input type="hidden" id="athorization_link" value="<?php echo e(Route('authlogin')); ?>">
					<img src="<?php echo env('URL')."/public/images/logo.png"; ?>"/>
					<section class="js-section-loading">
					  <h1 class="heading-1 enter exit">Authorization</h1>
					  <p class="checking">1 of 3: Checking your detail. </p>
					  <p class="error"></p>
					  <p class="dashboard">2 of 3: Initializing your dashboard.</p>
					  <p class="loggedin">3 of 3: Now you have Logged in.</p>
					</section>
				</div>
			</div>
		</div>

   <script type="text/javascript" src="<?php echo asset('/public/js/authorization.js'); ?>"></script>
	</body>
</html>
