<!DOCTYPE html>
<!-- saved from url=(0041)https://dashboard.backatyou.com/scheduler -->
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
  <link rel="icon" type="image/png" href="<?php echo asset('/public/images/index.png'); ?>" />
  <title>Scheduler </title>
  <link href="<?php echo asset('/public/css/linkPreview.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/jquery.timepicker.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/bootstrap.editable.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/seo/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/style-overrides.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/jquery-ui.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="<?php echo asset('/public/js/jquery.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/jquery.timepicker.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/jquery-ui.js'); ?>"></script>
  <script type="text/javascript">
    if (window.location.hash == "#_=_"){
      window.location.hash = " ";
    }
  </script>
</head>
<body cz-shortcut-listen="true">
  <div id="cl-wrapper" class="fixed-menu">
     
    <?php echo $__env->make('seo.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     
    <div class="container-fluid" id="pcont">
      <div class="cl-mcont">
        
        <div class="row no-margin-top">
          <div class="col-xs-12 top-main-header">
                     	         
            <!-- For Real Living -->
            <div class="row real-living">
              <div class="arrow_box"></div>
            </div>
  				  <?php echo $__env->make('seo.header-right', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  				  <?php echo $__env->yieldContent('pagetitle'); ?>

  			  </div>
        </div>

        <hr class="title-rule">
  	    <?php echo $__env->yieldContent('content'); ?>
        
      </div>
    </div>
  </div> 
  <script type="text/javascript" src="<?php echo asset('/public/js/seo/script.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/common.js'); ?>"></script>
</body>
</html>