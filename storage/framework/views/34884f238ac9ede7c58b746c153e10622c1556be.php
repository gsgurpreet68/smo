	<input type="hidden" value="<?php echo e(Request::segment(3)); ?>" id="category_segment"> 
	<input type="hidden" value="<?php echo e(Request::segment(5)); ?>" id="milestone_segment"> 
	<input type="hidden" value="<?php echo e(Route('getSubCategoryInfo')); ?>" id="getSubCatInfo">
	<aside class="functioncol view_tag" style="min-width: 320px; display: table-cell;">
		<?php if($masterCat->count() > 0): ?>
	  	<?php foreach($masterCat as  $mastercat): ?> 
				<div class="view_tag parent_con" id="first_level_<?php echo e($mastercat->id); ?>">  
	    		<h3 data-icon="g">
	    			<a class="top-nav" href="javascript:void(0);"><?php echo e($mastercat->title); ?></a>
	      	</h3> 
	        <div class="pointbox">95</div> 
        	<div class="progressbar">
        		<div class="bar" style="width: 2%;"></div>
          </div> 
          <?php if($mastercat->subcats->count() > 0): ?>
	          <ul class="task_section_children functionlist" >
	          	<?php foreach($mastercat->subcats as $subcat): ?>
	          	<li class="view_tag second_level_<?php echo e($subcat->id); ?>" id="task_section_<?php echo e($subcat->id); ?>" > 
	          		<a href="javascript:void(0);" data-id="<?php echo e($subcat->id); ?>" class="second_level_nav" id="milestone_category_info"><?php echo e($subcat->title); ?></a>  
	      				<ul class="task_section_children">
	      				<?php foreach($subcat->milestonecats as $milestonecat): ?>
	      					<li class="view_tag">	
	      						<a href="/seo/category/<?php echo e($subcat->id); ?>/tasks/<?php echo e($milestonecat->id); ?>" class="third_level_nav third_level_<?php echo e($milestonecat->id); ?>"><?php echo e($milestonecat->title); ?></a>  
	      					</li>
	      				<?php endforeach; ?>	
								</ul>  
	      			</li>
	      			<?php endforeach; ?>
	      		</ul> 
	      	<?php endif; ?>	
				</div>
			<?php endforeach; ?>
		<?php endif; ?>		
	</aside>