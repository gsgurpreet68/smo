<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Marketing Engines</title>
  <link rel="icon" type="image/png" href="<?php echo asset('/public/images/index.png'); ?>" />
  <link href="<?php echo asset('/public/css/linkPreview.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/jquery.timepicker.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/style-overrides.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('/public/css/jquery-ui.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="<?php echo asset('/public/js/jquery.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/jquery.timepicker.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo asset('/public/js/jquery-ui.js'); ?>"></script>
  <script src="http://cdn.auth0.com/js/lock/10.2/lock.min.js"></script>
  <script src="https://cdn.auth0.com/js/lock-passwordless-2.2.min.js"></script>
    <!-- Styles -->
    <style>
    
      .full-height {
        height: 100vh;
      }

      .flex-center {
       padding: 50px 0;
      }

      .position-ref {
        position: relative;
      }

      .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
      }

      .content {
        text-align: center;
      }

      .title {
        font-size: 24px;
        font-weight: bold;
      }

      .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
      }

      .m-b-md {
        margin-bottom: 30px;
      }
      .links ul {
        padding: 0;
        text-align: center;
      }
      .links li {
        display: inline-block;
        list-style: outside none none;
        margin: 0 10px;
      }
      .auth0-lock-header-logo{
        visibility: hidden;
      }
      .Logos .auth0-lock-header-logo {
        visibility: visible;
      }
      .auth0-lock.auth0-lock .auth0-lock-header{
        height: 120px;
      }

      .col-sm-1.or {
        min-height: 500px;
        position: relative;
        text-align: center;
      }
      .or h4 {
        position: absolute;
        top: 30%;
        width: 100%;
        left: 0;
      }
      .position-ref h3 {
        margin: 0 0 30px 0;
      }

      .social-user {
        text-align: right;
      }
      .social-user #root {
        float: right;
      }
      .social-user > h3 {
        text-align: right;
      }
      .mobile-user > h3 {
        text-align: left;
      }
      #roots {
        float: left;
      }


    @media (max-width: 767px) {
      .social-user #root {
        float: none;
        margin: 0 auto;
      }
      #roots {
        float: none;
      }
      .mobile-user > h3,.social-user > h3 {
        text-align: center;
      }
      .col-sm-1.or {
        min-height: 100px;
      }
    }   
    @media (max-width: 480px) {
      .auth0-lock.auth0-lock .auth0-lock-tabs-container {
        margin: 0 auto 15px!important;
        position: static!important;
      }
      .auth0-lock-header-bg-blur{
        display: block!important;
      }
    }

    </style>
    <script>
if (window.location.hash == '#_=_'){

  // Check if the browser supports history.replaceState.
  if (history.replaceState) {

    // Keep the exact URL up to the hash.
    var cleanHref = window.location.href.split('#')[0];

    // Replace the URL in the address bar without messing with the back button.
    history.replaceState(null, null, cleanHref);

  } else {

    // Well, you're on an old browser, we can get rid of the _=_ but not the #.
    window.location.hash = '';

  }

}
    </script>
  </head>
  <body>
    <div class="flex-center position-ref full-height">
       <input type="hidden" id="AUTH0_CLIENT_ID" value="<?php echo $auth0['AUTH0_CLIENT_ID']; ?>"/>
       <input type="hidden" id="AUTH0_DOMAIN" value="<?php echo $auth0['AUTH0_DOMAIN']; ?>"/>
       <input type="hidden" id="AUTH0_CLIENT_SECRET" value="<?php echo $auth0['AUTH0_CLIENT_SECRET']; ?>"/>
       <input type="hidden" id="AUTH0_CALLBACK_URL" value="<?php echo  env('URL').$auth0['AUTH0_CALLBACK_URL']; ?>"/>
       <input type="hidden" id="Logo" value="<?php echo  env('URL'); ?>"/>
     

      <div class="container">
        <?php if((app('request')->input('email')) != "" &&  (app('request')->input('success'))=="true"): ?>
           <h3 style="color:green"><?php echo e("Your Email is now verified. Please sign in to continue."); ?> </h3>
        <?php endif; ?>
        
        <?php if(Session::has('message')): ?>
        <div class="alert alert-info error">
          <h3 style="color:red"><?php echo e(Session::get('message')); ?></h3>
        </div>
        <?php endif; ?>
        <?php if(Session::has('suggestion')): ?>
        <div class="alert alert-info error">
          <h3 style="color:green"><?php echo e(Session::get('suggestion')); ?></h3>
        </div>
        <?php endif; ?>
        <div class="row">
          <div class="col-sm-5 col-xs-12 social-user">
          <h3>Login/Signup using social media</h3>
            <div id="root" >
            </div>
          </div>
          <div class="col-sm-1 col-xs-12 or">
          <h4>OR</h4>
          </div>
          <div class="col-sm-5 col-xs-12 mobile-user">
          <h3>Login/Signup using mobile</h3>
            <div id="roots" >
            </div>
          </div>
        </div>
        <!-- <div class="title m-b-md">
          Login With Social platform
        </div>
        
        <div class="links">
          <ul>
            <li>
              <a href="<?php echo e(url('social/facebook')); ?>"><img src="<?php echo e(url('/public/images/facebook.png')); ?>"></a>
            </li>
            <li>
              <a href="<?php echo e(url('social/twitter')); ?>"><img src="<?php echo e(asset('/public/images/twitter.png')); ?>"></a>
            </li>
            <li>
              <a href="<?php echo e(url('social/google')); ?>"><img src="<?php echo e(asset('/public/images/google.png')); ?>"></a>
            </li>
            <li>
              <a href="<?php echo e(url('social/linkedin')); ?>"><img src="<?php echo e(asset('/public/images/linkedin.png')); ?>"></a>
            </li>
          </ul>
        </div> -->
         
      </div>
    </div>
    
  <script type="text/javascript">
  var AUTH0_CLIENT_ID = "";
  var AUTH0_DOMAIN = "";
  var AUTH0_CLIENT_SECRET = "";
  var AUTH0_CALLBACK_URL = "";
  var Logo = "";
  $(document).ready(function() {        
     AUTH0_CLIENT_ID = $("#AUTH0_CLIENT_ID").val();
     AUTH0_DOMAIN = $("#AUTH0_DOMAIN").val();
     AUTH0_CLIENT_SECRET = $("#AUTH0_CLIENT_SECRET").val();
     AUTH0_CALLBACK_URL = $("#AUTH0_CALLBACK_URL").val();
     Logo = $("#Logo").val();
  });       
</script>
  <script type="text/javascript" src="<?php echo asset('/public/js/home-script.js'); ?>"></script>
  </body>
</html>
