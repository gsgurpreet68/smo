<?php $__env->startSection('pagetitle'); ?>
  <h3 class="social-media-center">Seo Tasks</h3>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
	


	<section class="page-wrapper">
		<header class="siteheader">
		  <div class="logo">
      	<a href="index.html"></a>
      </div>
      <!-- logo end here -->
      <ul class="headerpoints">
			  <li><b><span class="headerpoints_total">220</span> <small>Pts</small></b>Total</li>
			  <li><b><span class="headerpoints_today">0</span> <small>Pts</small></b>Today</li>
			  <li><b><span class="headerpoints_week">25</span> <small>Pts</small></b>Week</li>
			  <li><b><span class="headerpoints_month">220</span> <small>Pts</small></b>Month</li>
		  </ul>
		</header>
		<!-- header end here -->
		<!-- content section start here -->
		<div class="fullwidthcol">
      <div class="fullwidthwrap">
      	<?php echo $__env->make('seo.aside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<!-- right side section area -->
				<article class="contentcol">
				  <div class="content view_tag" style="display: block;">
				    <div class="docked upperright">  
					    <a href="#" class="button btncolored smallbtn btn_add_task">Add New Task</a> 
					    <a href="#" class="button smallbtn btn_switch_view" data-view-mode="list">List View</a> 
				    </div>
				    <ul class="breadcrumbs">  
					    <li><a href="">Optimize Your Presence</a></li>  
					    <li><a href=""><?php echo e($SeoSubCatgory->title); ?></a></li>  
					  </ul>
				    <div class="sectionlinks"> 
				     	<!-- <a href="#dashboard" title="Previous" class="prevsectionlink" data-icon="y"><span>Previous</span></a>  -->
				     	<a href="#tasks/website_101" title="Next" class="nextsectionlink" data-icon="y"><span>Next</span></a> 
				    </div>
				    <h1 class="establish-heading"><?php echo e($SeoSubCatgory->title); ?></h1>
				    <!-- tab section start here -->
				    <div class="content-update">
					    <div class="cat_container">
							  <div class="tasks_wrapper">
							    	<?php echo e($SeoSubCatgory->description); ?>

							  </div> 
							</div>
						</div>	
				    <div class="tasks standard_view tabbar_container">  </div>
				   <!--  <a href="#tasks/website_101" class="button btncolored tasknextbtn">Next</a> -->
				  </div>
				</article>
			</div>
		</div>
		<!-- content section end here -->
	</section>
	<!-- whole one page section wrapper end here -->	



<?php $__env->stopSection(); ?>
<?php echo $__env->make('seo.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>