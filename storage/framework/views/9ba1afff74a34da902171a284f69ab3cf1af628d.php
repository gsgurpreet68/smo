	<?php $__env->startSection('pagetitle'); ?>
		<h3 class="social-media-center">Facebook Analytics</h3>
	  <p>Analytics and demographics from your Facebook business page.</p>
	<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- Get scheduled post -->
<?php if(!empty($allposts)): ?> 
	<!-- get parameter values from URL -->
	<?php /**/ $sort = app('request')->input('sort') /**/ ?>			    	
	<?php /**/ $perpage = app('request')->input('per-page') /**/ ?>	
	<?php /**/ $search = app('request')->input('search') /**/ ?>
	<?php /**/ $completeUrl = ""; /**/ ?>

	<!-- If Per page parameter exist in URL -->
	<?php if($perpage &&  $perpage !=""): ?>
		<?php $completeUrl = $completeUrl."&per-page=".$perpage; ?>
	<?php endif; ?>
	<!-- If Search parameter exist in URL -->
	<?php if($search &&  $search !=""): ?>
		<?php $completeUrl = $completeUrl."&search=".$search; ?>
	<?php endif; ?>
	<!-- If Order By Parameter from Controller -->
	<?php if($orderBy): ?>
		<?php if($orderBy == "DESC"): ?>
			<?php /**/ $orderkey = ""; /**/ ?>
		<?php else: ?>	
			<?php /**/ $orderkey = "-"; /**/ ?>
		<?php endif; ?>
	<?php endif; ?>	
	

	<!-- End of getting parameter values from URL -->
	<div role="tabpanel">
  	<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="/analytics/posts">Posts</a></li>
			<!-- <li class=""><a href="/analytics/page">Page</a></li> -->
		</ul>
		<div class="tab-content">
		<?php if($totalRecords != "" && $totalRecords > 0 ): ?>
		<!-- POSTS TAB -->
			<div role="tabpanel" class="tab-pane active" id="posts">
				<div class="search-form">
			    <form id="w0" action="/analytics/posts" method="GET" role="form" novalidate="">
			    	
			    	<?php if($sort && $sort != ""): ?>
			    		<input type="hidden" name="sort" value="<?php echo e(app('request')->input('sort')); ?>">
			    	<?php endif; ?>
			      <div class="per-page-select">Show
			        <select name="per-page" class="input-sm">
								<option value="10" <?php if($perpage == 10): ?>  selected="selected" <?php endif; ?>>10</option>
								<option value="25" <?php if($perpage == 25): ?>  selected="selected" <?php endif; ?>>25</option>
								<option value="50" <?php if($perpage == 50): ?>  selected="selected" <?php endif; ?>>50</option>
							</select>
							&nbsp;entries
						</div>
						<div class="search-page">
							<div class="search-label">Search:</div>
							<div class="search-input-area">
								<div class="input-group">
									<div class="form-group field-analyticspostssearch-search">
										<input id="analyticspostssearch-search" class="form-control" name="search" type="text" value="<?php echo e(app('request')->input('search')); ?>">

										<p class="help-block help-block-error"></p>
									</div>					
									<span class="input-group-btn">
										<button type="submit" class="btn btn-success">Find</button>
									</span>
								</div>
							</div>
						</div>
			    </form>	
		    </div>
		    <!-- Get URL parameter  -->

				<div class="table table-responsive analytics-table">
					<div id="no-more-tables" class="grid-view hide-resize">
						<div id="no-more-tables-container" class="table-responsive kv-grid-container">
							<table id="postAnalytics" class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<thead>
									<tr class="table-header">
										<th>
											<a class="<?php if($sort == 'published'): ?> <?php echo e('desc'); ?> <?php elseif($sort == '-published'): ?> <?php echo e('asc'); ?> <?php endif; ?>" href="/analytics/posts?sort=<?php echo e($orderkey); ?>published<?php echo e($completeUrl); ?>">Published</a>
										</th>
										<th>
											Post
										</th>
										<th>
											<a class="<?php if($sort == 'impressions'): ?> <?php echo e('desc'); ?> <?php elseif($sort == '-impressions'): ?> <?php echo e('asc'); ?> <?php endif; ?>" href="/analytics/posts?sort=<?php echo e($orderkey); ?>impressions<?php echo e($completeUrl); ?>"<?php echo e($completeUrl); ?>>Impressions 
											<i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="The number of times your post is displayed. People may see multiple impressions of the post." data-original-title="" title=""></i>
											</a>
										</th>
										<th>
											<a class="<?php if($sort == 'reach'): ?> <?php echo e('desc'); ?> <?php elseif($sort == '-reach'): ?> <?php echo e('asc'); ?> <?php endif; ?>" href="/analytics/posts?sort=<?php echo e($orderkey); ?>reach<?php echo e($completeUrl); ?>">
												Reach <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="The number of unique people that see your post. Reach may be less than impressions since a person is only counted once, even if they see your post multiple times." data-original-title="" title=""></i>
											</a>
										</th>
										<th>
											<a class="<?php if($sort == 'engagement'): ?> <?php echo e('desc'); ?> <?php elseif($sort == '-engagement'): ?> <?php echo e('asc'); ?> <?php endif; ?>" href="/analytics/posts?sort=<?php echo e($orderkey); ?>engagement<?php echo e($completeUrl); ?>">Engagement <i class="fa fa-question-circle tool-popover" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="This is the total number of likes, comments, clicks and shares combined." data-original-title="" title=""></i></a>
										</th>
									</tr>
								</thead>
								<tbody>
						 		  <?php foreach($allposts as $post): ?>
	                 	<?php $dateTime = strtotime($post->schedule_time); 
	                  $scheduleDate = date('m/d/Y',$dateTime); 
	                 	$scheduleTime = date('g:i a',$dateTime); ?>
		                <tr>
											<td data-title="Published">
												<span class="post-date"><?php echo e($scheduleDate); ?></span>
												<div class="post-time"><?php echo e($scheduleTime); ?></div>
											</td>
											<td data-title="Post">
												<div class="clear">
													<div class="post-container row">
														<?php foreach($post->media as $photo): ?>
														<div class="post-thumbnail">
															<img src="/public/uploads/<?php echo e(Auth::user()->id); ?>/<?php echo e($photo->photo); ?>"/>	
														</div>	
															<?php break; ?>		
														<?php endforeach; ?>
														<div class="post-link">
															<a target="_blank"><?php echo e(str_limit( $post->caption, 100)); ?></a>
														</div>
													</div>
												</div>
											</td>
											<td data-title="Impressions">
												<div class="post-impressions-count"> <?php if($post->analytic != ""): ?> <?php echo e($post->analytic->impressions); ?> <?php else: ?> <?php echo e('0'); ?> <?php endif; ?></div>
											</td>
											<td data-title="Reach">
												<div class="post-reach-count" style="float:left"> <?php if($post->analytic != ""): ?> <?php echo e($post->analytic->reach); ?> <?php else: ?> <?php echo e('0'); ?> <?php endif; ?></div>
											</td>
											<td data-title="Engagement">
												<div class="post-engagement-count"> <?php if($post->analytic != ""  ): ?> <?php echo e($post->analytic->clicks + $post->analytic->likes +$post->analytic->comments + $post->analytic->share); ?> <?php else: ?> <?php echo e('0'); ?> <?php endif; ?></div>
											</td>
										</tr>
			        		<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						  <?php echo $allposts->render(); ?> 
						  Showing <?php echo $allposts->firstItem(); ?>-<?php echo $allposts->lastItem(); ?> of <?php echo $allposts->total(); ?>  items.   
					</div>	
				</div>
			</div>
			<?php else: ?>
				<div class="msg" style="text-align: center;" >No post has been scheduled.</div>
			<?php endif; ?>
		</div>
	</div>             
<?php endif; ?>       
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>