<?php $__env->startSection('pagetitle'); ?>
  <h3 class="social-media-center">Seo Tasks</h3>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
	 <!-- <?php echo e($SeoMilestoneCats->title); ?> -->
	<?php foreach($SeoMilestoneTasks as $SeoMilestoneTask): ?>
	<?php endforeach; ?>

	<section class="page-wrapper">
		<header class="siteheader">
		  <div class="logo">
      	<a href="index.html"></a>
      </div>
      <!-- logo end here -->
      <ul class="headerpoints">
			  <li><b><span class="headerpoints_total">220</span> <small>Pts</small></b>Total</li>
			  <li><b><span class="headerpoints_today">0</span> <small>Pts</small></b>Today</li>
			  <li><b><span class="headerpoints_week">25</span> <small>Pts</small></b>Week</li>
			  <li><b><span class="headerpoints_month">220</span> <small>Pts</small></b>Month</li>
		  </ul>
		</header>
		<!-- header end here -->
		<!-- content section start here -->
		<div class="fullwidthcol">
      <div class="fullwidthwrap">
      	<?php echo $__env->make('seo.aside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<!-- right side section area -->
				<article class="contentcol">
				   <div class="content view_tag" style="display: block;">
				      <div class="docked upperright">  
					      <a href="#" class="button btncolored smallbtn btn_add_task">Add New Task</a> 
					      <a href="#" class="button smallbtn btn_switch_view" data-view-mode="list">List View</a> 
				      </div>
				      <ul class="breadcrumbs">  
					      <li><a href="#tasks/optimize_your_presence">Optimize Your Presence</a></li>  
					      <li><a href="#tasks/establish_your_presence">Establish Your Presence</a></li>  
					      <li><a href="#tasks/website_101">Website 101: Set-up for Success</a></li>  
				      </ul>
				     <div class="sectionlinks"> 
				     <a href="#dashboard" title="Previous" class="prevsectionlink" data-icon="y"><span>Previous</span></a> <a href="#tasks/website_101" title="Next" class="nextsectionlink" data-icon="y"><span>Next</span></a> 
				     </div>
				     <h1 class="establish-heading"> Website 101: Set-up for Success  </h1>
				     <p class="Establish-peragraf">In <strong>Website 101: Set-up for Success</strong> you will be completing very important tasks around setting up your website to be a key building block of your online real estate. In this section we want to make sure that you implement Google Analytics, Google Search Console and a number of foundational items to monitor and measure the health of your website now and over time. These are quick check tasks and any issues will be resolved in future sections.</p>

				     <!-- tab section start here -->
				     <div class="tasks standard_view tabbar_container">
					   <p class="noopentasksmessage" style="display: none;">You have no open tasks at this time. Please feel free to move on to tasks in the next section.</p>
					   <ul class="taskfilters tabbar">
					      <li><a data-filter="OPEN" href="#" class="on">Open (<span class="taskfilters_count">6</span>)</a></li>
					      <li><a data-filter="REVIEW" href="#">In Review (<span class="taskfilters_count">0</span>)</a></li>
					      <li><a data-filter="VERIFIED" href="#">Verified (<span class="taskfilters_count">0</span>)</a></li>
					      <li><a data-filter="COMPLETED" href="#">Completed (<span class="taskfilters_count">5</span>)</a></li>
					      <li><a data-filter="SKIPPED" href="#">On Hold (<span class="taskfilters_count">0</span>)</a></li>
					      <li><a data-filter="REJECTED" href="#">Not Applicable (<span class="taskfilters_count">0</span>)</a></li>
					   </ul>
						<div class="taskmoveall_wrapper">
						      <div class="taskmoveall clearfix">
						         <a href="#" class="btn_moveall button smallbtn">Bulk Edit</a> <a href="#" class="btn_moveall_update button smallbtn btncolored">Update</a> <a href="#" class="btn_moveall_cancel button smallbtn">Cancel</a> 
						         <div class="bulktool" style="display:none;">
						            <div class="assign_container clearfix">
						               <div class="select_all">
						                  Select All: 
						                  <div class="bulk_container_select_all"><input type="checkbox"></div>
						               </div>
						               To: 
						               <span class="tasks-bulk-team-members-select">
						                  <select>
						                     <option value=""></option>
						                     <option value="-">Unassigned</option>
						                  </select>
						               </span>
						               Due: 
						               <div> <input type="text" class="due_at"> <a href="#" class="close" data-icon="V"></a> </div>
						               Status: 
						               <div class="state">
						                  <a href="#" title="Select Task Status" class="taskcheckbox"></a> 
						                  <div class="taskpopover taskstatus" style="display:none;"> <a data-state="COMPLETED" href="#" data-icon="M" class="iconcomplete">Completed</a> <a data-state="VERIFIED" href="#" data-icon="M" class="iconverified">Verified</a> <a data-state="REVIEW" href="#" data-icon="" class="iconreview">In Review</a> <a data-state="SKIPPED" href="#" data-icon="3" class="iconhold">On Hold</a> <a data-state="REJECTED" href="#" data-icon="V" class="iconreject">Not Applicable</a> <a data-state="OPEN" href="#" data-icon="1" class="iconopen">Open</a> </div>
						               </div>
						            </div>
						         </div>
						      </div>
						   </div>
                           <div class="paginator">   <span> Viewing <strong>1</strong> - <strong>6</strong> of <strong>6</strong> </span>  </div>
						 <div class="tasks_wrapper">
						      <div class="task clearfix state_open pass" id="task_6943319">
						         <div class="taskwrap">
						            <a href="#" title="Select Task Status" data-icon="1" class="taskcheckbox"> <span>Select Task Status</span> </a> 
						            <div class="taskpopover taskstatus" style="display:none;"> <a data-state="COMPLETED" href="#" data-icon="M" class="iconcomplete">Completed</a> <a data-state="VERIFIED" href="#" data-icon="M" class="iconverified">Verified</a> <a data-state="REVIEW" href="#" data-icon="" class="iconreview">In Review</a> <a data-state="SKIPPED" href="#" data-icon="3" class="iconhold">On Hold</a> <a data-state="REJECTED" href="#" data-icon="V" class="iconreject">Not Applicable</a> <a data-state="OPEN" href="#" data-icon="1" class="iconopen">Open</a> </div>
						            <div class="pointsandstatus">
						               <ul class="taskpoints">
						                  <li class="hint"><b>5</b><small> Min</small>Effort</li>
						                  <li class="impactpoints hint"><b>15</b><small> Pts</small>Impact</li>
						               </ul>
						            </div>
						            <p>Confirm that marketingengines.com is listed in Yahoo! and Bing.</p>
						            <div class="taskstatuscheck" data-icon="U"> We've verified that this task is done. Go ahead and mark it "Completed." </div>
						            <ul class="taskfunctions">
						               <li class="bulk_container"><input type="checkbox"></li>
						               <li><a data-function="about" href="#" data-icon="r">About</a></li>
						               <li><a data-function="how_to_complete" href="#" data-icon="s">How To</a></li>
						               <li><a data-function="notes" href="#" data-icon="t">Notes</a></li>
						               <li><a class="attachments" data-function="attachments" href="#">Attachments</a></li>
						               <li><a data-function="assign" href="#" data-icon="Q">Assign<small>&nbsp;&nbsp;</small></a></li>
						            </ul>
						            <div class="taskfunction_edit"> <a href="#" class="hint"><img src="/images/pencil.png"></a> </div>
						            <div class="taskfunctions_container">
						               <div class="taskpopover taskfunction taskfunction_about" style="display:none;">
						                  <a href="#" title="Close" data-icon="V" class="closepopover"><span>Close</span></a> 
						                  <h4>About Task</h4>
						                  <p>Yahoo! and Bing share the same search results and are both powered by Microsoft’s Bing search engine. For your website to be ranked in Yahoo! and Bing your website must first be able to be found by the Bing web crawler and included in their  database of website, also known as an Index. </p>
						                  <p>Although Bing crawls billions of pages to populate its database, it’s inevitable that some sites will be missed. If Bing misses a site, it’s frequently for one of the following reasons:</p>
						                  <ul>
						                     <li>There are not many other websites that link to your website (more on this later).</li>
						                     <li>Your site launched after Bing’s most recent crawl was completed.</li>
						                     <li>The design of your site makes it difficult for Bing to effectively crawl and databse your content (more on this later as well).</li>
						                     <li>Your site was temporarily unavailable when Bing tried to crawl it or Bing received an error when they tried to crawl it.</li>
						                     <li>You are telling Googlenot to crawl your site (more on this later) </li>
						                  </ul>
						               </div>
						               <div class="taskpopover taskfunction taskfunction_how_to_complete" style="display:none;">
						                  <a href="#" title="Close" data-icon="V" class="closepopover"><span>Close</span></a> 
						                  <h4>How To Complete</h4>
						                  <p>We will continue to monitor this task for you and will mark this task as completed once we recognize your website in Bing. We will also provide a number of tasks in future sections to help you solve this issue if the problem persists. </p>
						               </div>
						               <div class="taskpopover taskfunction taskfunction_notes" style="display:none;">
						                  <a href="#" title="Close" data-icon="V" class="closepopover"><span>Close</span></a> 
						                  <h4>Notes</h4>
						                  <p><textarea></textarea></p>
						                  <button>Save</button> 
						               </div>
						               <div class="taskpopover taskfunction taskfunction_assign" style="display:none;">
						                  <a href="#" title="Close" data-icon="V" class="closepopover"><span>Close</span></a> 
						                  <h4>Assign Task</h4>
						                  <div class="assign_container">
						                     <select>
						                        <option value="">Unassigned</option>
						                     </select>
						                     Due: 
						                     <div> <input type="text" value="2/4/2017" class="due_at"> <a href="#" class="close" data-icon="V"></a> </div>
						                     <br> 
						                  </div>
						                  <button>Save</button> 
						               </div>
						               <div class="taskpopover taskfunction taskfunction_attachments" style="display:none;">
						                  <a href="#" title="Close" data-icon="V" class="closepopover"><span>Close</span></a>   
						                  <h4>Add Attachment</h4>
						                  <div class="attachment_container">
						                     <form class="add_attachment" enctype="multipart/form-data" action="/opus/attachments" accept-charset="UTF-8" method="post">
						                        <input name="utf8" type="hidden" value="✓"><input type="hidden" name="authenticity_token" value="+TZWMyKAJ39gejTVxH5Qvc8kmXT17uUktbkmgHZ7LcLgKmHe0X27xaMyquTy4vNIE0WNo9fFn9xTy+7AJuBZpg=="> <input name="attachment[task_id]" value="6943319" type="hidden"> File: 
						                        <div> <input type="file" name="attachment[stored_file]" size="30" class="attachment_stored_file"> </div>
						                        or URL: 
						                        <div> <input type="text" name="attachment[url]" size="30" class="attachment_url"> </div>
						                        <input name="commit" type="submit" value="Upload" class="btncolored"> 
						                     </form>
						                     <br> 
						                  </div>
						               </div>
						            </div>
						         </div>
						      </div>
						   </div> 
						</div>
						                    





				      <div class="tasks standard_view tabbar_container">  </div>
				      <a href="#tasks/website_101" class="button btncolored tasknextbtn">Next</a>
				   </div>
				</article>
			</div>
		</div>
		<!-- content section end here -->
	</section>
	<!-- whole one page section wrapper end here -->	



<?php $__env->stopSection(); ?>
<?php echo $__env->make('seo.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>