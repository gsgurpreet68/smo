<div class="seo-sidebar">
            <div class="cl-navblock">
                <div class="menu-space-area">
                        <div class="side-seo" >
                          <a href="<?php echo e(url('/home')); ?>"> 
                            <img src="<?php echo e(asset('/public/images/logo.png')); ?>">
                          </a>
                        </div>
                        <div class="flipportal">
                            <select class="flip-it">
                                <option value="<?php echo e(Route('home')); ?>">SMO</option>
                                <option selected="selected" value="<?php echo e(Route('seotasks')); ?>">SEO</option>
                            </select>
                        </div>
                        <ul id="w0" class="cl-vnavigation nav">
                            <li class="<?php echo e(Request::is('home') ? 'active' : ''); ?>">
                                <a href="/seo/category/1"><i class="fa fa-dashboard fa-fw"></i><span>Tasks</span></a>
                            </li>
                           
                        </ul>                       
        
                </div>
            </div>
        </div>
    <div class="powered-by-mobile" style="display:none">Powered by, <img src="<?php echo asset('images/logo.jpeg'); ?>"></div>