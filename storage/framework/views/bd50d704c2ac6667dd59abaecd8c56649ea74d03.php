<?php $__env->startSection('pagetitle'); ?>
  <h3 class="social-media-center">Account Settings</h3>
  <p>Change your profile information.</p>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?> 

<?php if(Session::has('error')): ?>
  <div class="alert alert-info error">
    <h3 style="color:red"><?php echo e(Session::get('error')); ?></h3>
  </div>
<?php endif; ?>
<?php if(Session::has('message')): ?>
  <div class="alert alert-info">
    <h3 style="color:green"><?php echo e(Session::get('message')); ?></h3>
  </div>
<?php endif; ?>

<div class="profile-detail">
    <div class="col-md-12">
      <div class="row no-margin-top">
        <div class="col-sm-6">
          <label for="Name" class="control-label">Name</label>
        </div>

        <div class="col-sm-6">
          <a id="company" class="editable no-link required" ><?php echo e($currentUser->name); ?> </a>
          <a href="javascript:void(0)" field-type="text" field-name="name"  title="Name" class="btn btn-primary change-profile pull-right">Change Name</a>
        </div>
      </div>
      <hr>
      <div class="form-group row">
        <div class="col-sm-6">
          <label class="control-label">Company</label>
        </div>
        <div class="col-sm-6">
            <a id="company" class="editable no-link required" ><?php if($currentUser->usermeta): ?><?php echo e($currentUser->usermeta->company); ?><?php endif; ?></a>
            <a href="javascript:void(0)" field-type="text" field-name="company" title="Company" class="btn btn-primary change-profile pull-right">Change Company</a>
        </div>
      </div>
      <hr>
      <div class="form-group row">
        <div class="col-sm-6">
          <label class="control-label">Profile Photo</label>
        </div>
        <div class="col-sm-6">
          <img class="profile-picture" src="<?php if(Auth::user()->avatar !='' && Auth::user()->avatar !='dummy.png'): ?><?php echo e(Auth::user()->avatar); ?> <?php else: ?> <?php echo e(url().'/public/images/user-profile.jpg'); ?>  <?php endif; ?> " style="width: 150px;height: 150px;border: 1px solid #CCCCCC;border-radius: 2px;">
          <a href="javascript:void(0)"  title="Profile Image" data-toggle="modal" data-target="#updateProfile" class="btn btn-primary pull-right">Change Profile Image</a>
        </div>
      </div>
     <hr>
      <div class="form-group row">
        <div class="col-sm-6">
          <label class="control-label">Primary Phone Number</label>
        </div>
        <div class="col-sm-6">
          <a id="primary_phone" class="editable no-link required" ><?php if($currentUser->usermeta): ?><?php echo e($currentUser->usermeta->phone); ?><?php endif; ?></a>
          <a href="javascript:void(0)" field-type="phone" field-name="phone_number" title="Phone" class="btn btn-primary change-profile pull-right">Change Phone</a>
        </div>
      </div>
      <hr>
      <div class="form-group row">
        <div class="col-sm-6">
          <label for="Display_name" class="control-label" style="display: block;">Display Name<br>
            <span class="text-muted" style="font-size: 80%;font-weight: normal;">Your display name is used on your Landing Pages and any emails we send on your behalf (from field)</span>
          </label>
        </div>
        <div class="col-sm-6">
          <a id="display_name" class="editable required no-link" field-name="display_name" data-title="Display Name" tabindex="-1"><?php if($currentUser->usermeta): ?><?php echo e($currentUser->usermeta->display_name); ?><?php endif; ?></a>
          <a href="javascript:void(0)" field-type="text" title="Display Name"  class="btn btn-primary change-profile pull-right">Change Display Name</a>
        </div>
      </div>
      
      </div>
      <div class="row">
       <!--  <a href="javascript:void(0)" id="ProfileUpdate"  data-toggle="modal" data-target="#updateProfile" class="btn btn-primary">Update</a> -->
      </div>
</div>

<hr>
<div class="service-area">       
       <!-- Facebook Area -->
       <label>Networks<br>
        <span class="text-muted" style="font-size: 80%;font-weight: normal;">Configure your social media networks</span>
      </label>
         <div class="row"> 
          <div class="col-lg-6">  
             <div class="form-group"><i class="fa fa-facebook" aria-hidden="true"></i>
              <?php if($fbPageName): ?> "<?php echo e($fbPageName->value); ?>" <?php endif; ?> </div>
          </div>  
          <div class="col-lg-6 ">  
            <div class="form-group text-right">
              <?php if($loginUrl): ?>
                  <?php if($fbPageId): ?>
                  <a class="btn btn-primary" href="<?php echo e($loginUrl); ?>">Change</a>
                  <?php else: ?>
                  <a class="btn btn-primary" href="<?php echo e($loginUrl); ?>">Connect Facebook Account</a>
                  <?php endif; ?>
              <?php endif; ?>
              </div>
           </div> 
        </div>
        <!-- Facebook Area End-->

        <!-- LinkedIn Area -->
        <div class="clearfix"></div>
        <div class="row">    
           <div class="col-lg-6 ">  
            <div class="form-group">
              <i class="fa fa-linkedin" aria-hidden="true"></i>
                 <?php if($linkedinusername): ?> "<?php echo e($linkedinusername->value); ?>" <?php endif; ?>
            </div>
          </div>
          <div class="col-lg-6 "> 
             <div class="form-group text-right">
               <?php if($linkedinusername): ?>
                <a class="btn btn-primary" href="<?php echo e($linkedinurl); ?>">Update</a>
               <?php else: ?>
                <a class="btn btn-primary" href="<?php echo e($linkedinurl); ?>">Connect Linkedin Account </a>
               <?php endif; ?> 
             </div>
          </div>   

        </div>   
        <!-- LinkedIn Area End -->
         <!-- Twitter Area start -->
       

        <div class="clearfix"></div>
       <div class="row">    
          <div class="col-lg-6 ">  
           <div class="form-group">
             <i class="fa fa-twitter" aria-hidden="true"></i>
             <?php if($twitterusername): ?> "<?php echo e($twitterusername->value); ?>" <?php endif; ?>
           </div>
         </div>
         <div class="col-lg-6 "> 
            <div class="form-group text-right">
              <?php if($twitterusername): ?>
               <a class="btn btn-primary" href="<?php echo e($twitterurl); ?>">Update</a>
              <?php else: ?>
               <a class="btn btn-primary" href="<?php echo e($twitterurl); ?>">Connect Twitter Account </a>
              <?php endif; ?> 
            </div>
         </div>  

       </div>


 
        <!-- Twitter Area End -->
</div>




<div class="popover fade top in update-profile-popup" role="tooltip" style="top: -130px; left: -40px; display: none;">
  <div class="arrow" style="left: 50%;"></div>
    <h3 class="popover-title">License Number</h3>
    <div class="popover-content"> <div>
    <div class="editableform-loading" style="display: none;"></div>
      <form class="form-inline editableProfileForm" action="<?php echo e(Route('updateprofile')); ?>" method="POST">
        <div class="control-group form-group">
          <div>
          <div class="editable-input" style="position: relative;">
            <input class="form-control input-sm" autocomplete="off" style="padding-right: 24px;" type="text">
              <span class="editable-clear-x"></span>
          </div>
          <div class="editable-buttons">
            <button type="submit" class="btn btn-success btn-sm editable-submit">Save</button>
            <button type="button" class="btn btn-default btn-sm editable-cancel">Cancel</button>
          </div>
          </div>
          <div class="editable-error-block help-block" style="display: none;"></div>
        </div>
      </form>
    </div>
  </div>
</div> 
 <div class="modal fade" id="updateProfile" >
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="updateProfilePic" role="form" action="<?php echo e(Route('updateprofile')); ?>" method="POST" >
                <input type="hidden"  name="_token" value="<?php echo e(csrf_token()); ?>">
                <input type="hidden" name="changeProfileImg" value="">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Update Profile</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <h4 class="profile-msg"></h4>
                    <div class="row">
                      <div class="col-md-6" style="text-align: center;">
                        <div class="editableform-loading" style="display: none;"></div>
                          <img class="changeUserImg" style="max-width: 200px;margin: 0 auto;" src="<?php if(Auth::user()->avatar !='' && Auth::user()->avatar !='dummy.png'): ?><?php echo e(Auth::user()->avatar); ?> <?php else: ?> <?php echo e(url().'/public/images/user-profile.jpg'); ?>  <?php endif; ?> " />
                      </div>
                      <div class="col-md-6">
                          <button id="changePhotoUpload" type="button" class="btn btn-default btn-lg" style="width: 100%;"><i class="fa fa-upload" style="font-size: 22px;"></i><br>Upload a Photo<br>from My Computer</button>
                          <input type="file" style="display: none;" id="profile_pic" name="profile_pic" >
                      </div>
                    </div>
                  </div>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-popup" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /#DeleteContent -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>