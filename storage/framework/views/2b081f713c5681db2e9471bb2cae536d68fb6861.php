<?php $__env->startSection('pagetitle'); ?>
  <h3 class="social-media-center">Seo Tasks</h3>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
	 <!-- <?php echo e($SeoMilestoneCats->title); ?> -->

	<section class="page-wrapper">
		<header class="siteheader">
		  <div class="logo">
      	<a href="index.html"></a>
      </div>
      <!-- logo end here -->
      <ul class="headerpoints">
			  <li><b><span class="headerpoints_total">220</span> <small>Pts</small></b>Total</li>
			  <li><b><span class="headerpoints_today">0</span> <small>Pts</small></b>Today</li>
			  <li><b><span class="headerpoints_week">25</span> <small>Pts</small></b>Week</li>
			  <li><b><span class="headerpoints_month">220</span> <small>Pts</small></b>Month</li>
		  </ul>
		</header>
		<!-- header end here -->
		<!-- content section start here -->
		<div class="fullwidthcol">
      <div class="fullwidthwrap">
      	<?php echo $__env->make('seo.aside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<!-- right side section area -->
				<article class="contentcol">
				   <div class="content view_tag" style="display: block;">
				      <div class="docked upperright">  
					     <!--  <a href="#" class="button btncolored smallbtn btn_add_task">Add New Task</a>  -->
					      <a href="#" class="button smallbtn btn_switch_view" data-view-mode="list">List View</a> 
				      </div>
				      <ul class="breadcrumbs">  
					      <li><a href="/seo/category/<?php echo e($subCat->id); ?>"><?php echo e($subCat->belongstomaster->title); ?></a></li>  
					      <li><a href="/seo/category/<?php echo e($subCat->id); ?>"><?php echo e($subCat->title); ?></a></li>  
					      <li><a href="/seo/category/<?php echo e($subCat->id); ?>/tasks/<?php echo e($SeoMilestoneCats->id); ?>"><?php echo e($SeoMilestoneCats->title); ?></a></li>  
				      </ul>
				     <div class="sectionlinks"> 
				    	<a href="#dashboard" title="Previous" class="prevsectionlink" data-icon="y"><span>Previous</span></a> <a href="#tasks/website_101" title="Next" class="nextsectionlink" data-icon="y"><span>Next</span></a> 
				     </div>
				     	<h1 class="establish-heading"> <?php echo e($SeoMilestoneCats->title); ?> </h1>
				    <div class="content-update">
				     	<div class="cat_container">
							  <div class="tasks_description">
							  	<?php echo e($SeoMilestoneCats->description); ?>

							  </div>
						  </div>	
				     <!-- tab section start here -->
				     	<div class="tasks standard_view tabbar_container">
					   		<p class="noopentasksmessage" style="display: none;">You have no open tasks at this time. Please feel free to move on to tasks in the next section.</p>
					   	<ul class="taskfilters tabbar">
					      <li>
					      	<a data-filter="0" href="javascript:void(0);" class="on">Open (<span class="taskfilters_count open"><?php echo e($taskStatus['open']); ?></span>)</a>
					      </li>
					      <li>
					      	<a data-filter="3" href="javascript:void(0);">In Review (<span class="taskfilters_count review"><?php echo e($taskStatus['review']); ?></span>)</a>
					      </li>
					      <li>
					      	<a data-filter="2" href="javascript:void(0);">Verified (<span class="taskfilters_count verified"><?php echo e($taskStatus['verified']); ?></span>)</a>
					      </li>
					      <li>
					      	<a data-filter="1" href="javascript:void(0);">Completed (<span class="taskfilters_count completed"><?php echo e($taskStatus['completed']); ?></span>)</a>
					      </li>
					      <li>
					      	<a data-filter="4" href="javascript:void(0);">On Hold (<span class="taskfilters_count hold"><?php echo e($taskStatus['hold']); ?></span>)</a>
					      </li>
					      <li>
					      	<a data-filter="5" href="javascript:void(0);">Not Applicable (<span class="taskfilters_count reject"><?php echo e($taskStatus['reject']); ?></span>)</a>
					      </li>
					   	</ul>
							<div class="taskmoveall_wrapper">
						    <div class="taskmoveall clearfix">
						      <a href="#" class="btn_moveall button smallbtn">Bulk Edit</a> <a href="#" class="btn_moveall_update button smallbtn btncolored">Update</a><a href="#" class="btn_moveall_cancel button smallbtn">Cancel</a> 
						        <div class="bulktool" style="display:none;">
						          <div class="assign_container clearfix">
						            <div class="select_all">
						              Select All: 
						              <div class="bulk_container_select_all"><input type="checkbox"></div>
						            </div>
						              To: 
						            <span class="tasks-bulk-team-members-select">
						              <select>
			            	        <option value=""></option>
			                      <option value="-">Unassigned</option>
						            	</select>
						            </span>
						              Due: 
							          <div> 
							          	<input type="text" class="due_at"> <a href="#" class="close" data-icon="V"></a> 
							          </div>
							            Status: 
					              <div class="state">
					                <a href="#" title="Select Task Status" class="taskcheckbox"></a> 
						                <div class="taskpopover taskStatus" style="display:none;"> <a data-state="COMPLETED" href="#" data-icon="M" class="iconcomplete">Completed</a> 
						                <a data-state="VERIFIED" href="#" data-icon="M" class="iconverified">Verified</a> 
						                <a data-state="REVIEW" href="#" data-icon="" class="iconreview">In Review</a> 
						                <a data-state="SKIPPED" href="#" data-icon="3" class="iconhold">On Hold</a> 
						                <a data-state="REJECTED" href="#" data-icon="V" class="iconreject">Not Applicable</a> 
						                <a data-state="OPEN" href="#" data-icon="1" class="iconopen">Open</a> 
					                </div>
					              </div>
				           		</div>
				         		</div>
				      		</div>
				   			</div>
				   			<input type="hidden" value="<?php echo e(Route('updateTaskStatus')); ?>" id="updateStaus">
							   <input type="hidden" value="<?php echo e(Route('ajaxMilestoneTasks')); ?>" id="updatePostStatus" >
							   <input type="hidden" value="<?php echo e(Route('ajaxTasksCountByStatus')); ?>" id="ajaxTasksCountByStatus" >
                
                <div class="tasks-ajax-wrapper">
	                <div class="paginator">  
	                <?php if($SeoMilestoneTasks->count() > 0): ?>
	                	<span> Viewing <strong><?php echo e($SeoMilestoneTasks->firstItem()); ?></strong> - <strong><?php echo e($SeoMilestoneTasks->lastItem()); ?></strong> of <strong><?php echo e($SeoMilestoneTasks->count()); ?></strong> </span>  
	                <?php endif; ?>	
	                </div>                 
							   
							 		<div class="tasks_wrapper">
							 			<?php foreach($SeoMilestoneTasks as $SeoMilestoneTask): ?>
								      <div class="task clearfix state_open tasks_loop_con" id="task_<?php echo e($SeoMilestoneTask->id); ?>">
								      	<input type="hidden" id="task_ID" value="<?php echo e($SeoMilestoneTask->id); ?>">
								         <div class="taskwrap">							         
								            <a href="javascript:void(0);" title="Select Task Status" data-icon="o" class="taskcheckbox"> <span>Select Task Status</span> </a> 
									          <div class="taskpopover taskstatus"  style="display:none;"> 
									            <a data-state="1" href="javascript:void(0);"  class="iconcomplete">Completed</a> 
									            <a data-state="2" href="javascript:void(0);"  class="iconverified">Verified</a> 
									            <a data-state="3" href="javascript:void(0);" class="iconreview">In Review</a> 
									            <a data-state="4" href="javascript:void(0);"  class="iconhold">On Hold</a> 
									            <a data-state="5" href="javascript:void(0);"  class="iconreject">Not Applicable</a> 
									            <a data-state="0" href="javascript:void(0);"  class="iconopen">Open</a> 
								            </div>
								            <div class="pointsandstatus">
								               <ul class="taskpoints">
								                  <li class="hint"><b><?php echo e($SeoMilestoneTask->estimated_time); ?></b><small> Min</small>Effort</li>
								                  <li class="impactpoints hint"><b><?php echo e($SeoMilestoneTask->points); ?></b><small> Pts</small>Impact</li>
								               </ul>
								            </div>
								            <p><?php echo e($SeoMilestoneTask->title); ?></p>
								            <div class="taskStatuscheck" data-icon="U"> We've verified that this task is done. Go ahead and mark it "Completed." </div>
								            <ul class="taskfunctions">
								              <li class="bulk_container"><input type="checkbox"></li>
								              <li>
								               	<a data-function="about" href="javascript:void(0)" data-icon="r">About</a>
								               </li>
								              <li>
								              	<a data-function="how_to_complete" href="javascript:void(0)" data-icon="s">How To</a>
								              </li>
								              <li>
								              	<a data-function="notes" href="javascript:void(0)" data-icon="t">Notes</a>
								              </li>
								              <!-- <li>
								              	<a class="attachments" data-function="attachments" href="javascript:void(0)">Attachments</a>
								              </li> -->
								              <li>
								              	<a data-function="assign" href="javascript:void(0)" data-icon="Q">Assign<small>&nbsp;&nbsp;</small></a>
								              </li>
								            </ul>
								         
								            <div class="taskfunctions_container">
								               <div class="taskpopover taskfunction taskfunction_about" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>About Task</h4>
								                  <div class="about_task">
								                  	<?php echo e($SeoMilestoneTask->about); ?>

								                  </div>
								               </div>
								               <div class="taskpopover taskfunction taskfunction_how_to_complete" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>How To Complete</h4>
								                 <div class="about_task">
								                  	<?php echo e($SeoMilestoneTask->how_to); ?>

								                  </div>
								               </div>
								               <div class="taskpopover taskfunction taskfunction_notes" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>Notes</h4>
								                  <form action="<?php echo e(Route('taskNotesUpdate')); ?>" id="taskNotesUpdate">
									                  <p>
									                  	<textarea><?php if($SeoMilestoneTask->userTaskDetail): ?><?php echo e($SeoMilestoneTask->userTaskDetail->notes); ?><?php endif; ?></textarea>
									                  </p>
									                  <button>Save</button> 
								                  </form>
								               </div>
								               <div class="taskpopover taskfunction taskfunction_assign" style="display:none;">
								                  <a href="javascript:void(0);" title="Close"  class="closepopover">X</a> 
								                  <h4>Assign Task</h4>
								                  <div class="assign_container">
								                     <select>
								                        <option value="">Unassigned</option>
								                     </select>
								                     Due: 
								                     <div> <input type="text" value="2/4/2017" class="due_at"> <a href="javascript:void(0);" class="close" >X</a> </div>
								                     <br> 
								                  </div>
								                  <button>Save</button> 
								               </div>
								               <div class="taskpopover taskfunction taskfunction_attachments" style="display:none;">
								                  <a href="#" title="Close"  class="closepopover"></a>   
								                  <h4>Add Attachment</h4>
								                  <div class="attachment_container">
								                     <form class="add_attachment" enctype="multipart/form-data" action="/opus/attachments" accept-charset="UTF-8" method="post">
								                        <input name="utf8" type="hidden" value="✓"><input type="hidden" name="authenticity_token" value="+TZWMyKAJ39gejTVxH5Qvc8kmXT17uUktbkmgHZ7LcLgKmHe0X27xaMyquTy4vNIE0WNo9fFn9xTy+7AJuBZpg=="> <input name="attachment[task_id]" value="6943319" type="hidden"> File: 
								                        <div> <input type="file" name="attachment[stored_file]" size="30" class="attachment_stored_file"> </div>
								                        or URL: 
								                        <div> <input type="text" name="attachment[url]" size="30" class="attachment_url"> </div>
								                        <input name="commit" type="submit" value="Upload" class="btncolored"> 
								                     </form>
								                     <br> 
								                  </div>
								               </div>
								            </div>
								         </div>
								      </div>

							      <?php endforeach; ?>
							   </div> 
						   </div>
						</div>
					</div>

				      
				     <!--  <a href="#tasks/website_101" class="button btncolored tasknextbtn">Next</a> -->
				   </div>
				</article>
			</div>
		</div>
		<!-- content section end here -->
	</section>
	<!-- whole one page section wrapper end here -->	



<?php $__env->stopSection(); ?>
<?php echo $__env->make('seo.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>