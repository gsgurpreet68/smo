<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
     protected $table = 'networks';
     protected $fillable = ['fb_enable','in_enable','tw_enable'];

    //  public function network(){
    //     return $this->belongsTo('App\Post','post_id');
    // }

}
