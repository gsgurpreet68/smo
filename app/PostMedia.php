<?php namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

use \DB;

class PostMedia extends Model {

  //  use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['caption','photo'];

     public function posts(){
        return $this->belongsTo('App\Post','user_id');
    }
}
