<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\PostStatus;
use App\UserNotification;
use Happyr;
use Session;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\LinkedInSchedule;


class LinkedInQueueJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user, $post;
    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct(User $user,Post $post )
    {
        $this->user = $user;
        $this->post = $post;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
     public function handle()
    {
        \Log::info("Linkedin Job Start");
        $user = $this->user;
        $post = $this->post;
        $output = (new LinkedInSchedule)->socialLinkedinSchedule($user,$post );
        \Log::info($output);
        $poststatus =  PostStatus::firstOrCreate(array('post_id'=>$post->id));
        if($output['status']){ // If successfull post to social
           $poststatus->in_status = 1;
        }else{ // If fail scheduling
            $poststatus->in_status = 2;
            $usernotification = new UserNotification;
            $usernotification->title = $output['message'];
            $usernotification->description = $output['description'];
            $user->notification()->save($usernotification);
        }
        $poststatus->save();       
        \Log::info("Linkedin Job End");

    }
}
