<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\PostStatus;
use App\UserNotification;
use SammyK;
use Session;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\FacebookSchedule;

class CheckScheduleStatus extends Job implements SelfHandling, ShouldQueue
{
     use InteractsWithQueue, SerializesModels;
    protected $user, $post;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct(User $user,Post $post )
    {
        $this->user = $user;
        $this->post = $post;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $post = $this->post;
        \Log::info("status");
        $poststatus =  PostStatus::where(array('post_id'=>$post->id))->first();
            $fb_status = $poststatus->fb_status;
            $in_status = $poststatus->in_status;
            $tw_status = $poststatus->tw_status;
            if($fb_status == 2 || $in_status == 2 || $tw_status == 2){
                $post->schedule_status = 2;
            }else{
                $post->schedule_status = 1;
            }    
            $post->save();           
    }
}
