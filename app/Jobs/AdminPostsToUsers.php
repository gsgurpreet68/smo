<?php

namespace App\Jobs;
use App\Jobs\Job;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\AdminPost;
use App\Network;
use Session;
use File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminPostsToUsers extends Job implements SelfHandling, ShouldQueue
{
  use InteractsWithQueue, SerializesModels;
  protected $user, $post;
  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct(User $user,Post $post )
  {
      $this->user = $user;
      $this->post = $post;        
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle(){
    $user = $this->user;
    $lastpost = $this->post;
    $adminId = $user->id;
    $originalPostId = $lastpost->id;
    $adminPost = AdminPost::where('post_id','=',$originalPostId)->first();
    $allUsers = $adminPost->users;
    $usersList = json_decode($allUsers,true);

    foreach ($usersList as $eachUserId) {
      $currentUser = User::find($eachUserId);
      $newpost = $lastpost->replicate();
      $currentUser->posts()->save($newpost);
      $network = Network::where('post_id','=',$originalPostId)->first();
      $newnetwork = $network->replicate();
      $newpost->network()->save($newnetwork);

      //postMedia Table replica
      $media = PostMedia::where('post_id','=',$originalPostId)->get();
      
      $total = count($media);
      if($total > 0){
        foreach ($media as $key => $mediarow) {
          $originalmedia = PostMedia::find($mediarow->id);
          $filename = $originalmedia->photo;
          $newmedia = $originalmedia->replicate();
          $newpost->media()->save($newmedia);
          $path = public_path().'/uploads/'.$eachUserId;
          if (!file_exists($path)) {
             File::makeDirectory($path); 
          }
          $superAdminMedia =  public_path().'/uploads/'.$adminId."/".$filename;
          $userMedia =  public_path().'/uploads/'.$eachUserId."/".$filename;
          File::copy($superAdminMedia, $userMedia);
        }
      }
    }
  }
}
