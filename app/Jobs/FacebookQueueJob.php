<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\PostStatus;
use App\UserNotification;
use SammyK;
use Session;
use App\PostAnalytic;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\FacebookSchedule;

class FacebookQueueJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user, $post;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user,Post $post )
    {
        $this->user = $user;
        $this->post = $post;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("Facebook Job Start");
        $user = $this->user;
        $post = $this->post;
        $output = (new FacebookSchedule)->facebookSchedulePost($user,$post );
        $poststatus =  PostStatus::firstOrCreate(array('post_id'=>$post->id));
        if($output['status']){ // If successfull post to social
            $poststatus->fb_status = 1;
            $post->fb_id = $output['id'];
            $post->save();
            // Just store empty values in analytic table 
            $analytic = PostAnalytic::firstOrCreate(array('post_id'=>$post->id));    
            $analytic->impressions = 0;
            $analytic->reach = 0;
            $analytic->clicks = 0;
            $analytic->likes = 0;
            $analytic->comments = 0;
            $analytic->share = 0;
            $analytic->engagement = 0;
            $analytic->save();
        }else{ // If fail scheduling
            $poststatus->fb_status = 2;
            $usernotification = new UserNotification;
            $usernotification->title = $output['message'];
            $usernotification->description = $output['description'];
        
            $user->notification()->save($usernotification);
        }
        $poststatus->save();       
        \Log::info("Facebook Job end");
    }
}
