<?php

namespace App\Jobs;
use App\Jobs\Job;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\BackgroundFacebookAnalyticsController;

class FacebookAnalyticsJob extends Job implements SelfHandling, ShouldQueue
{
  use InteractsWithQueue, SerializesModels;
  protected $user;
  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct(User $user)
  {
      $this->user = $user;
      
  }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      \Log::info("Analytic Job Start");
        $user = $this->user;
        // get user which Logged in
        //call to function to getting analytics detail
        // This is Controller and call to getPostAnalyticsDetail function to gett detail and store in database.
        $output = (new BackgroundFacebookAnalyticsController)->getPostAnalyticsDetail($user);
        \Log::info("Analytic Job end");
    }
}
