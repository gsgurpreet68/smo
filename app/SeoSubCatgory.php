<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoSubCatgory extends Model
{
	protected $table = "seo_sub_catgory";
  protected $fillable = ['master_id','title', 'description'];

  public function belongstomaster(){
  	return $this->belongsTo('App\SeoMasterCatgory','master_id');
  }

  public function milestonecats(){
      return $this->hasMany('App\SeoMilestoneCatgory','sub_cat_id');
  }
}
