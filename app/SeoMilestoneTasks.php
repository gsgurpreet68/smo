<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class SeoMilestoneTasks extends Model
{		
	protected $table = "seo_milestone_tasks";
	protected $fillable = ['milestone_id','title', 'description'];

  public function belongstoMilestoneCat(){
  	return $this->belongsTo('App\SeoMilestoneCat','milestone_id');
  }

  public function userTaskDetail(){
  	return $this->hasOne('App\UserTaskDetail','task_id')->where('user_id',Auth::user()->id);
  }

  // scope to get only open tasks  by user
  public function scopeOpenGetTask($query,$milestone_id,$statusId){
  	$tasksList = $query->with('userTaskDetail')->whereHas('userTaskDetail', function($q) use ($milestone_id,$statusId) { 
  			$q->where('status', '!=',$statusId )->where("milestone_id","=",$milestone_id); 
  	});
  	return $tasksList;
  }

  // scope to get tasks according to status and milestone
  public function scopeFilterTaskStatus($query,$milestone_id,$statusId){

  	$tasksList = $query->with('userTaskDetail')->whereHas('userTaskDetail', function($q) use ($milestone_id,$statusId) { 
  			$q->where('status', '=',$statusId )->where("milestone_id","=",$milestone_id); 
  	});
  	return $tasksList;
  }


}
