<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostAnalytic extends Model
{
     protected $table = 'post_analytic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id','impressions','reach','clicks','likes','comments','share'];

     public function postanalytic(){
        return $this->belongsTo('App\Post','post_id');
    }
}
