<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostStatus extends Model
{
    protected $table = 'post_status';
    protected $fillable = ['post_id','fb_status','in_status','tw_status'];
    

    public function poststatusbelongs(){
        //return $this->hasMany('App\UserPost','user_id');
        return $this->belongsTo('App\Post','post_id');
    }
}
