<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = "user_notifications";

    public function userNotify(){
        return $this->belongsTo('App\User','user_id');
    }
}
