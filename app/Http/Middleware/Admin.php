<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class Admin
{
  protected $auth;
  /**
   * Create a new middleware instance.
   *
   * @param  Guard  $auth
   * @return void
   */
  public function __construct(Guard $auth)
  {
    $this->auth = $auth;
  }
  public function handle($request, Closure $next)
  {
    if ($this->auth->guest()) {
      return redirect()->guest('/');
    }else{
       if(Auth::user()->role == 1){
        return $next($request);
      }else{
        return redirect()->guest('/');
      }           
    }
    return $next($request);
  }
}
