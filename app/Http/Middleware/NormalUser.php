<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class NormalUser
{
  protected $auth;
  /**
   * Create a new middleware instance.
   *
   * @param  Guard  $auth
   * @return void
   */
  public function __construct(Guard $auth)
  {
    $this->auth = $auth;
  }
  public function handle($request, Closure $next)
  {   
    if(Auth::check()){
      if(Auth::user()->role == 2){
        return redirect()->guest('/home');      
      }else{
        return $next($request);   
      }      

    }else{
      return $next($request);
    }           
    
    return $next($request);
  }
}
