<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use Input;
use Session;
use Redirect;
use Storage;
use Validator;
use SammyK;
use View;
use File;


class FacebookController extends Controller
{

    public function __construct(){
        $userId = Auth::user()->id;
        $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();  
        $linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
        $twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();
        $userLinks['fb'] = "";  $userLinks['in'] = ""; $userLinks['tw']="";
        if($fbPageId){
            $userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
        }
        if($linkedin_profile_link){
            $userLinks['in'] = $linkedin_profile_link->value;   
        }
        if($twitterusername){
            $userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;  
        }
        
        View::share('userprofilelink', $userLinks);
        $this->fb = app(SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
    }
    public function getAccessTokennumber(){
                 try {
                     $helper = $this->fb->getRedirectLoginHelper();
                     $gettoken = Session::get('fb_user_access_token');

                    if (isset($gettoken)) {
                        $accessToken = Session::get('fb_user_access_token');

                    } else {

                        $accessToken = $helper->getAccessToken();
                      
                    }

                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                    Session::flash('error', "Graph returned an error: " . $e->getMessage());
                    return Redirect::back();   
                    exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                    Session::flash('error', "Facebook SDK returned an error: " . $e->getMessage());
                    return Redirect::back();    
                    exit;
                 }
                 Session::set('fb_user_access_token', (string) $accessToken);
                $this->fb->setDefaultAccessToken($accessToken);
                $userId = (Auth::User()->id);
               
                 $service = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'fb_secret_key'));
                    $service->value = $accessToken;
                    $service->save();
                
                // Get all facebook pages from Account
                    $pages = $this->fb->get('/me/accounts');
                    $pages = $pages->getGraphEdge()->asArray();
                return View('user.facebookpages',compact('pages')); 
    }           
    

    // Facebook Page Save
    public function facebookPageSave(Request $request){
            $data = $request->all();
            $pageId = $data['pagename'];
            $pageName = $data[$pageId];
            $userId = (Auth::User()->id);
           
            $fbpageid = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'fb_page_id'));
                $fbpageid->value = $pageId;
                $fbpageid->save();
            
            $fbpagename = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'fb_page_name'));
                $fbpagename->value = $pageName;
                $fbpagename->save();
            Session::flash('message', "Congrats! Page has successfully connected");

            return Redirect('settings');
        
    }    
    //facebook Post Now

    public function socialFbscheduleNow($postID,$post,$media){
        //schedule post on facebook
        
        $userId = (Auth::User()->id);
        $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();

        if(trim($fbPageId) != ""){
            $fbSecretKey = Service::where('user_id','=',$userId)->where('key','fb_secret_key')->first();
            if(trim($fbSecretKey) != ""){
                $helper = $this->fb->getRedirectLoginHelper();
                    $accessToken=$fbSecretKey->value;
                    Session::set('fb_user_access_token', (string) $accessToken);
                    $this->fb->setDefaultAccessToken($accessToken);

                if(isset($accessToken)){
                    $pages = $this->fb->get('/me/accounts');
                    $pages = $pages->getGraphEdge()->asArray();
                    
                    $total = count($media);
                    if($post->first_link != "" && $total > 0){ // link and images
                        foreach ($media as $number => $image) {
                            $imageName = $image->photo;
                        break;
                        }
                        $imagePath = url().'/public/uploads/'. Auth::user()->id.'/'.$imageName;
                        $data = array(
                            'message' => $post->caption,
                            'link' => $post->first_link,
                            'picture' => $imagePath
                        );
                        foreach ($pages as $key) {
                            if($key['id'] == $fbPageId->value){
                                $postapi = $this->fb->post('/' .$fbPageId->value. '/feed', $data,$key['access_token']);
                                $result = $postapi->getGraphNode()->asArray();
                            }
                        }
                    }elseif($post->first_link =="" && $total > 0){ // not link but only images
                        foreach ($pages as $key) {
                            foreach ($media as $number => $image) {
                                $imageName = $image->photo;
                            break;
                            }
                            $imagePath = url().'/public/uploads/'. Auth::user()->id.'/'.$imageName;
                            if($key['id'] == $fbPageId->value){

                                $postapi = $this->fb->post('/'.$fbPageId->value.'/photos', [
                                    'caption' => $post->caption,
                                    'url' =>  $imagePath
                                    ], 
                                    $key['access_token']);
                                $result = $postapi->getGraphNode()->asArray();

                            }
                        }       
                    }elseif($post->first_link !="" && $total == 0){ // link but not images
                        foreach ($pages as $key) {
                            if($key['id'] == $fbPageId->value){
                                $postapi = $this->fb->post('/' . $fbPageId->value . '/feed', array(
                                    'message' => $post->caption,
                                    'link' => $post->first_link
                                    ), $key['access_token']);
                                $result = $postapi->getGraphNode()->asArray();
                            }
                        }       
                    }else{ // only message but not link and images

                        foreach ($pages as $key) {
                            if($key['id'] == $fbPageId->value){
                                $postapi = $this->fb->post('/' . $fbPageId->value . '/feed', array(
                                    'message' => $post->caption
                                    ), $key['access_token']);
                                $result = $postapi->getGraphNode()->asArray();
                                //print_r($result);exit;
                            }
                        }       
                    }                  
                }
                if($result){
                    $message = "true";
                    //save post ID 
                    if($result['id']){
                        $post->fb_id= $result['id'];
                        $post->save();
                    }   

                }else{
                    $message ="Something went wrong";
                }
            }else{
                //$message = "Please connect your account in settings.";
            }
        }else{
            //$message ="Please add Facebook Page ID in settings.";
        }   

        //return Redirect::back();
        return $message;
    }


    
}
