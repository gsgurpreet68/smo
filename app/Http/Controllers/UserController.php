<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\UserMeta;
use App\PostMedia;
use App\PostAnalytic;
use App\Service;
use App\Network;
use App\Link;
use App\UserNotification;
use App\PostStatus;
use Input;
use Session;
use Redirect;
use Storage;
use Validator;
use SammyK;
use Happyr;
use Twitter;
use View;
use File;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\NotificationController;
use App\Jobs\FacebookQueueJob;
use App\Jobs\LinkedInQueueJob;
use App\Jobs\TwitterQueueJob;
use App\Jobs\CheckScheduleStatus;
use App\Http\Controllers\FacebookSchedule;
use Auth0\SDK\API\Authentication;

class UserController extends Controller

{	
	//global $linkedIn;
	protected $linkedIn;
	protected $fb;


	public function __construct(){
		if(Auth::user()){
			$userId = Auth::user()->id;
			$fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();	
			$linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
			$twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();

			$userLinks['fb'] = ""; 	$userLinks['in'] = ""; $userLinks['tw']="";
			if($fbPageId){
				$userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
			}
			if($linkedin_profile_link){
				$userLinks['in'] = $linkedin_profile_link->value;	
			}
			if($twitterusername){
				$userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;	
			}
			
			View::share('userprofilelink', $userLinks);
			$this->notify = new NotificationController();
			$this->notify->currentNotifications();

			$this->linkedIn=new Happyr\LinkedIn\LinkedIn(env('LINKEDIN_APP_ID'),env('LINKEDIN_SECRET_KEY'));
			$this->fb = app(SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
		}
	}

	public function index(){
		$auth0 = array();
		$auth0['AUTH0_CLIENT_ID'] = env('AUTH0_CLIENT_ID');
		$auth0['AUTH0_DOMAIN'] = env('AUTH0_DOMAIN');
		$auth0['AUTH0_CLIENT_SECRET'] = env('AUTH0_CLIENT_SECRET');
		$auth0['AUTH0_CALLBACK_URL'] = env('AUTH0_CALLBACK_URL');
		
		return view('welcome',compact('auth0'));

	}

	// Schedule Home Page
	public function home(){
		
		return view('user.home');
	}
	
	// Create Schedule View
	function schedule(){
		$user = Auth::user();
		$userId = $user->id;
		
		$allposts = $user->posts()->where("schedule_status","=",0)->orderBy('id','desc')->paginate(6);
		$failedposts = $user->posts()->where("schedule_status","=",2)->orderBy('id','desc')->get();
		$fbPageName = Service::where('user_id',$userId)->where('key','fb_page_name')->first();
		$linkedinusername = Service::where('user_id',$userId)->where('key','linkedin_user_name')->first();
		$twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();
				
		return View('user.schedule',compact('allposts','failedposts','fbPageName','linkedinusername','twitterusername'));
	}

	public function service()
    {
    	//facebook

    	$helper = $this->fb->getRedirectLoginHelper();
    	$permissions = ['manage_pages','publish_pages','user_photos','email','public_profile','read_insights']; // optional
    	$redirect_url = route('accesstoken');
    	define('APP_URL', $redirect_url);
    	$loginUrl = $helper->getLoginUrl(APP_URL, $permissions);
    	$userId = Auth::User()->id;
    	$currentUser = User::find($userId);
    	
		$fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();	
		$fbPageName = Service::where('user_id',$userId)->where('key','fb_page_name')->first();

		//LinkedIn
		$redirect_linkedin_url = route('linkedin');
		$linkedinurl = $this->linkedIn->getLoginUrl(array('redirect_uri'=>$redirect_linkedin_url));
		$linkedinusername = Service::where('user_id',$userId)->where('key','linkedin_user_name')->first();	

		//Twitter

    	$redirect_twitter_url = route('twitter.login');
		$token = Twitter::getRequestToken(route('twitter.callback'));
		$twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();	
		$twitterurl = route('twitter.login'); 

    	return view('user.settings',compact('currentUser','loginUrl','fbPageId','fbPageName','linkedinurl','linkedinusername','twitterusername','twitterurl'));
    }	

	//Save Schedule
	function schedulesave(Request $request){		
		$data = $request->all();
		$post = new Post; 
		$post->first_link = $data['first_link'];
		$post->banner = $data['banner'];
		$post->bedrooms = $data['bedrooms'];
		$post->bathrooms = $data['bathrooms'];
		$post->squarefeet = $data['squarefeet'];
		$post->caption = $data['caption'];
		if(isset($data['brandbar'])){
			$post->brandbar = $data['brandbar'];
		}	

		
		if($data['postScheduleTime'] == "Now"){
			$scheduleTime = date("Y-m-d H:i:s");  
			$scheduleStringTime = strtotime($scheduleTime);
			$data['schedule_time'] = $scheduleTime;
			$post->schedule_time = $scheduleTime;

			$post->schedule_status = 1;	
			$rules = array(
				'first_link' =>array('regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'),
				'caption'=>'required'
			);
		}else{
			$dateTime = strtotime($data['schedule_date']." ".$data['schedule_time']);
			$scheduleTime = date("Y-m-d H:i:s",$dateTime);  
			$scheduleStringTime = strtotime($scheduleTime);
			$currenttime = date("Y-m-d H:i:s");
			$data['schedule_time'] = $scheduleTime;
			$post->schedule_time = $scheduleTime;

			$post->schedule_status = 0;		
			$rules = array(
				'first_link' =>array('regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'),
				'schedule_time' => 'required | after:'.$currenttime,
				'schedule_date'=>'required',
				'caption'=>'required'
			);
		}
		$validator = Validator::make($data, $rules);
		if($validator->passes()){
			$user = Auth::user();
			$user->posts()->save($post);
			
			//upload Media
			$mediaGallery = $request->file('photoUpload');
			$files = array_filter($mediaGallery);
			$file_count = count($files);
			$uploadcount = 0;
			if(!empty($files)){
				foreach ($files as $key => $file) {
					$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				    $validator = Validator::make(array('file'=> $file), $rules);
				      if($validator->passes()){
				      	
				      	$destinationPath = public_path().'/uploads/'. Auth::user()->id;
				      	$ext = $file->getClientOriginalExtension();
						$filename = time() . '_'.md5(strtolower($file->getClientOriginalName())).'.'.$ext;
				        $upload_success = $file->move($destinationPath, $filename);
				        $media = new PostMedia;
				        $media->photo = $filename;
				        $post->media()->save($media);
				        $uploadcount ++;
				    }
				}	
			}else{
				if($data['previewImage']){
					$url = $data['previewImage'];
					$extension = pathinfo($url, PATHINFO_EXTENSION);
					$pieces = explode("?", $extension);
					$extension = $pieces[0];
					if($extension == ""){
						$extension = "jpg";
					}
					$filename = time().'.'. $extension;

					//get file content from url
					$file = file_get_contents($url);
					$destinationPath = public_path().'/uploads/'. Auth::user()->id;
					if (!file_exists($destinationPath)) {
						mkdir($destinationPath, 0755, true);
					}
					$save = file_put_contents($destinationPath.'/'.$filename, $file);
					$previewmedia = new PostMedia;
			        $previewmedia->photo = $filename;
			        $post->media()->save($previewmedia);
				}
			}

			
			$network = new Network;	
			if(isset($data['post_facebook'])){
				$network->fb_enable = 1;	
				$fb_network = true;
			}else{
				$network->fb_enable = 0;
				$fb_network = false;
			}
	        if(isset($data['post_linkedin'])){
	        	$network->in_enable = 1;	
	        	$in_network = true;
	        }else{
	        	$network->in_enable = 0;
	        	$in_network = false;
	        }
	         if(isset($data['post_twitter'])){
	        	$network->tw_enable = 1;	
	        	$tw_network = true;
	        }else{
	        	$network->tw_enable = 0;
	        	$tw_network = false;
	        }

	        if($fb_network || $in_network || $tw_network){
		        $post->network()->save($network);
	    	}

			$lastPostId = $post->id;
			$lastpost = Post::find($lastPostId);
            $lastmedia = PostMedia::where('post_id','=',$lastPostId)->get();
            $user = Auth::user();
            if($data['postScheduleTime'] == "Now"){
	            if($fb_network){
	            		$fbjob = (new FacebookQueueJob($user,$lastpost))->delay(3);
						$this->dispatch($fbjob);
	            }
				if($in_network){
					$injob = (new LinkedInQueueJob($user,$lastpost))->delay(6);
						$this->dispatch($injob);
				}
				if($tw_network){
					$twjob = (new TwitterQueueJob($user,$lastpost))->delay(9);
					$this->dispatch($twjob);
				}
				$check_schedule_status = (new CheckScheduleStatus($user,$lastpost))->delay(12);
					$this->dispatch($check_schedule_status);
				
			}else{

			}
			Session::flash('message', "The post has been scheduled");		
		}else{
			Session::flash('error',$validator->messages()->first());
	    	return redirect('/schedule')->withinput();
		}	
        return Redirect::back();
	}	

	
	// Update Post
	public function update($id,Request $request){

		$data = $request->all();
		$deletemedia = false;
		if (array_key_exists('deletephotoid',$data)){
			$deletemedia = true;
		}	
		$post = Post::find($id);
		$post->banner = $data['banner'];
		$post->bedrooms = $data['bedrooms'];
		$post->bathrooms = $data['bathrooms'];
		$post->squarefeet = $data['squarefeet'];
		$post->caption = $data['caption'];
		// if(isset($data['brandbar'])){
		// 	$post->brandbar = $data['brandbar'];
		// }
		$dateTime = strtotime($data['schedule_date']." ".$data['schedule_time']);
		$scheduleTime = date("Y-m-d H:i:s",$dateTime);  
		$post->schedule_time = $scheduleTime;

		$rules = array(
				'schedule_time'=>'required',
    			'caption'=>'required'
			);
		$validator = Validator::make($data, $rules);
		if($validator->passes()){
			$post->save();
			$user = Auth::user();
			if($deletemedia){ 
				foreach ($data['deletephotoid'] as $key => $mediaid) {
					$media = PostMedia::find($mediaid);
					$media->delete();
				}
			}
			$mediacount = PostMedia::where('post_id', '=' , $id)->get();
			$updatemore = 4 - count($mediacount);			
			
			//upload New Media

			$mediaGallery = $request->file('photoUpload');
			$files = array_filter($mediaGallery);
			$file_count = count($files);
			$uploadcount = 0;
			$limitcounter = 1;
			if(!empty($files)){
				foreach ($files as $key => $file) {
					if($limitcounter <= $updatemore ){
						$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					    $validator = Validator::make(array('file'=> $file), $rules);
					      if($validator->passes()){
					      	$destinationPath = public_path().'/uploads/'. Auth::user()->id;
					        $filename =time().$file->getClientOriginalName();
					        $upload_success = $file->move($destinationPath, $filename);
					        $media = new PostMedia;
					        $media->photo = $filename;
					        $post->media()->save($media);
					        $uploadcount ++;
					    }
					 $limitcounter++;   
					}
				}
			}
		}else{
			Session::flash('error',$validator->messages()->first());
	    	return redirect('/schedule')->withinput();
		}	
		Session::flash('message', "Post has updated.");
        return Redirect('/schedule');
		
	}

	//Schedule again if post fail
	function postReschedule($id){
		$lastpost = Post::find($id);
		$user = Auth::user();
		if($lastpost->poststatus->fb_status == 2){
			$fbjob = (new FacebookQueueJob($user,$lastpost))->delay(3);
			$this->dispatch($fbjob);
	    }
		if($lastpost->poststatus->in_status == 2){
			$injob = (new LinkedInQueueJob($user,$lastpost))->delay(6);
			$this->dispatch($injob);
		}
		if($lastpost->poststatus->tw_status == 2){
			$twjob = (new TwitterQueueJob($user,$lastpost))->delay(9);
			$this->dispatch($twjob);
		}
		$check_schedule_status = (new CheckScheduleStatus($user,$lastpost))->delay(12);
		$this->dispatch($check_schedule_status);
		$lastpost->schedule_status = 1;	
		$lastpost->save();
		Session::flash('message',"The post has been scheduled.");
	    return Redirect::back();
	}


	//Delete Post
	public function destroy($id){
		$post = Post::find($id);
		$post->media()->delete();
		$post->poststatus()->delete();	
		$post->network()->delete();	
		$user = User::find(Auth::user()->id);		
		$user->posts()->detach($post);				
		$post->delete();		
		Session::flash('message', "Post Deleted Successfully");
		return Redirect::back();
	}

	public function updateProfile(Request $request){
		$result = array();
		$data = $request->all();
		$user = Auth::user();
		$current_user_id = $user->id;
		
		$usermeta =UserMeta::firstOrCreate(array('user_id'=>$current_user_id));
		
		//$usermeta = new UserMeta;
		if(isset($data['name'])) {
			$user->name = $data['name'];
			$user->save();
		}
		if(isset($data['company'])) $usermeta->company = $data['company'];
		if(isset($data['phone_number'])) $usermeta->phone = $data['phone_number'];
		if(isset($data['display_name'])) $usermeta->display_name = $data['display_name'];
		if($usermeta->save()){
			$result = true;
		}

		if(isset($data['profile_pic'])){
			$file = $request->file('profile_pic');
					$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				    $validator = Validator::make(array('file'=> $file), $rules);
				      if($validator->passes()){
				      	
				      	$destinationPath = public_path().'/uploads/'. Auth::user()->id;
				      	$ext = $file->getClientOriginalExtension();
						$filename = time() . '_'.md5(strtolower($file->getClientOriginalName())).'.'.$ext;
				       $upload_success = $file->move($destinationPath, $filename);
				       $currentUser	  = Auth::user();
				       $fileURL = url().'/public/uploads/'. Auth::user()->id."/".$filename;
				       $currentUser->avatar = $fileURL;
				       $currentUser->save();
				       $result = $fileURL;
				    }else{
				    	$result = false;
				    }
				}

		echo $result;
		exit;
		
	}

	

	
	public function test(){
		echo Carbon::now();
		echo public_path();
		$fb = app(SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);

		$accessToken = "EAAYOi8BNvAwBAAKeyrPeTI2uNgfUlxGiDbM8yqfh6wB7xkaLhCDfeaqGM5UJWhYZBZBfzF2RAWNGUZCdxBRtvgdkMeAR5EKwLvEVzSlZAAuCxOS5vFhfgaMRwitPlZCJUdzyBR76TAmZAXSsm3axHsh8LyQFOsK2kZB2VBosuQ55AZDZD";
		Session::set('fb_user_access_token', (string) $accessToken);
        $fb->setDefaultAccessToken($accessToken);
        
        //Impressions
		//$postapi = $fb->get("/1380031418910896_1696764200570948/insights/post_impressions");


        //Reach 
		//$postapi = $fb->get("/1380031418910896_1696764200570948/insights/post_impressions_unique");
		
		// echo "<pre>";
		// $result = $postapi->getGraphEdge();	
		// print_r($result);

		// total click
		//$postclicks = $fb->get("/1380031418910896_1696764200570948/insights/post_consumptions");




		//total likes
		// $postID = "1380031418910896_1672112519702783";
		// $postclicks = $fb->get($postID."/insights/post_reactions_by_type_total");
		// $data =	$postclicks->getGraphEdge()->asArray();
        //total comments
		//$postclicks = $fb->get("1380031418910896_1672112519702783/comments?limit=0&summary=1&filter=stream");


		//$postclicks = $fb->get("1380031418910896_1729848803929154?fields=shares,comments.summary(true)");
		
		
		echo "<pre>";
//		print_r($data);

		

	}	

	

}