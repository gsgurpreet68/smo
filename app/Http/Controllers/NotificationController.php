<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use App\UserNotification;
use App\PostStatus;
use Input;
use Session;
use Redirect;
use Storage;
use View;

class NotificationController extends Controller
{
  public function __construct(){
    $userId = Auth::user()->id;
    $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();  
    $linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
    $twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();

    $userLinks['fb'] = "";  $userLinks['in'] = ""; $userLinks['tw']="";
    if($fbPageId){
      $userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
    }
    if($linkedin_profile_link){
      $userLinks['in'] = $linkedin_profile_link->value;   
    }
    if($twitterusername){
      $userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;  
    }
    
    View::share('userprofilelink', $userLinks);
  }



  // get active notifications
  function currentNotifications(){
    $user = Auth::user();
    $notifications = $user->notification()->where("status",'=',0)->get()->count();
    Session::set("total_notifications",$notifications);
  }

  //get Notifications and set status 1.
  function getNotifications(){
    $user = Auth::user();
    $notifications = $user->notification()->orderBy('id', 'desc')->paginate(10);
    $total = $notifications->count();
    if($total){
      $user->notification()->where("status",0)->update(array('status'=>1));
      $note_title = array();
      foreach ($notifications as $key => $value) {
        $note_title[] =array('title'=>$value['title'],'description'=>$value['description']);            
      }
      $result = json_encode($note_title);
    }else{
      $result = 0;
    }
    Session::set("total_notifications",0);
    Session::save();
    echo $result;
    die;        
  }


  // Show all Notification on Page
   function getNotificationView(){
    $user = Auth::user();
    $notifications = $user->notification()->paginate(15);
    return view('user.notifications',compact('notifications'));
   }


}
