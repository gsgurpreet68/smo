<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth0\SDK\API\Authentication;

class Auth0UserController extends Controller
{
  public function __construct(){
    $auth0 = new Authentication(env('AUTH0_DOMAIN'), env('AUTH0_CLIENT_ID'));
    $this->auth0Oauth = $auth0->get_oauth_client(env('AUTH0_CLIENT_SECRET'), env('URL'), ['persist_id_token' => true,'persist_refresh_token' => true,]);
  }

  public function loginAuthorization(){

    $userInfo = $this->auth0Oauth->getUser();

    return view("authorization");
  }
}
