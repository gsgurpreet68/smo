<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use Input;
use Session;
use Redirect;
use Storage;
use Twitter;
use View;

class TwitterController extends Controller
{
  public function __construct(){  
    $userId = Auth::user()->id;
    $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();  
    $linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
    $twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();
    $userLinks['fb'] = "";  $userLinks['in'] = ""; $userLinks['tw']="";
    if($fbPageId){
      $userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
    }
    if($linkedin_profile_link){
      $userLinks['in'] = $linkedin_profile_link->value;   
    }
    if($twitterusername){
      $userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;  
    }
    
    View::share('userprofilelink', $userLinks);
  }  

  
  // Twitter Get Access token, Secret and another detail
  public function twitterlogin(){
    $sign_in_twitter = true;
    $force_login = false;

    // Make sure we make this request w/o tokens, overwrite the default values in case of login.
    Twitter::reconfig(['token' => '', 'secret' => '']);
    $token = Twitter::getRequestToken(route('twitter.callback'));
       
    if (isset($token['oauth_token_secret'])){
      $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

      Session::put('oauth_state', 'start');
      Session::put('oauth_request_token', $token['oauth_token']);
      Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

      return Redirect::to($url);
    }

  }

  //twitter Callback
  public function twittercallback(){
    if (Session::has('oauth_request_token'))
    {
      $request_token = [
        'token'  => Session::get('oauth_request_token'),
        'secret' => Session::get('oauth_request_token_secret'),
      ];

      Twitter::reconfig($request_token);

      $oauth_verifier = false;

      if (Input::has('oauth_verifier'))
      {
        $oauth_verifier = Input::get('oauth_verifier');
      }

      // getAccessToken() will reset the token for you
      $token = Twitter::getAccessToken($oauth_verifier);
       
      if (!isset($token['oauth_token_secret']))
      {
        return Redirect::route('twitter.login')->with('error', 'We could not log you in on Twitter.');
      }

      $credentials = Twitter::getCredentials();
      

      if (is_object($credentials) && !isset($credentials->error))
      {
        
        Session::put('access_token', $token);

        $userId = (Auth::User()->id);
        $twitter_oauth_token  = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'twitter_oauth_token'));
          $twitter_oauth_token->key = "twitter_oauth_token";
          $twitter_oauth_token->value = $token['oauth_token'];
          $twitter_oauth_token->save();
         
        $twitter_oauth_token_secret = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'twitter_oauth_token_secret'));
          $twitter_oauth_token_secret->key = "twitter_oauth_token_secret";
          $twitter_oauth_token_secret->value = $token['oauth_token_secret'];
          $twitter_oauth_token_secret->save();    
         
        $twitter_user_name = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'twitter_user_name'));
          $twitter_user_name->key = "twitter_user_name";
          $twitter_user_name->value = $credentials->screen_name;
          $twitter_user_name->save();

        $twitter_user_id = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'twitter_user_id'));
          $twitter_user_id->key = "twitter_user_id";
          $twitter_user_id->value = $token['user_id'];
          $twitter_user_id->save();    

         
        

        return Redirect::to('/settings')->with('message', 'Congrats! You have successfully connected!');
      }

      return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
    }
  }

   
}
