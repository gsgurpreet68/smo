<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use Input;
use Session;
use Redirect;
use Storage;
use Validator;
use SammyK;
use View;
use File;

class FacebookSchedule extends Controller
{
  public function __construct(){
    $this->fb = app(SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
  }

  // Schedule post using Laravel Queue
  public function facebookSchedulePost($user,$post){
    $userId = $user->id;
    $postTitle =  substr($post->caption, 0, 100)."...";
    
    $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();
    if(trim($fbPageId) != ""){
      $fbSecretKey = Service::where('user_id','=',$userId)->where('key','fb_secret_key')->first();
      if(trim($fbSecretKey) != ""){
        $helper = $this->fb->getRedirectLoginHelper();
          $accessToken=$fbSecretKey->value;
          Session::set('fb_user_access_token', (string) $accessToken);
          $this->fb->setDefaultAccessToken($accessToken);

        if(isset($accessToken)){
          try{
            $pages = $this->fb->get('/me/accounts');
            $pages = $pages->getGraphEdge()->asArray();
            $accessTokenValid = true;
          } catch (\Exception $e) {
            $error = $e->getMessage();
            $message['status'] = false; 
            $message['message'] = "Facebook ".$error;
            $accessTokenValid = false;
          }
          if($accessTokenValid){

            $media = PostMedia::where('post_id','=',$post->id)->get();
            $total = count($media);
            $message = array();
            if($post->first_link != "" && $total > 0){ // link and images
              foreach ($media as $number => $image) {
                $imageName = $image->photo;
              break;
              }
              $imagePath = url().'/public/uploads/'. $userId.'/'.$imageName;
              $data = array(
                'message' => $post->caption,
                'link' => $post->first_link,
                'picture' => $imagePath
              );
              foreach ($pages as $key) {
                if($key['id'] == $fbPageId->value){
                  try{
                    $postapi = $this->fb->post('/' .$fbPageId->value. '/feed', $data,$key['access_token']);
                    $result = $postapi->getGraphNode()->asArray();
                    $message['status'] = true;
                    $message['id'] = $result['id']; 
                  } catch (\Exception $e) {
                    $error = $e->getMessage();
                    $message['status'] = false; 
                    $message['message'] ="Facebook ". $error;
                  }
                }
              }
            }elseif($post->first_link =="" && $total > 0){ // not link but only images
              foreach ($pages as $key) {
                foreach ($media as $number => $image) {
                  $imageName = $image->photo;
                break;
                }
                $imagePath = url().'/public/uploads/'. $userId.'/'.$imageName;
                if($key['id'] == $fbPageId->value){
                  try{
                    $postapi = $this->fb->post('/'.$fbPageId->value.'/photos', [
                    'caption' => $post->caption,
                    'url' =>  $imagePath
                    ], 
                    $key['access_token']);
                    $result = $postapi->getGraphNode()->asArray();
                    $message['status'] = true;
                    $message['id'] = $result['id']; 
                  } catch (\Exception $e) {
                    $error = $e->getMessage();
                    $message['status'] = false; 
                    $message['message'] ="Facebook ".  $error;
                  }

                }
              }
            }elseif($post->first_link !="" && $total == 0){ // link but not images
              foreach ($pages as $key) {
                if($key['id'] == $fbPageId->value){
                  try {
                    $postapi = $this->fb->post('/' . $fbPageId->value . '/feed', array(
                    'message' => $post->caption,
                    'link' => $post->first_link
                    ), $key['access_token']);
                    $result = $postapi->getGraphNode()->asArray();
                    $message['status'] = true;
                    $message['id'] = $result['id']; 
                  } catch (\Exception $e) {
                    $error = $e->getMessage();
                    $message['status'] = false; 
                    $message['message'] ="Facebook ".  $error;
                  }
                }
              }
            }else{ // only message but not link and images
              foreach ($pages as $key) {
                if($key['id'] == $fbPageId->value){
                   try {
                    $postapi = $this->fb->post('/' . $fbPageId->value . '/feed', array('message' => $post->caption), $key['access_token']);
                      $result = $postapi->getGraphNode()->asArray();
                      $message['status'] = true;
                      $message['id'] = $result['id']; 
                    } catch (\Exception $e) {
                      $error = $e->getMessage();
                      $message['status'] = false; 
                      $message['message'] ="Facebook ".  $error;
                    }
                }
              } 
            }
          }
        }
      }else{
        $message['status'] = false; 
        $message['message'] = "Please connect your Facebook account in settings.";
      }
    }else{
      $message['status'] = false; 
      $message['message'] ="Please add Facebook Page ID in settings.";
    }  
    $message['description'] =$postTitle; 
    return $message;
  }
}