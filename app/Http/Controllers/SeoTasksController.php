<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Service;
use App\SeoMasterCatgory;
use App\SeoSubCatgory;
use App\SeoMilestoneCatgory;
use App\SeoMilestoneTasks;
use App\UserTaskDetail;
use App\UserTasks;
use Input;
use Session;
use Redirect;
use Storage;
use Validator;
use SammyK;
use View;
use File;

class SeoTasksController extends Controller
{
	public function __construct(){
			$userId = Auth::user()->id;
			$fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();	
			$linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
			$twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();

			$userLinks['fb'] = ""; 	$userLinks['in'] = ""; $userLinks['tw']="";
			if($fbPageId){
				$userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
			}
			if($linkedin_profile_link){
				$userLinks['in'] = $linkedin_profile_link->value;	
			}
			if($twitterusername){
				$userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;	
			}
			
			View::share('userprofilelink', $userLinks);
	}

	//get milestone category info
	public function 	milestoneCategory($catid = null){
		if($catid == ""){
			$catid = 1;
		}
		$masterCat = SeoMasterCatgory::all();
		$SeoSubCatgory = SeoSubCatgory::find($catid);
		return view('seo.category',compact('masterCat','SeoSubCatgory'));
	}

	// get milestone open tasks
	public function openMilestoneTasks($catid = null,$id = null){
		$taskStatus = array();
		if($catid == "") $catid = 1;
		if($id == "")	$id = 1;
			
		//for sidebar	
		$masterCat = SeoMasterCatgory::all();

		$subCat = SeoSubCatgory::find($catid);
		
		$SeoMilestoneCats = SeoMilestoneCatgory::find($id);
		
		// get those tasks which user done 	
		$SeoMilestoneTasks = SeoMilestoneTasks::openGetTask($id,0)->get();

		//completed
		$taskStatus['completed'] = SeoMilestoneTasks::filterTaskStatus($id,1)->count();
		//verified
		$taskStatus['verified'] = SeoMilestoneTasks::filterTaskStatus($id,2)->count();
		//review
		$taskStatus['review'] = SeoMilestoneTasks::filterTaskStatus($id,3)->count();
		//hold
		$taskStatus['hold'] = SeoMilestoneTasks::filterTaskStatus($id,4)->count();
		//reject
		$taskStatus['reject'] = SeoMilestoneTasks::filterTaskStatus($id,5)->count();
		
		//return $SeoMilestoneTasks;
		//get all open tasks 
		$openStatusTasks = $SeoMilestoneTasks->pluck('id');
		//return $openStatusTasks;		
		
		$SeoOpenTasks = SeoMilestoneTasks::whereNotIn('id',$openStatusTasks)->where("milestone_id","=",$id);
		$taskStatus['open'] = $SeoOpenTasks->count();
		$SeoMilestoneTasks = $SeoOpenTasks->paginate(100);
		
		return view('seo.tasks',compact('masterCat','SeoMilestoneCats','SeoMilestoneTasks','taskStatus','subCat'));
	}	

	public function ajaxMilestoneTasks(){
		$id = $_POST['id'];
		$status_id = $_POST['status_id'];
			//return $SeoMilestoneTasks;
		//get all open tasks 
		if($status_id == 0){
			$SeoMilestoneTasks = SeoMilestoneTasks::openGetTask($id,$status_id)->get();
			$openStatusTasks = $SeoMilestoneTasks->pluck('id');
			//return $openStatusTasks;		
			
			$SeoOpenTasks = SeoMilestoneTasks::whereNotIn('id',$openStatusTasks)->where("milestone_id","=",$id);
			$taskStatus['open'] = $SeoOpenTasks->count();
			$SeoMilestoneTasks = $SeoOpenTasks->paginate(100);
		}else{
			$SeoMilestoneTasks = SeoMilestoneTasks::filterTaskStatus($id,$status_id)->paginate(100);
		}
		return view('seo.taskstatus',compact('SeoMilestoneTasks'));
	}


	//get milestone info and tasks according to milestone and status except open tasks
	// public function ajaxMilestoneTasks(){
	// 	$id = $_POST['id'];
	// 	$staus_id = $_POST['status_id'];
	// 	$SeoMilestoneTasks = SeoMilestoneTasks::filterTaskStatus($id,$staus_id)->paginate(100);

	// 	return view('seo.taskstatus',compact('SeoMilestoneTasks'));
	// }

//count all tasks according to status
	public function ajaxTasksCountByStatus(){
		$id = $_POST['id'];

		$taskStatus['completed'] = SeoMilestoneTasks::filterTaskStatus($id,1)->count();
		//verified
		$taskStatus['verified'] = SeoMilestoneTasks::filterTaskStatus($id,2)->count();
		//review
		$taskStatus['review'] = SeoMilestoneTasks::filterTaskStatus($id,3)->count();
		//hold
		$taskStatus['hold'] = SeoMilestoneTasks::filterTaskStatus($id,4)->count();
		//reject
		$taskStatus['reject'] = SeoMilestoneTasks::filterTaskStatus($id,5)->count();
		$SeoMilestoneTasks = SeoMilestoneTasks::openGetTask($id,0)->get();
		$openStatusTasks = $SeoMilestoneTasks->pluck('id');
		//return $openStatusTasks;		
		
		$SeoOpenTasks = SeoMilestoneTasks::whereNotIn('id',$openStatusTasks)->where("milestone_id","=",$id);
		$taskStatus['open'] = $SeoOpenTasks->count();

		$array =  json_encode($taskStatus);
		return $array;
	}

	//get specific sub category information
	public function getSubCategoryInfo(){
		 $result = array();
		 $cat_Id = $_POST['cat_id'];
		 $info = SeoSubCatgory::find($cat_Id);
		 $result['title'] = $info->title;
		 $result['description'] = $info->description;
		 $array = json_encode($result);
		 return $array;
	}

	
	public function updateTaskStatus(){
		$user = Auth::user();
		$taskId = $_POST['taskId']; 
		$taskStaus = $_POST['taskStaus']; 		
		if($taskStaus > -1 && $taskStaus < 6){
			if(isset($taskId) && isset($taskStaus)){		
				$saveTask = UserTaskDetail::firstOrCreate(array('user_id'=>$user->id,'task_id'=> $taskId));
				$saveTask->task_id = $_POST['taskId'];
				$saveTask->status = $_POST['taskStaus'];
				$user->userManyTaskDetail()->save($saveTask);		
			}else{
				return "false";
			}
		}	
		return "true";
	}

	public function taskNotesUpdate(){
		$user = Auth::user();
		$taskId = $_POST['taskId'];
		$notes = $_POST['notes'];
		if(isset($taskId) && isset($notes)){		
				$saveTask = UserTaskDetail::firstOrCreate(array('user_id'=>$user->id,'task_id'=> $taskId));
				$saveTask->task_id = $taskId;
				$saveTask->notes = $notes;
				$user->userManyTaskDetail()->save($saveTask);		
			}else{
				return "false";
			}
	}
  

}
