<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use SammyK;
use App\Service;
use App\PostAnalytic;

class BackgroundFacebookAnalyticsController extends Controller
{
  public function __construct(){
    $this->fb = app(SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
  }

  public function getPostTotalImpression($postID){
    //Impressions
    try{
      $impressionRequest = $this->fb->get($postID."/insights/post_impressions");
      $postImpression = $impressionRequest->getGraphEdge()->asArray();
      $totalImpressions = $postImpression[0]['values'][0]['value'];
      $message['status'] = true;
      $message['value'] = $totalImpressions; 
    }catch (\Exception $e) {
      $error = $e->getMessage();
      $message['status'] = false; 
      $message['message'] = "Facebook ".$error;
    }
    return $message;
  }   

  public function getPostTotalReach($postID){
    //Reach 
    try{
        $reachsRequest = $this->fb->get($postID."/insights/post_impressions_unique");
        $postReach = $reachsRequest->getGraphEdge()->asArray();
        $totalReach = $postReach[0]['values'][0]['value'];
        $message['status'] = true;
        $message['value'] = $totalReach; 
    }catch (\Exception $e) {
        $error = $e->getMessage();
        $message['status'] = false; 
        $message['message'] = "Facebook ".$error;
    }
    return $message;
  }

  public function getPostTotalClick($postID){
    //total click
    try{
        $clicksRequest = $this->fb->get($postID."/insights/post_consumptions");
        $postClicks = $clicksRequest->getGraphEdge()->asArray();
        $totalClicks = $postClicks[0]['values'][0]['value'];
        $message['status'] = true;
        $message['value'] = $totalClicks; 
    }catch (\Exception $e) {
        $error = $e->getMessage();
        $message['status'] = false; 
        $message['message'] = "Facebook ".$error;
    }
    return $message;
  }

  public function getPostTotalLikes($postID){
    //total likes
    try{
        $likesRequest = $this->fb->get($postID."/insights/post_reactions_by_type_total");
        $postLikes = $likesRequest->getGraphEdge()->asArray();
        $totalLikes = $postLikes[0]['values'][0]['value']['like'];
        $message['status'] = true;
        $message['value'] = $totalLikes; 
    }catch (\Exception $e) {
        $error = $e->getMessage();
        $message['status'] = false; 
        $message['message'] = "Facebook ".$error;
    }
    return $message;
  }
    
  public function getPostTotalShare($postID){    
    try{
      $shareRequest = $this->fb->get($postID."?fields=shares");
      $postShare = $shareRequest->getGraphNode()->asArray();
      $totalShare = $postShare['shares']['count'];
      $message['status'] = true;
      $message['value'] = $totalShare; 
    }catch (\Exception $e) {
      $error = $e->getMessage();
      $message['status'] = false; 
      $message['message'] = "Facebook ".$error;
    }
    return $message;
  }   

  public function getPostTotalComments($postID){    
    try{
      $commentsRequest = $this->fb->get($postID."/comments?limit=0&summary=1&filter=stream");
      $postComments = $commentsRequest->getGraphEdge();
      $totalComments = $postComments->getTotalCount(); 
      $message['status'] = true;
      $message['value'] = $totalComments; 
    }catch (\Exception $e) {
      $error = $e->getMessage();
      $message['status'] = false; 
      $message['message'] = "Facebook ".$error;
    }
    return $message;
  } 

  public function getPostAnalyticsDetail($user){
    $userId = $user->id;
    $allposts = $user->posts()->where("schedule_status","=",1)->orderBy('id','desc')->get();
    $fbSecretKey = Service::where('user_id','=',$userId)->where('key','fb_secret_key')->first();
    if(trim($fbSecretKey) != ""){
      $accessToken=$fbSecretKey->value;
      Session::set('fb_user_access_token', (string) $accessToken);
      try{
        $this->fb->setDefaultAccessToken($accessToken);
        $accessTokenValid = true;
      } catch (\Exception $e) {
        $error = $e->getMessage();
        $message['status'] = false; 
        $message['message'] = "Facebook ".$error;
        $accessTokenValid = false;
      }
      if(!empty($allposts)){
        foreach ($allposts as $post){                        
          if($accessTokenValid){
            $postID = $post->fb_id;
            $engagement = 0;    
            $analytic = PostAnalytic::firstOrCreate(array('post_id'=>$post->id));
            $postTotalImpressions = $this->getPostTotalImpression($postID);
            if($postTotalImpressions['status']){
              $analytic->impressions= $postTotalImpressions['value'];
            }
            $totalReach = $this->getPostTotalReach($postID);
            if($totalReach['status']){
              $analytic->reach= $totalReach['value'];
            }
            $totalClicks = $this->getPostTotalClick($postID);
            if($totalClicks['status']){
              $analytic->clicks= $totalClicks['value'];
              $engagement += $totalClicks['value'];
            }           
            $totalLikes = $this->getPostTotalLikes($postID);
            if($totalLikes['status']){
              $analytic->likes= $totalLikes['value'];
              $engagement += $totalLikes['value'];
            }
            $totalComments = $this->getPostTotalComments($postID);
            if($totalComments['status']){
              $analytic->comments= $totalComments['value'];
              $engagement += $totalComments['value'];
            }
            $totalShare = $this->getPostTotalShare($postID);
            if($totalShare['status']){
              $analytic->share= $totalShare['value'];
              $engagement += $totalShare['value'];
            }
            if($totalShare['status']){
              $analytic->engagement= $engagement;
            }
            $analytic->save();    
          }    
        }
      }else{
        echo "No Posts";
      }    
    }    
  }   
}

