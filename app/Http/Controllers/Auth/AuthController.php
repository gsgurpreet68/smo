<?php

namespace App\Http\Controllers\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Socialite;
use Auth;
use App\User;
use App\UserMeta;
use Session;
use Redirect;
use Auth0\SDK\API\Authentication;
use App\Jobs\FacebookAnalyticsJob;

class AuthController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Registration & Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users, as well as the
  | authentication of existing users. By default, this controller uses
  | a simple trait to add these behaviors. Why don't you explore it?
  |
  */

  use AuthenticatesAndRegistersUsers, ThrottlesLogins;

  /**
   * Create a new authentication controller instance.
   *
   * @return void
   */
  public function __construct(){
  
    $auth0 = new Authentication(env('AUTH0_DOMAIN'), env('AUTH0_CLIENT_ID'));

    $this->auth0Oauth = $auth0->get_oauth_client(env('AUTH0_CLIENT_SECRET'), env('URL'), ['persist_id_token' => true,'persist_refresh_token' => true,]);
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'name' => 'required|max:255',
      'email' => 'required|email|max:255|unique:users',
      'password' => 'required|confirmed|min:6',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return User
   */
  protected function create(array $data)
  {
    return User::create([
      'name' => $data['name'],
      'email' => $data['email'],
      'password' => bcrypt($data['password']),
    ]);
  }

  public function Authlogin($provider){
    Session::put('authlogin', 'true');
    return Socialite::with($provider)->redirect();
  }

  public function socialCallback($provider){
    
    $value = Session::get('authlogin');
    if($value != "true"){
      return redirect('/');
    }
    Session::put('authlogin', 'false');                          
    $userData =  Socialite::with($provider)->user();                       
    if (!isset($userData->email)) {
      $userData->email = time() . '-no-reply@smo.com';
    }
    $user = User::where('provider_id','=',$userData->id)->first();
    
    $emailExists = User::where('email', '=', $userData->email)->first();

    if(!$user && $emailExists) {
      Session::flash('message', "User Already registered with this email");
      return Redirect('/');
    }
     
    if($user == "") { 
      $user = new User;
      $user->provider_id=$userData->id;
      $user->provider=$provider;
      $user->name=$userData->name;
      $user->username=$userData->nickname;
      $user->email=$userData->email;
      $user->role='2';
      $user->confirmed = '1';
      if($userData->avatar!=null){
        $user->avatar=$userData->avatar;
      }
      $user->save();
    }
    else{
      $this->checkIfUserNeedsUpdating($userData, $user);
    }

    Auth::loginUsingId($user->id);

    if (Auth::check()) {
      return redirect('home');
      
    }else{
      exit("Nope");    // The user is logged in...
    }        
  }


  //   public function loginAuthorization(){
  //      $userData = $this->auth0Oauth->getUser();
  //      return view("authorization");
  // }

  public function checkIfUserNeedsUpdating($userData, $user,$userProvider) {
    
    if(array_key_exists('email',$userData)){
      $socialData = [
          'avatar' => $userData['picture'],
          'email' => $userData['email'],
          'name' => $userData['name'],
          'username' => $userData['nickname'],
      ];
      $dbData = [
        'avatar' => $user->avatar,
        'email' => $user->email,
        'name' => $user->name,
        'username' => $user->username,
      ];
    }else{
      $socialData = [
        'avatar' => $userData['picture'],
        'name' => $userData['name'],
        'username' => $userData['nickname'],
      ];
      $dbData = [
        'avatar' => $user->avatar,
        'name' => $user->name,
        'username' => $user->username,
      ];
    }

    if ((array_diff($socialData, $dbData))) {
      //if profile pic changed by social user
      if(array_key_exists('email',$userData)){
        $updated_data=[
        'avatar' => $userData['picture'],
        'email' => $userData['email'],
        'name' => $userData['name'],
        ];
      }else{
        $updated_data=[
          'avatar' => $userData['picture'],
          "name"=>$userData['name'],
        ];
      }               
      $user->update($updated_data);
    }
  }
  
  function auth0LoginSignup(){
  
    $result = array();
    $result['redirect'] = "/";
    $userData = $this->auth0Oauth->getUser();
    //print_r($userData);

    if($userData=="" || $userData == false){
      $this->auth0Oauth->logout(); 
      $result['status'] = false;
      $result['message'] = "You are not logged in please try again."; 
      Session::flash('message', "You are not logged in please try again.");
      return json_encode($result);
    }
    //Provide name and id
    if(array_key_exists('identities',$userData)){
      $userProvider = $userData['identities']['0']['provider'];
      $userProviderId = $userData['identities']['0']['user_id'];
    }
    // user name
    if(array_key_exists('name',$userData)){
      $userName = $userData['name'];
    }else{
      $userName = "";
    }
    // user nick name
    if(array_key_exists('nickname',$userData)){
      $userNickname = $userData['nickname'];
    }else{
      $userNickname = "";
    }
    //email
    if(array_key_exists('email',$userData)){
      $userEmail = $userData['email'];
      if($userEmail){
        $email_verified = $userData['email_verified'];
      }else{
        $email_verified = false;
      }
    }else{
      $userEmail = "";
    }
    
    //user email verified check
    if($userEmail){
      if($email_verified == "" || $email_verified == false){
        $this->auth0Oauth->logout(); 
        $result['status'] = false;
        $result['message'] = "Please verify your email ".$userEmail."."; 
        Session::flash('suggestion', "Please verify your email ".$userEmail."."); 
        return json_encode($result);
        exit;
      }
    } 

    //phone number  
    if(array_key_exists('phone_number',$userData)){
      $userPhone = $userData['phone_number'];
    }else{
      $userPhone = "";
    }

    //user image
    if($userProvider=="auth0" || $userProvider =="sms"){
      $userAvatar = "";
    }else{
      if(array_key_exists('picture',$userData)){
        if($userData['picture'] != ""){
          $userAvatar = $userData['picture'];
        }else $userAvatar ="";
      }
    }

    if($userProvider == "sms"){
      if($userPhone){
        $phone_verified = $userData['phone_verified'];
        if($phone_verified == "" || $phone_verified == false){
          $this->auth0Oauth->logout(); 
          $result['status'] = false;
          $result['message'] = "Please verify your phone number".$userPhone."."; 
          Session::flash('suggestion', "Please verify your phone number ".$userPhone.".");
          return json_encode($result);
          exit;              
        }
      }
    }

    //get user name from email if user logged in with email
    if($userProvider == "auth0" ){
      $userName = $userNickname;        
    }
    
    
    //get user by provider and provider id
    $user = User::where('provider_id','=',$userProviderId)->where('provider','=',$userProvider)->first();

    //get user by email, if email exit but user not (using provider and provider id)  
    if($userEmail){
      $emailExists = User::where('email', '=', $userEmail)->first();
      if(!$user && $emailExists) {
        $this->auth0Oauth->logout(); 
        $result['status'] = false;
        $result['message'] = "User Already registered with this email ".$userEmail;
        Session::flash('message', "User Already registered with this email ".$userEmail);
        return json_encode($result);
      }
    }
    
    //create guid 
    $providerAndId = $userProviderId.$userProvider;
    $guid = str_shuffle($providerAndId); 
     if($user == "") { 
        $user = new User;
        $user->provider_id=$userProviderId;
        $user->provider=$userProvider;
        $user->guid = $guid;
        $user->name=$userName;
        $user->username=$userNickname;
        $user->email=$userEmail;
        $user->role='2';
        $user->confirmed = '1';
        if($userAvatar){
          $user->avatar=$userAvatar;
        }
        $user->save();
        $result['redirect'] = Route('settings');
    }
    else{
      if($userProvider != "sms" || $userProvider != "auth0"){
      $result['redirect'] = Route('home');
      $this->checkIfUserNeedsUpdating($userData, $user,$userProvider);
      }
    }
    
    if($userPhone){
      $usermeta =UserMeta::firstOrCreate(array('user_id'=>$user->id));
      $usermeta->phone = $userPhone;
      $usermeta->save();
    }

    Auth::loginUsingId($user->id);

    if (Auth::check()) {

      $analuticJob = (new FacebookAnalyticsJob($user))->delay(1);
      
      $this->dispatch($analuticJob);
      
      $result['status'] = true;
      $result['message'] = "User Auth has verified.";
      return json_encode($result);
    }else{
      $this->auth0Oauth->logout(); 
      $result['status'] = false;
      $result['message'] = "There is some technical problem please connect to support team.";
      Session::flash('message', "There is some technical problem please connect to support team.");
      return json_encode($result);
    } 

  }


  //Logout User
  public function Logout(){
    Auth::logout();
    $this->auth0Oauth->logout();
    return redirect('/');      
  }
}
