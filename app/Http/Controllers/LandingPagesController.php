<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Service;
use App\Network;
use Input;
use Session;
use Redirect;
use Storage;
use View;

class LandingPagesController extends Controller
{
  public function __construct(){
    $userId = Auth::user()->id;
    $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();  
    $linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
    $twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();
    $userLinks['fb'] = "";  $userLinks['in'] = ""; $userLinks['tw']="";
    if($fbPageId){
      $userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
    }
    if($linkedin_profile_link){
      $userLinks['in'] = $linkedin_profile_link->value; 
    }
    if($twitterusername){
      $userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;  
    }     
    View::share('userprofilelink', $userLinks);
  }

  public function landingPages(){
    return view('user.landing-pages');
  }
  public function homeValue(){
    return view('user.home-value');
  }
  public function contactForm(){
    return view('user.contact-form');
  }
  
  
}
