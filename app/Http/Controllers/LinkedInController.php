<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use Input;
use Session;
use Redirect;
use Storage;
use Happyr;
use View;


class LinkedInController extends Controller
{
  public function __construct(){  
    $userId = Auth::user()->id;
    $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();  
    $linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
    $twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();
    $userLinks['fb'] = "";  $userLinks['in'] = ""; $userLinks['tw']="";
    if($fbPageId){
      $userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
    }
    if($linkedin_profile_link){
      $userLinks['in'] = $linkedin_profile_link->value;   
    }
    if($twitterusername){
      $userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;  
    }
    
    View::share('userprofilelink', $userLinks);
    $this->linkedIn=new Happyr\LinkedIn\LinkedIn(env('LINKEDIN_APP_ID'), env('LINKEDIN_SECRET_KEY'));
  }  

  public function linkedin(){
     if ($this->linkedIn->isAuthenticated()){
      $linkedInaccesstoken = $this->linkedIn->getAccessToken(); 
      $linkedInUser=$this->linkedIn->get('v1/people/~:(firstName,lastName,public-profile-url)');

      $userName = $linkedInUser['firstName']." ".$linkedInUser['lastName'];
      $userProfileLink =  $linkedInUser['publicProfileUrl'];
      $userId = (Auth::User()->id);
      
      $linkedin_secret_key = Service::firstOrCreate(array('user_id'=>$userId, 'key'=>'linkedin_secret_key'));
        $linkedin_secret_key->value = $linkedInaccesstoken;
        $linkedin_secret_key->save();
      
      $linkedin_user_name = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'linkedin_user_name'));
        $linkedin_user_name->value = $userName;
        $linkedin_user_name->save();
      
      $linkedin_profile_link = Service::firstOrCreate(array('user_id'=>$userId,'key'=>'linkedin_profile_link'));
        $linkedin_profile_link->value = $userProfileLink;
        $linkedin_profile_link->save();
       
      Session::flash('message', "Congrats! You have successfully connected!");
    } elseif ($this->linkedIn->hasError()) {
      Session::flash('error', "Error found during connecting LinkedIn account.");
    }
    return Redirect('settings');
  }

}
