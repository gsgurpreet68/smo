<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
class ExternalServicesController extends Controller
{
  function file_get_contents_curl($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12'));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
  }

  public function previewLink(){
    $url = $_POST['url'];
    $html = $this->file_get_contents_curl($url);
    $title = "";
    $description ="";
    $image = "";

    //parsing begins here:
    $doc = new \DOMDocument();
    @$doc->loadHTML($html);
    $nodes = $doc->getElementsByTagName('title');
    //get and display what you need:
    $title = $nodes->item(0)->nodeValue;
    $metas = $doc->getElementsByTagName('meta');
    for ($i = 0; $i < $metas->length; $i++){
      $meta = $metas->item($i);
      if($meta->getAttribute('name') == 'Description'){
        $description = $meta->getAttribute('content');
      }
      if($description == ""){
        if($meta->getAttribute('name') == 'description'){
          $description = $meta->getAttribute('content');
        }
      }   
      if($description == ""){
           if($meta->getAttribute('property') == 'og:description'){
          $description = $meta->getAttribute('content');
        } 
      }
      if($description == ""){
           if($meta->getAttribute('property') == 'og:description'){
          $description = $meta->getAttribute('content');
        } 
      }

      if($meta->getAttribute('property') == 'og:image'){
        $image = $meta->getAttribute('content');
        $extension = pathinfo($image, PATHINFO_EXTENSION);
          if($extension == ""){
            $image = "";
          }
      }
    }
    //getting image if og:image not found
    if($image == ""){
      $images = $doc->getElementsByTagName('img');
      $Imglength = $images->length;
      if($Imglength > 100){
        $Imglength = 100;
      }

      for ($i = 0; $i < $Imglength; $i++){
        $imagesrc = $images->item($i);
        $image = $imagesrc->getAttribute('src');
        
          //$file_headers = @get_headers($image);
          if (@fopen($image, "r")) {
            $extension = pathinfo($image, PATHINFO_EXTENSION);
            if($extension != ""){
              list($width, $height) = getimagesize($image);
              if($height > 200 && $width > 200){
                break;
              }else{
                continue;
              }
            }
          }
          else {
            $image = "";
          }
      }
    }

    if($title && $title !="" ){
      $response = array(
          'status' => 'success',
          'title' => $title,
          'description' => $description,
          'image' => $image
        );
      return Response::json($response);  
    }else{
       $response = array(
          'status' => 'failed',
        );
      return Response::json($response);     
    }
  }    
}
