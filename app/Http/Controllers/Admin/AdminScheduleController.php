<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\UserNotification;
use App\PostStatus;
use App\AdminPost;
use Input;
use Session;
use Redirect;
use Storage;
use Validator;
use View;
use File;
use App\Jobs\AdminPostsToUsers;

class AdminScheduleController extends Controller
{
  public function adminSchedule(){
    $user = Auth::user();
    $userId = $user->id;
    $allposts = $user->posts()->where("schedule_status","=",0)->orderBy('id','desc')->paginate(6);
    $allUsers = User::where('role', '=', '2')->get();

    return View('admin.schedule',compact('allposts','allUsers'));
  }

  function adminScheduleSave(Request $request){        
    $data = $request->all();
    $post = new Post; 
    $post->first_link = $data['first_link'];
    $post->banner = $data['banner'];
    $post->bedrooms = $data['bedrooms'];
    $post->bathrooms = $data['bathrooms'];
    $post->squarefeet = $data['squarefeet'];
    $post->caption = $data['caption'];
    if(isset($data['brandbar'])){
        $post->brandbar = $data['brandbar'];
    }
    //get date Time and combine it in one date function format      
    $dateTime = strtotime($data['schedule_date']." ".$data['schedule_time']);
    $scheduleTime = date("Y-m-d H:i:s",$dateTime);  
    $scheduleStringTime = strtotime($scheduleTime);
    $currenttime = date("Y-m-d H:i:s");
    $data['schedule_time'] = $scheduleTime;
    $post->schedule_time = $scheduleTime;

    $post->schedule_status = 0;     
    $rules = array(
        'first_link' =>array('regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'),
        'schedule_time' => 'required | after:'.$currenttime,
        'schedule_date'=>'required',
        'caption'=>'required'
    );
    //check validations
    $validator = Validator::make($data, $rules);
    if($validator->passes()){
      $user = Auth::user();
      //save post
      $user->posts()->save($post);
      
      //upload Media
      $mediaGallery = $request->file('photoUpload');
      $files = array_filter($mediaGallery);
      $file_count = count($files);
      $uploadcount = 0;
      if(!empty($files)){
        foreach ($files as $key => $file) {
          $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
          $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
              $destinationPath = public_path().'/uploads/'. Auth::user()->id;
              $ext = $file->getClientOriginalExtension();
              $filename = time() . '_'.md5(strtolower($file->getClientOriginalName())).'.'.$ext;
              $upload_success = $file->move($destinationPath, $filename);
              $media = new PostMedia;
              $media->photo = $filename;
              $post->media()->save($media);
              $uploadcount ++;
          }
        }   
      }else{
        if($data['previewImage']){
          $url = $data['previewImage'];
          $extension = pathinfo($url, PATHINFO_EXTENSION);
          $pieces = explode("?", $extension);
          $extension = $pieces[0];
          if($extension == ""){
              $extension = "jpg";
          }
          $filename = time().'.'. $extension;
          //get file content from url
          $file = file_get_contents($url);
          $destinationPath = public_path().'/uploads/'. Auth::user()->id;
          if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0755, true);
          }
          $save = file_put_contents($destinationPath.'/'.$filename, $file);
          $previewmedia = new PostMedia;
          $previewmedia->photo = $filename;
          $post->media()->save($previewmedia);
        }
      }
      $network = new Network; 
      if(isset($data['post_facebook'])){
          $network->fb_enable = 1;    
          $fb_network = true;
      }else{
          $network->fb_enable = 0;
          $fb_network = false;
      }
      if(isset($data['post_linkedin'])){
          $network->in_enable = 1;    
          $in_network = true;
      }else{
          $network->in_enable = 0;
          $in_network = false;
      }
       if(isset($data['post_twitter'])){
          $network->tw_enable = 1;    
          $tw_network = true;
      }else{
          $network->tw_enable = 0;
          $tw_network = false;
      }

      if($fb_network || $in_network || $tw_network){
          $post->network()->save($network);
      }

      $lastPostId = $post->id;
      $lastpost = Post::find($lastPostId);
      $lastmedia = PostMedia::where('post_id','=',$lastPostId)->get();
      $user = Auth::user();
      //To get selected users and then save to database
      if(array_key_exists('checkedusers',$data)){
        $selectedUsers = $data['checkedusers'];
        $usersArray =  json_encode($selectedUsers,true);
        $adminPost = new AdminPost; 
        $adminPost->users = $usersArray;
        $adminPost->user_id = $user->id;
        $lastpost->adminposts()->save($adminPost);
        //call Queue job  to assign posts to selected users
        $assignPosts = (new AdminPostsToUsers($user,$lastpost))->delay(2);
        $this->dispatch($assignPosts);        
      }

      Session::flash('message', "The post has been scheduled");       
    }else{
      Session::flash('error',$validator->messages()->first());
      return redirect('/schedule')->withinput();
    }   
    return Redirect::back();
  }

  //delete post by admin
  public function destroy($id){
    $post = Post::find($id);
    $post->media()->delete();
    $post->poststatus()->delete();  
    $post->network()->delete();
    $post->adminposts()->delete();     
    $user = User::find(Auth::user()->id);   
    $user->posts()->detach($post);        
    $post->delete();    
    Session::flash('message', "Post Deleted Successfully");
    return Redirect::back();
  }
}
