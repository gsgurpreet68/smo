<?php

namespace App\Http\Controllers\Admin;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use Validator;
use Carbon;
use Request;
use Session;
use Redirect;
class AdminAuthController extends Controller
{
  
	public $auth;

  public function __construct(Guard $auth, Request $request){
      $this->auth = $auth;
  } 	
	public function index(){
		return view('admin.login');
	}

	public function adminAuthorize(Request $request){

		$request=Request::all();
		$remember = (Input::has('rememberme')) ? true : false;

    	$rules = array(
				'email' => 'required',
				'password' => 'required',
			);

    	$validator = Validator::make($request, $rules);
    	
    	if($validator->passes()){

    		$credential=[
				'email'=>$request['email'],
				'password'=>$request['password']
    		];
    		if(Auth::attempt($credential,$remember)){
    			$user=User::where('email','=',$credential['email'])
							->first();
					if($user){
						Auth::user()->last_login_date = Carbon\Carbon::now();
						Auth::user()->last_login_ip = Request::getClientIp();
						Auth::user()->save();
						$this->auth->login($user, true);
						if(Auth::user()->role=='1'){
							return redirect()->route('adminWelcome');
						}else{
							 Session::flash('error',"you can't access admin panel with these credentials.");
	   					 return Redirect::back();	
						}
					}

    		}else{
    				Session::flash('error','Please check login detail.');
	    			return redirect()->back()->withinput();
    		}
    	}else{
    		Session::flash('error',$validator->messages()->first());
	    	return redirect()->back()->withinput();	
    	}	

	}


	function adminWelcome(){
		return view('admin.home');	
	}

	function adminLogout(){
		Auth::logout();
    return redirect('/admin/login');      
	}



}
