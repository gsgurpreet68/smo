<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use Input;
use Session;
use Redirect;
use Storage;
use Validator;
use Twitter;
use View;
use File;

class TwitterSchedule extends Controller
{
    
  public function __construct(){  
     
  }  

   // twitter Schedule posts
  public function socialtwitterSchedule($user,$post){
    $userId = $user->id;
    $twitter_user_name = Service::where('user_id','=',$userId)->where('key','twitter_user_name')->first();
    $postTitle =  substr($post->caption, 0, 100)."...";
     
    
    if(trim($twitter_user_name) != ""){
        $oauth_request_token = Service::where('user_id','=',$userId)->where('key','twitter_oauth_token')->first();
        $oauth_request_token_secret = Service::where('user_id','=',$userId)->where('key','twitter_oauth_token_secret')->first();
        $twitter_user_id = Service::where('user_id','=',$userId)->where('key','twitter_user_id')->first();
        
         $twiiterSession = array('oauth_token' => $oauth_request_token->value,'oauth_token_secret' => $oauth_request_token_secret->value,
          'user_id'=> $twitter_user_id->value,
          'screen_name' => $twitter_user_name->value,
          'x_auth_expires'=> 0 
        );
        

        Session::set('oauth_request_token',$oauth_request_token->value);
        Session::set('oauth_request_token_secret',$oauth_request_token_secret->value);
        Session::put('access_token', $twiiterSession);
        $caption = $post->caption;
        $media = PostMedia::where('post_id','=',$post->id)->get();
        $total = count($media);
        if($post->first_link != "" && $total > 0){ // link and images
          foreach ($media as $number => $image) {
              $imageName = $image->photo;
              break;
          }
          $small = substr($caption, 0, 40)." ". $post->first_link;
          $imagePath = base_path().'/public/uploads/'. $userId.'/'.$imageName;
          
          try{
            $uploaded_media = Twitter::uploadMedia(['media' => File::get($imagePath)]);
            $result_tw = Twitter::postTweet(['status' => $small, 'media_ids' => $uploaded_media->media_id_string]);

            $message['status'] = true;
            $message['id'] = $result_tw->id; 
          } catch (\Exception $e) {
            $error =  $e->getMessage();
            $message['status'] = false; 
            $message['message'] ="Twitter ". $error;
          }                 
        }        
        elseif($post->first_link =="" && $total > 0){ // not link but only images
          foreach ($media as $number => $image) {
              $imageName = $image->photo;
              break;
          }
          $imagePath = base_path().'/public/uploads/'. $userId.'/'.$imageName;

          try{
            $uploaded_media = Twitter::uploadMedia(['media' => File::get($imagePath)]);
            $result_tw = Twitter::postTweet(['status' => $caption, 'media_ids' => $uploaded_media->media_id_string]);
            $message['status'] = true;
            $message['id'] = $result_tw->id; 
          } catch (\Exception $e) {
            $error =  $e->getMessage();
            $message['status'] = false; 
            $message['message'] = "Twitter ".$error;
          }
        }                       
        elseif($post->first_link !="" && $total == 0){ // link but not images
          $small = substr($caption, 0, 40)." ". $post->first_link;

          try{
            $result_tw = Twitter::postTweet(['status' => $small, 'format' => 'json']);
            $id = json_decode($result_tw,true);
            $message['status'] = true;
            $message['id'] = $id['id']; 
          } catch (\Exception $e) {
            $error =  $e->getMessage();
            $message['status'] = false; 
            $message['message'] = "Twitter ".$error;
          }                
        }else{ // only message but not link and images                    
          try{
            $result_tw = Twitter::postTweet(['status' => $caption, 'format' => 'json']);
            $id = json_decode($result_tw,true);
            $message['status'] = true;
            $message['id'] = $id['id']; 
          } catch (\Exception $e) {
            $error =  $e->getMessage();
            $message['status'] = false; 
            $message['message'] = "Twitter ".$error;
          }
        }
    }else{
      $message['status'] = false; 
      $message['message'] ="Please connect your twitter account.";
    }
     $message['description'] =$postTitle;
    return $message; 
  }


}   








