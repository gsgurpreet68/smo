<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use SammyK;
use App\User;
use App\Post;
use App\UserPost;
use App\Service;
use App\PostAnalytic;
use View;

class FacebookAnalyticsController extends Controller
{
    public function __construct(){
      $userId = Auth::user()->id;
      $fbPageId = Service::where('user_id',$userId)->where('key','fb_page_id')->first();  
      $linkedin_profile_link = Service::where('user_id',$userId)->where('key','linkedin_profile_link')->first();
      $twitterusername = Service::where('user_id',$userId)->where('key','twitter_user_name')->first();
      $userLinks['fb'] = "";  $userLinks['in'] = ""; $userLinks['tw']="";
      if($fbPageId){
        $userLinks['fb'] = "http://www.facebook.com/".$fbPageId->value;
      }
      if($linkedin_profile_link){
        $userLinks['in'] = $linkedin_profile_link->value; 
      }
      if($twitterusername){
        $userLinks['tw'] = 'https://twitter.com/'.$twitterusername->value;  
      }     
      View::share('userprofilelink', $userLinks);
    }

    public function analytics(){
        $user = Auth::user();
        $sorting = Input::get('sort');
        $searchText = Input::get('search');
        $limit = Input::get('per-page');
        if($limit == ""){
          $limit = 25;
        }        
        $search = preg_replace('/\s+/', ' ', $searchText);
        //get posts if sortColumn name is empty or equal to published(date) 
        if (strpos($sorting, '-') !== false) {
            $orderBy = "DESC";
        }else{
            $orderBy = "ASC";
        }
        $sortColumn = str_replace('-', '', $sorting);
        $allposts=$user->posts()->where("schedule_status","=",1)->where('caption', 'like', '%'.$search.'%')->orderBy('schedule_time',$orderBy)->paginate($limit);
        //end of get posts if sortColumn ...

        // if sorting is not empty and not equal to published(date) 
        if($sortColumn != ""){
          if($sortColumn != 'published'){
            $postIds=array();
            $analyticsArr=array();
            //get posts ids from post table      
            foreach ($allposts as $key => $post) {
                $postIds[]= $post->id;
            }
            //get analytic data according to posts id's
            $analytics=PostAnalytic::whereIn('post_id',$postIds)->orderBy($sortColumn,$orderBy)->get();
            // serialize analytics ids
            foreach ($analytics as $key => $analytic) {
                $analyticsArr[]= $analytic->post_id;
            }
            // finally get posts data with analytics
            $allposts=Post::whereIn('id',$analyticsArr)->where("schedule_status","=",1)->orderBy('id',$orderBy)->paginate($limit);
          }
        }
        $totalRecords = $allposts->count();
        
      return view('user.analytics',compact('allposts','orderBy','totalRecords'));
    }
    
}

