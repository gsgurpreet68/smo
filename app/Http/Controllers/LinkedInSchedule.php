<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\Link;
use Input;
use Session;
use Storage;
use Validator;
use Happyr;
use View;
use File;

class LinkedInSchedule extends Controller
{
   public function __construct(){  
    $this->linkedIn=new Happyr\LinkedIn\LinkedIn(env('LINKEDIN_APP_ID'), env('LINKEDIN_SECRET_KEY'));
  }  

   // Schedule post using Laravel Queue 
  public function socialLinkedinSchedule($user,$post){
    $userId = $user->id;
    $postTitle =  substr($post->caption, 0, 100)."...";
     
              
    //schedule post on facebook
      $linkedin_secret_key = Service::where('user_id','=',$userId)->where('key','linkedin_secret_key')->first();
      if(trim($linkedin_secret_key) != ""){
          try{
            $this->linkedIn->setAccessToken($linkedin_secret_key->value);
            $accessTokenValid = true;
          } catch (\Exception $e) {
              $error = $e->getMessage();
              $message['status'] = false; 
              $message['message'] ="LinkedIn: ". $error;
               $accessTokenValid = false;
          }
           if($accessTokenValid) {
          
            $media = PostMedia::where('post_id','=',$post->id)->get();
            $total = count($media);
            
            if($post->first_link != "" && $total > 0){ // link and images
              foreach ($media as $number => $image) {
                $imageName = $image->photo;
                break;
              }
              $imagePath = url().'/public/uploads/'. $userId.'/'.$imageName;
              $options = array('json'=>
                array(
                  'content' => array(
                    'title' => $post->caption, // optional
                    'description' => $post->caption,   
                    'submitted-url' =>  $post->first_link,
                    'submitted-image-url'=> $imagePath
                  ),
                  'visibility' => array(
                    'code' => 'anyone'
                  )
                 )
              );
               $result = $this->linkedIn->post('v1/people/~/shares', $options);

            }elseif($post->first_link =="" && $total > 0){  // not link but only images
              foreach ($media as $number => $image) {
                $imageName = $image->photo;
                break;
              }
              $imagePath = url().'/public/uploads/'. $userId.'/'.$imageName;
               $options = array('json'=>
                array(
                  'content' => array(
                    'title' => $post->caption, // optional
                    'description' => $post->caption,   
                    'submitted-url' =>  url(),
                    'submitted-image-url'=> $imagePath
                  ),
                  'visibility' => array(
                    'code' => 'anyone'
                  )
                 )
              );
               $result = $this->linkedIn->post('v1/people/~/shares', $options);

            }elseif($post->first_link !="" && $total == 0){ // link but not images
              $options = array('json'=>
                array(
                  'content' => array(
                    'title' => $post->caption, // optional
                    'description' => $post->caption,   
                    'submitted-url' =>  $post->first_link
                  ),
                  'visibility' => array(
                    'code' => 'anyone'
                  )
                 )
              );

               $result = $this->linkedIn->post('v1/people/~/shares', $options);

            }else{ // only message but not link and images
              $options = array('json'=>
              array(
                'comment' => $post->caption,
                  'visibility' => array(
                    'code' => 'anyone'
                  )
                )
              );      
              $result = $this->linkedIn->post('v1/people/~/shares', $options);
            
          }
        }
        if (array_key_exists("updateKey",$result)){
          $message['status'] = true; 
          $message['id'] = $result['updateKey'];
        }else{
          $message['status'] = false; 
          $message['message'] ="LinkedIn: ". $result['message'];
        }
      }else{
        $message['status'] = false; 
        $message['message'] ="Please connect your Linkedin account in settings.";
      }
      $message['description'] =$postTitle;
    return $message;
  }
}
