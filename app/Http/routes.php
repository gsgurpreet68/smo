<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Super Admin Route
Route::get('/admin/login','Admin\AdminAuthController@index');
Route::post('/admin/authorize','Admin\AdminAuthController@adminAuthorize')->name('adminAuthorize');
//Routes after loggedin by Admin
Route::group(['prefix' => 'admin/','middleware' => ['admin']], function () {
  Route::get('welcome','Admin\AdminAuthController@adminWelcome')->name('adminWelcome');
  Route::get('logout','Admin\AdminAuthController@adminLogout')->name('adminLogout');
  Route::get('schedule','Admin\AdminScheduleController@adminSchedule')->name('adminSchedule');
  Route::post('schedulesave','Admin\AdminScheduleController@adminScheduleSave')->name('adminScheduleSave');
  Route::post('/post/delete/{id}', 'Admin\AdminScheduleController@destroy');
});
// End of Super Admin Route 

//Front-end Routes
Route::get('/hash',function(){
   echo Hash::make('marketingengines@123');
});

Route::get('/','UserController@index')->middleware(['normaluser']);
Route::get('login', 'Auth0UserController@loginAuthorization')->name('login')->middleware(['normaluser']);

Route::get('/authorize', 'Controller@getUserInformation');
Route::get('social/{provider?}', 'Auth\AuthController@Authlogin');
Route::get('socialCallback/{provider?}', 'Auth\AuthController@socialCallback');
Route::post('authlogin', 'Auth\AuthController@auth0LoginSignup')->name('authlogin');
Route::get('/test','UserController@test');
//Route after loggedin by user
Route::group(['middleware' => ['user']], function () {
  Route::get('/home','UserController@home')->name('home');
  Route::get('schedule', 'UserController@schedule');
  Route::post('schedulesave', 'UserController@schedulesave');
  Route::get('analytics/posts', 'FacebookAnalyticsController@analytics');
  Route::get('posts', 'UserController@Allposts');
  Route::get('logout', 'Auth\AuthController@logout');
  Route::post('/post/delete/{id}', 'UserController@destroy');
  Route::post('/post/update/{id}', 'UserController@update');
  Route::get('/post/reschedule/{id}', 'UserController@postReschedule');
  Route::get('/accesstoken','FacebookController@getAccessTokennumber')->name('accesstoken');
  Route::post('facebook/savepage','FacebookController@facebookPageSave');        
  Route::get('settings','UserController@service')->name('settings');
  
  //preview link
  Route::post('/previewlink','ExternalServicesController@previewLink');
  
  //LinkedIn
  Route::get('/linkedin','LinkedInController@linkedin')->name('linkedin');
  
  //Twitter 
  Route::get('twitter/login','TwitterController@twitterlogin')->name('twitter.login');
  Route::get('twitter/callback','TwitterController@twittercallback')->name('twitter.callback');

  //User Notifications
  Route::post('getnotifications','NotificationController@getNotifications')->name('getnotifications');
  Route::get('notifications','NotificationController@getNotificationView');
  
  //Profile Update
  Route::post('updateprofile','UserController@updateProfile')->name('updateprofile');  

  //Facebook Posts Analytics
  Route::get('analyticsdata','BackgroundFacebookAnalyticsController@getPostAnalyticsDetail')->name('analyticsdata'); 

  //Landing Pages
  Route::get('landing-pages','LandingPagesController@landingPages')->name('landingpages');   
  Route::get('landing-pages/home-value','LandingPagesController@homeValue')->name('landingpages');   
  Route::get('landing-pages/contact-form','LandingPagesController@contactForm')->name('landingpages'); 

  //Seo Tasks
  Route::get('/seo/category/{catid?}','SeoTasksController@milestoneCategory')->name('seotasks');   
  Route::get('/seo/category/{catid?}/tasks/{id?}','SeoTasksController@openMilestoneTasks')->name('openseotasks');
  Route::post('/ajaxmilestonetasks','SeoTasksController@ajaxMilestoneTasks')->name('ajaxMilestoneTasks'); 
  Route::post('getcatinfo','SeoTasksController@getSubCategoryInfo')->name('getSubCategoryInfo');   
  
  Route::post('updatetaskstatus','SeoTasksController@updateTaskStatus')->name('updateTaskStatus');

  Route::post('ajaxTasksCountByStatus','SeoTasksController@ajaxTasksCountByStatus')->name('ajaxTasksCountByStatus');   
   Route::post('taskNotesUpdate','SeoTasksController@taskNotesUpdate')->name('taskNotesUpdate');   
});   
//End of Front-end Routes 

