<?php namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

use \DB;


class Post extends Model {

	
	//use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_link','second_link','banner','bedrooms','bathrooms','squarefeet','caption','start_date','end_date'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function media(){
        //return $this->hasMany('App\UserPost','user_id');
        return $this->hasMany('App\PostMedia','post_id');
    }

    public function userPost(){
        //return $this->hasMany('App\UserPost','user_id');
        return $this->hasOne('App\UserPost','post_id');
    }
   
    public function network(){
        //return $this->hasMany('App\UserPost','user_id');
        return $this->hasOne('App\Network','post_id');
    }

    public function poststatus(){
        //return $this->hasMany('App\UserPost','user_id');
        return $this->hasOne('App\PostStatus','post_id');
    }

    public function analytic(){
        //return $this->hasMany('App\UserPost','user_id');
        return $this->hasOne('App\PostAnalytic','post_id');
    }
    public function adminposts(){
        return $this->hasOne('App\AdminPost','post_id');
    }
    
}
