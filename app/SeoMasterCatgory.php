<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoMasterCatgory extends Model
{
	  protected $table = "seo_master_catgory";
  public function subcats(){
      return $this->hasMany('App\SeoSubCatgory','master_id');
  }
}
