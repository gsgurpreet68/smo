<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTaskDetail extends Model
{
	protected $table = "user_task_detail";
	 protected $fillable = ['status','notes','attachment','assign','due'];

    public function belongToUser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function seoMilestones(){
        return $this->belongsTo('App\SeoMilestoneTasks','task_id');
    }
}
