<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use App\User;
use App\Post;
use App\UserPost;
use App\PostMedia;
use App\Service;
use App\Network;
use App\PostStatus;
use App\UserNotification;
use App\Link;
use Input;
use Session;
use Carbon\Carbon;
use App\Http\Controllers\FacebookSchedule;
use App\Http\Controllers\LinkedInSchedule;
use App\Http\Controllers\TwitterSchedule;

class SchedulePostsToSocial extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'scheduleposts:tosocial';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'This Command will use to schedule post to social platform';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    echo Carbon::now();
    $allposts = Post::where('schedule_time', '>',Carbon::now())->where('schedule_time', '<',Carbon::now()->addMinute(2))->where('schedule_status', '=',0)->get();
     \Log::info("This line add to log after every one minute ".\Carbon\Carbon::now());
     if($allposts){
      
      $facebook= 1; $linkedin = 1; $twitter = 1;

       foreach ($allposts as $singlepost) {
         $userId = $singlepost->userPost->user_id;
         $post = Post::find($singlepost->id);
         $user = User::find($userId); 

         //Facebook Cron Job      
         if($post->network->fb_enable){
          $output = (new FacebookSchedule)->facebookSchedulePost($user,$post );
          \Log::info($output);
          $poststatus =  PostStatus::firstOrCreate(array('post_id'=>$post->id));
          if($output['status']){ // If successfull post to social
             $poststatus->fb_status = 1;
             $post->fb_id = $output['id'];
             $post->save();
          }else{ // If fail scheduling
            $facebook = 0;
            $poststatus->fb_status = 2;
            $usernotification = new UserNotification;
            $usernotification->title = $output['message'];
            $usernotification->description = $output['description'];
          
            $user->notification()->save($usernotification);
          }
          $poststatus->save();       
             
         }

         //Linkedin Cron Job
         if($post->network->in_enable){
          $output = (new LinkedInSchedule)->socialLinkedinSchedule($user,$post );
            $poststatus =  PostStatus::firstOrCreate(array('post_id'=>$post->id));
            if($output['status']){ // If successfull post to social
               $poststatus->in_status = 1;
            }else{ // If fail scheduling
              $linkedin = 0;
              $poststatus->in_status = 2;
              $usernotification = new UserNotification;
              $usernotification->title = $output['message'];
              $usernotification->description = $output['description'];
              $user->notification()->save($usernotification);
            }
            $poststatus->save(); 
          
         }

         //Twitter Cron Job
         if($post->network->tw_enable){
            $output = (new TwitterSchedule)->socialtwitterSchedule($user,$post );
            $poststatus =  PostStatus::firstOrCreate(array('post_id'=>$post->id));
            if($output['status']){ // If successfull post to social
               $poststatus->tw_status = 1;
            }else{ // If fail scheduling
              $twitter = 0;
              $poststatus->tw_status = 2;
              $usernotification = new UserNotification;
              $usernotification->title = $output['message'];
              $usernotification->description = $output['description'];
              $user->notification()->save($usernotification);
            }
            $poststatus->save();     
         }
        if($facebook == 1 && $linkedin == 1 && $twitter == 1){
          $post->schedule_status = 1;
          $post->save();
        }else{ //failed Job
          $post->schedule_status = 2;
          $post->save();
        }
       
       }
    }
    

  }
}
