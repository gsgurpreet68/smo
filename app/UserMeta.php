<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = "user_meta";
    protected $fillable = ['user_id','name','phone','company','display_name'];

    public function userMeta(){
        return $this->belongsTo('App\User','user_id');
    }
}
