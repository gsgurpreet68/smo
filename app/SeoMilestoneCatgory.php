<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoMilestoneCatgory extends Model
{
	protected $table = "seo_milestone_catgory";
	protected $fillable = ['sub_cat_id','title', 'description'];
  public function belongstosub(){
  	return $this->belongsTo('App\SeoSubCatgory','	sub_cat_id');
  }
  public function milestonetasks(){
      return $this->hasMany('App\SeoMilestoneTasks','milestone_id');
  }
}
