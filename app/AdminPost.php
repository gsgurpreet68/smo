<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminPost extends Model
{	

     public function postbyadmin(){
        return $this->belongsTo('App\Post','post_id');
    }
}
