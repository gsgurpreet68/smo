<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;



class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
     
	use Authenticatable, CanResetPassword;

    protected $fillable = ['name','username', 'email', 'password', 'avatar','provider','provider_id','active','confirmed','last_login_ip','last_login_date'];

    protected $hidden = ['password', 'remember_token'];

 	public function posts(){
		//return $this->hasMany('App\UserPost','user_id');
		return $this->belongsToMany('App\Post','user_posts','user_id','post_id');
	}

	public function services(){
        return $this->hasMany('App\Service');
    }
    public function notification(){
        return $this->hasMany('App\UserNotification');
    }
    public function usermeta(){
        return $this->hasOne('App\UserMeta');
    }

    public function userManyTaskDetail(){
        return $this->hasMany('App\UserTaskDetail','user_id');
    }

   
}
