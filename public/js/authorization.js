jQuery(document).ready(function(){
	jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    var action = jQuery("#athorization_link").val();
      jQuery.ajax({
        url: action,
        type: 'POST',
        beforeSend: function() {
          setTimeout(function(){
            jQuery(".checking").addClass("enter");
          });
        },
        success: function(response ){
          var result = jQuery.parseJSON(response);
          if(result.status){
            setTimeout(function(){
              jQuery(".checking").addClass("exit");
              jQuery(".dashboard").addClass("enter");
            },1500);
            setTimeout(function(){
              jQuery(".dashboard").addClass("exit");
              jQuery(".loggedin").addClass("enter");
            },2500);
            setTimeout(function(){
              window.location.href = result.redirect;
            },5000);
          }else{
            setTimeout(function(){
              jQuery(".checking").addClass("exit");
              jQuery(".error").html(result.message);
              jQuery(".error").addClass("enter");
            },1500);
            setTimeout(function(){
              window.location.href = result.redirect;
            },3000);
          }            
          
          
          
        }
      });
	
});