jQuery(document).ready(function(){
 jQuery.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
      }
  });
	//Seo nav slide
	jQuery(".top-nav").click(function(){
		jQuery(".functionlist").hide(300);
	  var second_level =  jQuery(this).parents(".parent_con").find(".functionlist").slideToggle(200);
	  second_level.children("li").first().children("ul").show();
		jQuery(this).parents(".parent_con").find(".functionlist").children("li").first().children("#milestone_category_info").trigger("click");	  
	});

	//on second level menu click
	jQuery(".second_level_nav").click(function(){
		jQuery(".second_level_nav").removeClass("on");
		jQuery(this).parent("li").find("ul").show(300);
		jQuery(".functionlist a").removeClass("on");
		jQuery(this).addClass("on");
	});

	// if user refresh url
	// then expand nav according to that
		var category_segment = jQuery("#category_segment").val();
		var milestone_segment = jQuery("#milestone_segment").val();
		var $this = jQuery("#task_section_"+category_segment);
		$this.parent(".functionlist").show(200);
		$this.find("ul").show();
		if(milestone_segment != ""){
			jQuery(".third_level_"+milestone_segment).addClass("on");	
		}else{
			$this.children("a").addClass("on");
		}

	// hide/show dropdown for tasks status change 
	jQuery(document).on('click', function (event) {
	  if (!$(event.target).closest('.taskpopover,.taskcheckbox').length) {
	    jQuery(".taskstatus").hide(100);
	  }
	});

	jQuery("body").on("click",".taskcheckbox",function(){
		jQuery(".taskstatus").hide(100);
		jQuery(this).next(".taskstatus").show(100).addClass("activepopup");
	});	

	//get Tasks status for accordian 
	function getTotalTasks(){
		var milestone_segment = jQuery("#milestone_segment").val();
		action = jQuery("#ajaxTasksCountByStatus").val();
		jQuery.ajax({
      url: action,
      type: "post",
      data: {id:milestone_segment},
      success: function(response ){	
      	var  allStatus = jQuery.parseJSON(response);
      	jQuery(".tabbar .review").html(allStatus.review);
      	jQuery(".tabbar .completed").html(allStatus.completed);
      	jQuery(".tabbar .verified").html(allStatus.verified);
      	jQuery(".tabbar .hold").html(allStatus.hold);
      	jQuery(".tabbar .reject").html(allStatus.reject);
      	jQuery(".tabbar .open").html(allStatus.open);      	
      }
    });	
	}

	//get tasks list 

	function getTasksList(milestone_segment,statusTypeValue){

		action = jQuery("#updatePostStatus").val();
		jQuery.ajax({
      url: action,
      type: "post",
      data: {id: milestone_segment,status_id :statusTypeValue},
      success: function(response){	
      	jQuery(".tasks-ajax-wrapper").html(response);
      }
    });	
	}
	//change task status
	jQuery("body").on("click",".taskstatus a",function(){
		var taskId = jQuery(this).parents(".tasks_loop_con").find("#task_ID").val();
		var taskStatus = jQuery(this).attr("data-state");
		var milestone_segment = jQuery("#milestone_segment").val();
		var statusTypeValue= jQuery(".tabbar a.on").attr("data-filter");
		action = jQuery("#updateStaus").val();
		jQuery.ajax({
      url: action,
      type: "post",
      data: {taskId:taskId,taskStaus: taskStatus},
      success: function(response ){	
      	if(response == "true"){
      		getTasksList(milestone_segment,statusTypeValue);
      		getTotalTasks();
      	}
      }
    });	
	});

	// get posts according to status
	jQuery("body").on("click",".taskfilters.tabbar a",function(){
		jQuery(".taskfilters.tabbar a").removeClass("on");
		jQuery(this).addClass("on");
		var milestone_segment = jQuery("#milestone_segment").val();
		var statusTypeValue = jQuery(this).attr("data-filter");
		getTasksList(milestone_segment,statusTypeValue);
	});


	//tasks content tabs open/hide
	jQuery("body").on("click","ul.taskfunctions a",function(){
		var tabName =  jQuery(this).attr("data-function");
		jQuery(".taskfunctions_container .taskfunction").hide();
		jQuery(this).parents(".taskwrap").find(".taskfunctions_container .taskfunction_"+tabName).show();
	});

	//close tasks detail tab
	jQuery("body").on("click",".taskfunctions_container .closepopover",function(){
		jQuery(".taskfunctions_container .taskfunction").hide();
	});
	
	// get second level category information on nav link click
	jQuery("body").on("click","#milestone_category_info",function(){
		var cat_id = jQuery(this).attr("data-id");
		var action = jQuery("#getSubCatInfo").val();
		jQuery.ajax({
      url: action,
      type: "post",
      data: {cat_id:cat_id},
      success: function(response ){	
      	jQuery(".content-update").html("<div class='tasks_description'></div>");
      	window.history.pushState("", "", "/seo/category/"+cat_id);
      	var result = jQuery.parseJSON(response);
      	jQuery(".establish-heading").html(result.title);
      	jQuery(".tasks_description").html(result.description);
      }
    });
	});	

	jQuery("body").on("click","#taskNotesUpdate button",function(e){
		e.preventDefault();
		var taskId = jQuery(this).parents(".tasks_loop_con").children("#task_ID").val();
		var notes = jQuery(this).parents("#taskNotesUpdate").find("textarea").val();
		var action =  jQuery(this).parents("#taskNotesUpdate").attr("action");
		jQuery.ajax({
      url: action,
      type: "post",
      data: {taskId:taskId,notes:notes},
      success: function(response ){	
      		if(notes != ""){

      		}else{

      		}
      }
    });
	});


});

//if user refresh url
//if nav expand not load on document ready then expand on window load
jQuery(window).load(function(){
	var category_segment = jQuery("#category_segment").val();
	var milestone_segment = jQuery("#milestone_segment").val();
	var $this = jQuery("#task_section_"+category_segment);
	$this.parent(".functionlist").show(200);
	$this.find("ul").show();
	if(milestone_segment != ""){
		jQuery(".third_level_"+milestone_segment).addClass("on");	
	}

});