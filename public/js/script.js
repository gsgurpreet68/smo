// jQuery(function () {
//     jQuery.ajaxSetup({
//         headers: { 'X-CSRF-TOKEN': jQuery('meta[name="_token"]').attr('content') }
//     });
// });

jQuery(document).ready(function(){
  jQuery.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
  jQuery("#selectPostType .listing").click(function(){
    jQuery("#selectPostType .btn").removeClass("active btn-primary");
    jQuery(this).addClass("active btn-primary").removeClass("btn-default");
    jQuery("#FieldsLink,#FieldsListing,#FieldsMessage,#FieldsBrandedBar").show();
    jQuery("#FieldsPhoto").hide();
  });
  jQuery("#selectPostType .link").click(function(){
    jQuery("#selectPostType .btn").removeClass("active btn-primary");
    jQuery(this).addClass("active btn-primary").removeClass("btn-default");
    jQuery("#FieldsLink").show();
    jQuery("#FieldsPhoto,#FieldsListing,#FieldsMessage,#FieldsBrandedBar").hide();
  });
  jQuery("#selectPostType .photo").click(function(){
    jQuery("#selectPostType .btn").removeClass("active btn-primary");
    jQuery(this).addClass("active btn-primary").removeClass("btn-default");
    jQuery("#FieldsPhoto").show();
    jQuery("#FieldsLink,#FieldsListing,#FieldsMessage,#FieldsBrandedBar").hide();
  });
  jQuery("#selectPostType .message").click(function(){
    jQuery("#selectPostType .btn").removeClass("active btn-primary");
    jQuery(this).addClass("active btn-primary").removeClass("btn-default");
    jQuery("#FieldsMessage").show();
    jQuery("#FieldsPhoto,#FieldsLink,#FieldsListing,#FieldsBrandedBar").hide();
  });

  //timepicker
  jQuery('#NewPostDate,#SPDate').datepicker();
  jQuery('#NewPostTime,#SPTime').timepicker({});


  // Schedule and Edit form Validations
  jQuery("#btnSubmitNewPost").click(function(){
    jQuery(this).addClass("thisButtonClick");
  }); 

  var i = true;
  jQuery("#SubmitNewPost").submit(function(e){
    
    if(i){
      e.preventDefault();
      i = false;        
      var caption = jQuery("#SubmitNewPost #newContentMessage").val();
      var date = jQuery("#SubmitNewPost #NewPostDate").val();
      var time = jQuery("#SubmitNewPost #NewPostTime").val();

      if(caption == ""){
        jQuery("#FieldsMessage .error").html("Please add your message.");
        i = true;
        return false;
      }else{
        jQuery("#FieldsMessage .error").html("");
      }

      if(jQuery('.thisButtonClick').attr("name") == "schedule_later"){
        jQuery(".postScheduleTime").attr("value","");
        if(date != "" && time == "" ){
          jQuery("#SubmitNewPost #scheduleArea .error").html("Please add time.");
          i = true;
          jQuery('#btnSubmitNewPost').removeClass("thisButtonClick");
          return false;
        }else{
          jQuery("#SubmitNewPost #scheduleArea .error").html("");
        } 
        if(date == "" && time != "" ){
          jQuery("#SubmitNewPost #scheduleArea .error").html("Please add date.");
          i = true;
          jQuery('#btnSubmitNewPost').removeClass("thisButtonClick");
          return false;
        }else{
          jQuery("#SubmitNewPost #scheduleArea .error").html("");
        }
        if(date == "" && time == "" ){
          jQuery("#SubmitNewPost #scheduleArea .error").html("Please add date and time.");
          i = true;
          jQuery('#btnSubmitNewPost').removeClass("thisButtonClick");
          return false;
        }else{
          jQuery("#SubmitNewPost #scheduleArea .error").html("");
        }
      }else{
        jQuery(".postScheduleTime").attr("value","Now");
      }

      
      if(date != "" && time != "" ){

        if (time.indexOf('am') > -1)
        {
          var array = time.split('am');
          var time = array[0]+" "+ "am";
        }else if (time.indexOf('pm') > -1)
        {
          var array = time.split('pm');
          var time = array[0]+" "+ "pm";
        }else{
          var array = time.split('am');
            var time = array[0]+" "+ "am";
        }
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if(AMPM == "pm" && hours<12) hours = hours+12;
        if(AMPM == "am" && hours==12) hours = hours-12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if(hours<10) sHours = "0" + sHours;
        if(minutes<10) sMinutes = "0" + sMinutes;
        
        var expireDateArr = date.split("/");
        var expireDate = new Date(expireDateArr[2], expireDateArr[0]-1, expireDateArr[1], sHours,sMinutes);
        console.log(expireDate);
        var todayDate = new Date();
        console.log(todayDate);
        if (todayDate > expireDate) {
          var d = new Date();
           var month = d.getMonth()+1;
          var day = d.getDate();
          var output = (month<10 ? '0' : '') + month + '/' +  (day<10 ? '0' : '') + day +'/'+ d.getFullYear()   ;
            jQuery("#SubmitNewPost #scheduleArea .error").html("The schedule time must be a date after "+ output +" "+time);
            i = true;
            return false;
        }else{
          jQuery(".error").html("");  
          jQuery("#SubmitNewPost").submit();
        } 
      }else if(date == "" && time == ""){
        jQuery(".error").html("");  
        jQuery("#SubmitNewPost").submit();
      }     
    }

    
    
  });

  
  
  // delete posts dialog box
  jQuery(".open-DeleteContent").on("click",function(){
    var url = jQuery(this).attr("targetlink");
    //set dialog box form action
    jQuery("#DeleteContent form").attr("action",url);
  });

  //Edit Posts
  jQuery(".open-EditContent").on("click",function(){
    var postID = jQuery(this).attr("editpostid"); 
    var action = jQuery(this).attr("href"); 
    var previousEditID = jQuery("#editPostId").attr("currenteditpostID"); 
    console.log(postID + "@"+ previousEditID);
    if(previousEditID != postID ){  
      jQuery("#editPostId").attr("currenteditpostID",postID);
      jQuery("#EditContent form").attr("action",action)
      var banner = jQuery("#banner-"+postID).val();
      var bathrooms = jQuery("#bathrooms-"+postID).val();
      var bedrooms = jQuery("#bedrooms-"+postID).val();
      var squarefeet = jQuery("#squarefeet-"+postID).val();
      var first_link = jQuery("#first_link-"+postID).val();
      var second_link = jQuery("#second_link-"+postID).val();
      var caption = jQuery("#caption-"+postID).val();
      var start_time = jQuery("#scheduleTime-"+postID).val();
      var start_date = jQuery("#scheduleDate-"+postID).val();
      var postMedia = jQuery("#posts-media-"+postID).html();
      
      jQuery("#EditContent #contentMessage").val(caption);
      jQuery("#EditContent #banner").val(banner);
      jQuery("#EditContent #bedrooms").val(bedrooms);
      jQuery("#EditContent #bathrooms").val(bathrooms);
      jQuery("#EditContent #square_feet").val(squarefeet);
      jQuery("#EditContent #SPDate").val(start_date);
      jQuery("#EditContent #SPTime").val(start_time);
      jQuery("#EditContent #editPhotoUploader").prepend(postMedia);
    }
  });

  //remove images during update POST
  jQuery("body").on("click",".media .deletePhoto",function(){
    console.log("done");
    var photoID = jQuery(this).parent().children("#imageID").html();
    var cache = jQuery(this).parent(".media");
      cache.fadeOut(800, function(){ 
         cache.remove();
      });

    jQuery(".delete-media-con").append(photoID);
    jQuery(".delete-media-con input").attr("name","deletephotoid[]");
  });


  //preview link

  function ValidURL(str) {
    var pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,8}(:[0-9]{1,8})?(\/.*)?$/;
      if(!pattern.test(str)) {
      return false;
    } else {
      return true;
    }
  }
  function extractDomain(url) {
      var domain;
      //find & remove protocol (http, ftp, etc.) and get domain
      if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
      }
      else {
        domain = url.split('/')[0];
      }

      //find & remove port number
      domain = domain.split(':')[0];

      return domain;
  }

  jQuery(".first_link_preview").click(function(){
    var url = jQuery(".first_link").val();
    var action = jQuery("#previewAPI").val();
    var output = ValidURL(url) ;
    var domain = extractDomain(url);
    var token = jQuery("#tokenForAjax").val();
    if (output){
      jQuery(".previewurlerror").html(" ");
      jQuery(".first_link_preview i").addClass("fa-spinner fa-spin");
      jQuery(this).prop('disabled', true);
      jQuery.ajax({
        url: action,
        type: "post",
        data: {url:url,_token:token},
        success: function(response ){
          jQuery(".previewTitle").html(response.title);
          var description = response.description;
          if (description.length > 100) {
            jQuery(".previewDescription").html(description.substr(0, description.lastIndexOf(' ', 97)) + '...');
          }
          jQuery(".previewImg").attr("src",response.image);
          jQuery("#previewImage").val(response.image);
          jQuery(".previewSiteLink").html(domain);
          jQuery(".first_link_preview i").removeClass("fa-spinner fa-spin");
          jQuery(".first_link_preview").removeAttr('disabled');
          jQuery(".preview_areas").show();
        }
      });
    }else{
      jQuery(".previewurlerror").html("This value should be a valid url.");
    }
  });

  //clear preview image
  jQuery(".closePreview").click(function(){
    jQuery(".first_link").val("");
    jQuery(".preview_areas").hide();
    jQuery(".previewImg").attr("src","");
    jQuery("#previewImage").val("");
    jQuery(".previewSiteLink,.previewDescription,.previewTitle").html("");
  });

  //Hide Show Networks
  jQuery("#toggleNetworksOn").click(function(){
    jQuery(this).hide();
    jQuery("#toggleNetworksOff").show();
    jQuery("#networksPanel").show();
  });

  jQuery("#toggleNetworksOff").click(function(){
    jQuery(this).hide();
    jQuery("#toggleNetworksOn").show();
    jQuery("#networksPanel").hide();
  });

  // Photo Upload 
  jQuery("body").on("click",".uploadButton",function(){
    jQuery(this).next("input").trigger("click");
  });
  
  jQuery("body").on("change",".photoUploaderInput",function(){
    var  html = '<div class="multiboxContainer" style="float:left;"><div class="multibox uploadedImg uploadButton"><i style="color: #0095d7;margin: 5px;" class="fa fa-plus-circle fa-3x"></i><br>Add Photos</div><input name="photoUpload[]" class="photoUploaderInput orakuploaderFileInput"  type="file"></div>';
    jQuery(this).parents(".photos-con").append(html);   
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        jQuery(this).prev().removeClass("uploadButton").html("<div class='picture_delete'></div><img class='current' src='' />");
        
        reader.onload = function (e) {
          jQuery(".uploadedImg img.current").attr('src', e.target.result);
          jQuery(".uploadedImg img.current").removeClass("current");
        }            
        reader.readAsDataURL(this.files[0]);
      }
  });

  jQuery("body").on("click",".picture_delete",function(){
    jQuery(this).parents(".multiboxContainer").fadeOut(600, function() { $(this).remove(); });
  });

  // Load Notifications 
  jQuery("#global-notifications").click(function(){
    
    if(jQuery(".activenotify").length > 0){
      jQuery("#notification-container").hide();
      jQuery(this).removeClass("activenotify"); 
      return false;   
    }else{
      jQuery(this).addClass("activenotify");      
    } 
    var action = jQuery("#notification_route").val(); 
      jQuery.ajax({
        url: action,
        type: "post",
        data: {url:'test'},
        success: function(response ){
          if(response != 0){
            var response =jQuery.parseJSON(response);
            var html = '<ul>';
            jQuery.each(response,function(key,value){
              html +='<li>';
              html +='<span class="title">'+ value.title +'</span>';
              html +='<span class="desc">'+ value.description +'</span>';
              html +='</li>';
            });
            html += '</ul>';
            jQuery("#global-notifications").removeClass("red-alert");
            jQuery("#notification-list ul").remove();
            jQuery("#notification-list").append(html);
            jQuery(".no-notification").hide();
          }else{
            jQuery("#notification-list ul").remove();
            jQuery(".no-notification").show();

          }
          jQuery("#notification-container").show();
          }
      });
  });
  // Load Notifications  END

  // Profile Update START 
  jQuery("#profile_pic").change(function(e){
    e.preventDefault();

    jQuery(".editableform-loading").show();
    jQuery(".changeUserImg").hide();
    var action = jQuery("#updateProfilePic").attr("action");
    var formData = new FormData(jQuery("#updateProfilePic")[0]);
      jQuery.ajax({
        url: action,
        type: 'POST',
          data: formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
        success: function(response ){
          if(response){
            jQuery(".changeUserImg,.profile-picture").attr("src",response).show();
            jQuery(".editableform-loading").hide();
            jQuery(".close-popup").trigger("click");
          }else{
            jQuery(".profile-msg").html("An error found so please try agian later.")  
          }

          }
      });
  });

  jQuery("#changePhotoUpload").click(function(){
    jQuery("#profile_pic").trigger("click");
  });

  var fieldValue = "";
  jQuery("body").on("click",".editable-cancel",function(){
    jQuery(".profile-detail .update-profile-popup").remove();
    jQuery(".activeEditField").removeClass("activeEditField");
  });
  jQuery("body").on("click",".change-profile",function(){
    jQuery(".change-profile").removeClass("active-edit")
    if(jQuery(".profile-detail .update-profile-popup").length > 0){
      jQuery(".profile-detail .update-profile-popup").remove();
    }
    jQuery(this).addClass("active-edit");
    var $textField =  jQuery(this).prev("a");
    fieldValue = $textField.html();
    $textField.addClass("activeEditField");
    var heading = jQuery(this).attr("title");
    var fieldType = jQuery(this).attr("field-type");
    var fieldName = jQuery(this).attr("field-name");    
    var clone = jQuery(".update-profile-popup").clone();
    jQuery(clone).insertAfter(".active-edit");
    jQuery(".profile-detail .popover-title").html(heading);
    jQuery(".profile-detail .editable-input input").attr("type",fieldType).attr("name",fieldName).val(fieldValue);    
    jQuery(".profile-detail .update-profile-popup").show();
  });

  jQuery("body").on("click",".editable-submit",function(e){
    e.preventDefault();
    var fieldValue =  jQuery(".profile-detail .editable-input input").val();
    jQuery(".profile-detail .editableform-loading").show();
    jQuery(".profile-detail .editableProfileForm").css("visibility","hidden");
    var submitForm = jQuery(".profile-detail .editableProfileForm");
    var formData = new FormData(submitForm[0]);
    var action = submitForm.attr("action");
      jQuery.ajax({
        url: action,
        type: 'POST',
          data: formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
        success: function(response ){
          if(response){
            jQuery(".activeEditField").html(fieldValue);
            jQuery(".profile-detail .update-profile-popup").remove();
            jQuery(".activeEditField").removeClass("activeEditField");
          }else{
            jQuery(".editableform-loading").hide();
            jQuery(".editableProfileForm").css("visibility","visible");
          }
        
          }
      });
  });

  // Profile Update END

  //Analytic Page
  //Submit Form on Per page limit select
  jQuery(".per-page-select select").change(function(){
    console.log("seletc");
    jQuery(".search-form form").submit();
  });
  //EndAnalytic Page
});
