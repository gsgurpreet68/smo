jQuery(document).ready(function(){
  jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
  //for Social and database Auth0
  var lock = new Auth0Lock(AUTH0_CLIENT_ID, AUTH0_DOMAIN, { 
          container: 'root',          
          auth: {
            redirectUrl: AUTH0_CALLBACK_URL,
            responseType: 'code',
          }
        });
  // var options = {
  //   rememberLastLogin: false
  // };
    lock.show();

    // For mobile Auth0
    var lock1 = new Auth0LockPasswordless(AUTH0_CLIENT_ID,AUTH0_DOMAIN); 
     lock1.sms({
       container: 'roots',
      callbackURL: AUTH0_CALLBACK_URL,       
    });
    $(".auth0-lock-header-logo").attr("src",Logo+"/public/images/logo.png").css("visibility","visibile");
    $(".auth0-lock-header-logo").parent().addClass("Logos");
    $(".auth0-lock-name").html("Marketing Engines");
});